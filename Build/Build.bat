@echo off

set currentDir=%CD%

xcopy "C:\Users\Naveen Hegde\Desktop\CybernetCtrl\References\32\System.Data.SQLite.dll" "C:\Users\Naveen Hegde\Desktop\CybernetCtrl\CybernetCtrl.Client\bin\Release\System.Data.SQLite.dll" /Y /V
xcopy "C:\Users\Naveen Hegde\Desktop\CybernetCtrl\References\32\System.Data.SQLite.dll" "C:\Users\Naveen Hegde\Desktop\CybernetCtrl\CybernetCtrl.Server\bin\Release\System.Data.SQLite.dll" /Y /V

cd c:
cd "C:\Program Files (x86)\Microsoft SDKs\Windows\v7.0A\Bin" 
CorFlags.exe "C:\Users\Naveen Hegde\Desktop\CybernetCtrl\CybernetCtrl.Client\bin\Release\CybernetCtrl.Client.exe" /32Bit+
CorFlags.exe "C:\Users\Naveen Hegde\Desktop\CybernetCtrl\CybernetCtrl.Client.App\bin\Release\CybernetCtrl.Client.App.exe" /32Bit+

cd %currentDir%

"C:\Program Files (x86)\Inno Setup 5\ISCC.exe" /q .\Script-Cybernet-Ctrl.Admin.iss
"C:\Program Files (x86)\Inno Setup 5\ISCC.exe" /q .\Script-Cybernet-Ctrl.Client.iss

pause