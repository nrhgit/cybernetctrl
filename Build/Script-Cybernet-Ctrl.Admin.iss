[Setup]
AppName=Cybernet-Ctrl.Server
AppVerName=Cybernet-Ctrl.Server
DefaultDirName={pf32}\Cybernet-Ctrl.Server
DefaultGroupName=Cybernet-Ctrl
UninstallDisplayIcon={app}\CybernetCtrl.Server.exe
OutputDir=C:\Users\Naveen Hegde\Desktop\CybernetCtrl\Build
OutputBaseFilename=CybernetCtrl.Server
ChangesAssociations=yes
Compression=lzma2
SolidCompression=yes
VersionInfoVersion=1.00
VersionInfoTextVersion=1.00
VersionInfoProductName=Cybernet-Ctrl
VersionInfoProductVersion=1.00
PrivilegesRequired=admin
LicenseFile=C:\Users\Naveen Hegde\Desktop\CybernetCtrl\LICENSE.txt
AlwaysRestart=yes

[Icons]
Name: {group}\Cybernet-Ctrl.Server; Filename: {app}\CybernetCtrl.Server.exe

[Files]

; Application Files
Source: C:\Users\Naveen Hegde\Desktop\CybernetCtrl\CybernetCtrl.Server\bin\Release\CybernetCtrl.Server.exe; DestDir: {app}; Flags: ignoreversion restartreplace
Source: C:\Users\Naveen Hegde\Desktop\CybernetCtrl\CybernetCtrl.Server\bin\Release\ffmpeg.exe; DestDir: {app}; Flags: ignoreversion restartreplace
Source: C:\Users\Naveen Hegde\Desktop\CybernetCtrl\CybernetCtrl.Server\bin\Release\CybernetCtrl.Common.dll; DestDir: {app}; Flags: ignoreversion restartreplace
Source: C:\Users\Naveen Hegde\Desktop\CybernetCtrl\CybernetCtrl.Server\bin\Release\CybernetCtrl.Common.Forms.dll; DestDir: {app}; Flags: ignoreversion restartreplace
Source: C:\Users\Naveen Hegde\Desktop\CybernetCtrl\CybernetCtrl.Server\bin\Release\CybernetCtrl.Database.dll; DestDir: {app}; Flags: ignoreversion restartreplace
Source: C:\Users\Naveen Hegde\Desktop\CybernetCtrl\CybernetCtrl.Server\bin\Release\CybernetCtrl.License.dll; DestDir: {app}; Flags: ignoreversion restartreplace
Source: C:\Users\Naveen Hegde\Desktop\CybernetCtrl\CybernetCtrl.Server\bin\Release\CybernetCtrl.Message.dll; DestDir: {app}; Flags: ignoreversion restartreplace
Source: C:\Users\Naveen Hegde\Desktop\CybernetCtrl\CybernetCtrl.Server\bin\Release\CybernetCtrl.Modal.dll; DestDir: {app}; Flags: ignoreversion restartreplace
Source: C:\Users\Naveen Hegde\Desktop\CybernetCtrl\CybernetCtrl.Server\bin\Release\CybernetCtrl.TcpComm.dll; DestDir: {app}; Flags: ignoreversion restartreplace
Source: C:\Users\Naveen Hegde\Desktop\CybernetCtrl\CybernetCtrl.Server\bin\Release\MetroFramework.Design.dll; DestDir: {app}; Flags: ignoreversion restartreplace
Source: C:\Users\Naveen Hegde\Desktop\CybernetCtrl\CybernetCtrl.Server\bin\Release\MetroFramework.dll; DestDir: {app}; Flags: ignoreversion restartreplace
Source: C:\Users\Naveen Hegde\Desktop\CybernetCtrl\CybernetCtrl.Server\bin\Release\MetroFramework.Fonts.dll; DestDir: {app}; Flags: ignoreversion restartreplace
Source: C:\Users\Naveen Hegde\Desktop\CybernetCtrl\CybernetCtrl.Server\bin\Release\System.Data.SQLite.dll; DestDir: {app}; Flags: ignoreversion restartreplace

; scripts
Source: C:\Users\Naveen Hegde\Desktop\CybernetCtrl\CybernetCtrl.Server\bin\Release\Scripts\Ports.vbs; DestDir: {app}\Scripts; Flags: ignoreversion restartreplace
Source: C:\Users\Naveen Hegde\Desktop\CybernetCtrl\CybernetCtrl.Server\bin\Release\Scripts\Process.vbs; DestDir: {app}\Scripts; Flags: ignoreversion restartreplace

; database files
Source: C:\Users\Naveen Hegde\Desktop\CybernetCtrl\CybernetCtrl.Server\bin\Release\blocked.s3db; DestDir: {app}; Flags: ignoreversion restartreplace
Source: C:\Users\Naveen Hegde\Desktop\CybernetCtrl\CybernetCtrl.Server\bin\Release\blocked_server.s3db; DestDir: {app}; Flags: ignoreversion restartreplace
Source: C:\Users\Naveen Hegde\Desktop\CybernetCtrl\CybernetCtrl.Server\bin\Release\db.s3db; DestDir: {app}; Flags: ignoreversion restartreplace
Source: C:\Users\Naveen Hegde\Desktop\CybernetCtrl\CybernetCtrl.Server\bin\Release\server.s3db; DestDir: {app}; Flags: ignoreversion restartreplace
Source: C:\Users\Naveen Hegde\Desktop\CybernetCtrl\CybernetCtrl.Server\bin\Release\visited.s3db; DestDir: {app}; Flags: ignoreversion restartreplace
Source: C:\Users\Naveen Hegde\Desktop\CybernetCtrl\CybernetCtrl.Server\bin\Release\visited_server.s3db; DestDir: {app}; Flags: ignoreversion restartreplace

Source: C:\Users\Naveen Hegde\Desktop\CybernetCtrl\LICENSE.txt; DestDir: {app}; Flags: ignoreversion restartreplace
Source: C:\Users\Naveen Hegde\Desktop\CybernetCtrl\CybernetCtrl.Server\bin\Release\videomaker.bat; DestDir: {app}; Flags: ignoreversion restartreplace

;[Registry]
;Root: HKLM; Subkey: "SOFTWARE\Microsoft\Windows\CurrentVersion\Run"; ValueType: string; ValueName: "CybernetCtrl.Client"; ValueData: """{app}\CybernetCtrl.Client.exe"""; Flags: uninsdeletevalue

;[run]
;Filename: {sys}\sc.exe; Parameters: "create CybernetCtrlLauncher DisplayName= ""Cybernet-Ctrl Launcher"" start= auto binPath= ""{app}\CybernetCtrl.Launcher.exe""" ; Flags: runhidden

;[UninstallRun]
;Filename: {sys}\sc.exe; Parameters: "stop CybernetCtrlLauncher" ; Flags: runhidden
;Filename: {sys}\sc.exe; Parameters: "delete CybernetCtrlLauncher" ; Flags: runhidden
