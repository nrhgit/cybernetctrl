[Setup]
AppName=Cybernet-Ctrl.Client
AppVerName=Cybernet-Ctrl.Client
DefaultDirName={pf32}\Cybernet-Ctrl.Client
DefaultGroupName=Cybernet-Ctrl
UninstallDisplayIcon={app}\CybernetCtrl.Client.App.exe
OutputDir=C:\Users\Naveen Hegde\Desktop\CybernetCtrl\Build
OutputBaseFilename=CybernetCtrl.Client
ChangesAssociations=yes
Compression=lzma2
SolidCompression=yes
VersionInfoVersion=1.00
VersionInfoTextVersion=1.00
VersionInfoProductName=Cybernet-Ctrl
VersionInfoProductVersion=1.00
PrivilegesRequired=admin
LicenseFile=C:\Users\Naveen Hegde\Desktop\CybernetCtrl\LICENSE.txt
AlwaysRestart=yes

[Icons]
Name: {group}\Cybernet-Ctrl.Client; Filename: {app}\CybernetCtrl.Client.App.exe

[Files]

; Application Files
Source: C:\Users\Naveen Hegde\Desktop\CybernetCtrl\CybernetCtrl.Client.App\bin\Release\CybernetCtrl.Client.App.exe; DestDir: {app}; Flags: ignoreversion restartreplace
Source: C:\Users\Naveen Hegde\Desktop\CybernetCtrl\CybernetCtrl.Client\bin\Release\CybernetCtrl.Client.exe; DestDir: {app}\lib; Flags: ignoreversion restartreplace
Source: C:\Users\Naveen Hegde\Desktop\CybernetCtrl\CybernetCtrl.Client\bin\Release\CybernetCtrl.Uninstall.exe; DestDir: {app}; Flags: ignoreversion restartreplace
Source: C:\Users\Naveen Hegde\Desktop\CybernetCtrl\CybernetCtrl.Client\bin\Release\Wt.exe; DestDir: {app}; Flags: ignoreversion restartreplace
Source: C:\Users\Naveen Hegde\Desktop\CybernetCtrl\CybernetCtrl.Client\bin\Release\CybernetCtrl.Common.dll; DestDir: {app}; Flags: ignoreversion restartreplace
Source: C:\Users\Naveen Hegde\Desktop\CybernetCtrl\CybernetCtrl.Client\bin\Release\CybernetCtrl.Common.dll; DestDir: {app}\lib; Flags: ignoreversion restartreplace
Source: C:\Users\Naveen Hegde\Desktop\CybernetCtrl\CybernetCtrl.Client\bin\Release\CybernetCtrl.Common.Forms.dll; DestDir: {app}\lib; Flags: ignoreversion restartreplace
Source: C:\Users\Naveen Hegde\Desktop\CybernetCtrl\CybernetCtrl.Client\bin\Release\CybernetCtrl.Database.dll; DestDir: {app}\lib; Flags: ignoreversion restartreplace
Source: C:\Users\Naveen Hegde\Desktop\CybernetCtrl\CybernetCtrl.Client\bin\Release\CybernetCtrl.License.dll; DestDir: {app}\lib; Flags: ignoreversion restartreplace
Source: C:\Users\Naveen Hegde\Desktop\CybernetCtrl\CybernetCtrl.Client\bin\Release\CybernetCtrl.Message.dll; DestDir: {app}\lib; Flags: ignoreversion restartreplace
Source: C:\Users\Naveen Hegde\Desktop\CybernetCtrl\CybernetCtrl.Client\bin\Release\CybernetCtrl.Modal.dll; DestDir: {app}\lib; Flags: ignoreversion restartreplace
Source: C:\Users\Naveen Hegde\Desktop\CybernetCtrl\CybernetCtrl.Client\bin\Release\CybernetCtrl.TcpComm.dll; DestDir: {app}\lib; Flags: ignoreversion restartreplace
Source: C:\Users\Naveen Hegde\Desktop\CybernetCtrl\CybernetCtrl.Client\bin\Release\CybernetCtrl.WebCtrl.dll; DestDir: {app}\lib; Flags: ignoreversion restartreplace
Source: C:\Users\Naveen Hegde\Desktop\CybernetCtrl\CybernetCtrl.Client\bin\Release\FiddlerCore.dll; DestDir: {app}\lib; Flags: ignoreversion restartreplace
Source: C:\Users\Naveen Hegde\Desktop\CybernetCtrl\CybernetCtrl.Client\bin\Release\MetroFramework.Design.dll; DestDir: {app}\lib; Flags: ignoreversion restartreplace
Source: C:\Users\Naveen Hegde\Desktop\CybernetCtrl\CybernetCtrl.Client\bin\Release\MetroFramework.dll; DestDir: {app}\lib; Flags: ignoreversion restartreplace
Source: C:\Users\Naveen Hegde\Desktop\CybernetCtrl\CybernetCtrl.Client\bin\Release\MetroFramework.Fonts.dll; DestDir: {app}\lib; Flags: ignoreversion restartreplace
Source: C:\Users\Naveen Hegde\Desktop\CybernetCtrl\CybernetCtrl.Client\bin\Release\System.Data.SQLite.dll; DestDir: {app}\lib; Flags: ignoreversion restartreplace
Source: C:\Users\Naveen Hegde\Desktop\CybernetCtrl\CybernetCtrl.Client\bin\Release\ICSharpCode.SharpZipLib.dll; DestDir: {app}\lib; Flags: ignoreversion restartreplace
; Fiddler core license
Source: C:\Users\Naveen Hegde\Desktop\CybernetCtrl\CybernetCtrl.Client\bin\Release\License.txt; DestDir: {app}\lib; Flags: ignoreversion restartreplace
Source: C:\Users\Naveen Hegde\Desktop\CybernetCtrl\LICENSE.txt; DestDir: {app}; Flags: ignoreversion restartreplace

; scripts
Source: C:\Users\Naveen Hegde\Desktop\CybernetCtrl\CybernetCtrl.Client.App\bin\Release\Scripts\Ports.vbs; DestDir: {app}\Scripts; Flags: ignoreversion restartreplace
Source: C:\Users\Naveen Hegde\Desktop\CybernetCtrl\CybernetCtrl.Client.App\bin\Release\Scripts\Process.vbs; DestDir: {app}\Scripts; Flags: ignoreversion restartreplace
Source: C:\Users\Naveen Hegde\Desktop\CybernetCtrl\CybernetCtrl.Client\bin\Release\Scripts\Ports.vbs; DestDir: {app}\lib\Scripts; Flags: ignoreversion restartreplace
Source: C:\Users\Naveen Hegde\Desktop\CybernetCtrl\CybernetCtrl.Client\bin\Release\Scripts\Process.vbs; DestDir: {app}\lib\Scripts; Flags: ignoreversion restartreplace

; database files
Source: C:\Users\Naveen Hegde\Desktop\CybernetCtrl\CybernetCtrl.Client\bin\Release\blocked.s3db; DestDir: {app}\lib; Flags: ignoreversion restartreplace
Source: C:\Users\Naveen Hegde\Desktop\CybernetCtrl\CybernetCtrl.Client\bin\Release\blocked_server.s3db; DestDir: {app}\lib; Flags: ignoreversion restartreplace
Source: C:\Users\Naveen Hegde\Desktop\CybernetCtrl\CybernetCtrl.Client\bin\Release\db.s3db; DestDir: {app}\lib; Flags: ignoreversion restartreplace
Source: C:\Users\Naveen Hegde\Desktop\CybernetCtrl\CybernetCtrl.Client\bin\Release\server.s3db; DestDir: {app}\lib; Flags: ignoreversion restartreplace
Source: C:\Users\Naveen Hegde\Desktop\CybernetCtrl\CybernetCtrl.Client\bin\Release\visited.s3db; DestDir: {app}\lib; Flags: ignoreversion restartreplace
Source: C:\Users\Naveen Hegde\Desktop\CybernetCtrl\CybernetCtrl.Client\bin\Release\visited_server.s3db; DestDir: {app}\lib; Flags: ignoreversion restartreplace

;[Registry]
;Root: HKLM; Subkey: "SOFTWARE\Microsoft\Windows\CurrentVersion\Run"; ValueType: string; ValueName: "CybernetCtrl"; ValueData: """{app}\Wt.exe"""; Flags: uninsdeletevalue

[Run]
Filename: "schtasks.exe"; Parameters: "/create /sc onlogon /tn CybernetCtrl /rl highest /tr ""\""{app}\CybernetCtrl.Client.App.exe\"" /F";

[UninstallRun]
Filename: {app}\CybernetCtrl.Uninstall.exe