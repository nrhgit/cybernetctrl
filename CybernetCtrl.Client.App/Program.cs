﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using CybernetCtrl.Common;

namespace CybernetCtrl.Client.App {
    static class Program {

        private static string libpath = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) + @"\CybernetCtrl\lib";

        [STAThread]
        static void Main() {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            Application.ThreadException += new System.Threading.ThreadExceptionEventHandler(Application_ThreadException);
            AppDomain.CurrentDomain.UnhandledException += new UnhandledExceptionEventHandler(CurrentDomain_UnhandledException);

            string localLib = Application.StartupPath + @"\lib";
            string localUpdate = localLib + @"\update";
            
            Copy(localLib, libpath, false);
            Copy(localUpdate, libpath, true);
            
            if (Directory.Exists(localUpdate)) {
                Directory.Delete(localUpdate, true);
            }

            AppDomain.CurrentDomain.AssemblyResolve += new ResolveEventHandler(CurrentDomain_AssemblyResolve);
            AppDomain.CurrentDomain.TypeResolve += new ResolveEventHandler(CurrentDomain_TypeResolve);

            SetFirewallSettings();

            if (IsApplicationAlreadyOpen()) {
                return;
            }

            Form form = (Form)Activator.CreateInstance("CybernetCtrl.Client", "CybernetCtrl.Client.ClientMainForm").Unwrap();
            Application.Run(form);
        }

        private static void SetFirewallSettings() {
            WinFirewall.EnablePorts();

            WinFirewall.ActivateApplication("CybernetCtrl Client App");
        }

        private static Assembly CurrentDomain_TypeResolve(object sender, ResolveEventArgs args) {
            return Assembly.LoadFile(libpath + @"\CybernetCtrl.Client.exe");
        }

        private static Assembly CurrentDomain_AssemblyResolve(object sender, ResolveEventArgs args) {
            if (String.IsNullOrEmpty(args.Name) == false) {
                string[] name = args.Name.Split(", ".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
                string exe = libpath + @"\" + name[0] + ".exe";
                string dll = libpath + @"\" + name[0] + ".dll";

                if (File.Exists(dll)) {
                    return Assembly.LoadFile(dll);
                }

                if (File.Exists(exe)) {
                    return Assembly.LoadFile(exe);
                }
            }

            return null;
        }

        private static void Copy(string sourceDir, string targetDir, bool overwrite) {
            if (Directory.Exists(sourceDir) == false) {
                return;
            }

            if (Directory.Exists(targetDir) == false) {
                Directory.CreateDirectory(targetDir);
            }

            foreach (var file in Directory.GetFiles(sourceDir)) {
                string destFile = Path.Combine(targetDir, Path.GetFileName(file));
                if (File.Exists(destFile)) {
                    if (overwrite) {
                        File.Copy(file, destFile, true);
                    }
                } else {
                    File.Copy(file, destFile);
                }
            }

            foreach (var directory in Directory.GetDirectories(sourceDir)) {
                Copy(directory, Path.Combine(targetDir, Path.GetFileName(directory)), overwrite);
            }
        }

        private static bool IsApplicationAlreadyOpen() {
            Process currentProcess = Process.GetCurrentProcess();
            Process[] processes = Process.GetProcessesByName(currentProcess.ProcessName);
            foreach (Process process in processes) {
                if (process.Id != currentProcess.Id && process.MainModule.FileName == currentProcess.MainModule.FileName) {
                    return true;
                }
            }

            return false;
        }

        static void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e) {
            LogException((Exception)e.ExceptionObject);
        }

        static void Application_ThreadException(object sender, System.Threading.ThreadExceptionEventArgs e) {
            LogException(e.Exception);
        }

        private static void LogException(Exception exc) {
            if (exc != null) {
                if (Logger.IsLogEnabled) {
                    Logger.Log(exc.Message + Environment.NewLine + exc.StackTrace, LogType.Error, ApplicationType.Client);
                } else {
                    MessageBox.Show(exc.Message + Environment.NewLine + exc.StackTrace + (exc.InnerException !=null ? exc.InnerException.Message : ""), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }
    }
}
