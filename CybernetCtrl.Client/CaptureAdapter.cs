﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;
using System.IO;
using System.Net.Sockets;
using System.Net;
using System.Threading;
using System.Windows.Forms;
using System.Drawing.Imaging;
using CybernetCtrl.Common;

namespace CybernetCtrl.Client {
    internal class CaptureAdapter {

        private static bool stop = false;

        public static void Start(string ip, int port) {
            stop = false;

            Thread thread = new Thread(new ParameterizedThreadStart(SenderThread));
            thread.IsBackground = true;
            thread.Start(new List<object>() { ip, port });
        }

        public static void Stop() {
            stop = true;
        }

        private static void SenderThread(object o) {
            Logger.Log("Starting image capture thread.", LogType.Information, ApplicationType.Client);

            List<object> args = (List<object>)o;
            string ip = (string)args[0];
            int port = (int)args[1];

            IPEndPoint ipep = new IPEndPoint(IPAddress.Parse(ip), port);

            Socket client = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);

            client.Connect(ipep);

            Logger.Log("Connected to IP:" + ip + " Port:" + port, LogType.Information, ApplicationType.Client);

            while (true) {

                if (stop) return;

                if (client.Connected == false) {
                    Logger.Log("Closing connection", LogType.Information, ApplicationType.Client);

                    client.Close();
                    return;
                }

                try {
                    using (MemoryStream m = new MemoryStream()) {
                        GetScreen(m);

                        byte[] data = m.ToArray();

                        Logger.Log("Sending image.", LogType.Information, ApplicationType.Client);
                        client.Send(data);
                        Logger.Log("Image successfully sent.", LogType.Information, ApplicationType.Client);
                    }
                } catch (Exception exc) {
                    Logger.Log("Error occured while sending image." + exc.Message, LogType.Error, ApplicationType.Client);
                }

                Thread.Sleep(500);
            }
        }

        private static void GetScreen(MemoryStream memory) {
            Rectangle bounds = Screen.GetBounds(Point.Empty);
            using (Bitmap bitmap = new Bitmap(Screen.PrimaryScreen.Bounds.Width, Screen.PrimaryScreen.Bounds.Height)) {
                using (Graphics g = Graphics.FromImage(bitmap)) {
                    g.CopyFromScreen(Point.Empty, Point.Empty, bounds.Size);
                }

                bitmap.Save(memory, ImageFormat.Jpeg);
                memory.Position = 0;
            }
        }
    }
}