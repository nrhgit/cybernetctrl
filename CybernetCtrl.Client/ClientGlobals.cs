﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Timers;

namespace CybernetCtrl.Client {
    public static class ClientGlobals {
        public static event EventHandler<NotificationEventArgs> ShowUserNotification;

        public static void ShowNotification(string message) {
            if (ShowUserNotification != null) {
                ShowUserNotification(null, new NotificationEventArgs() { Message = message });
            }
        }
    }

    public class NotificationEventArgs : EventArgs {
        public string Message { get; set; }
    }
}