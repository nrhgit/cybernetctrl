using System.Collections.Generic;
using System.Threading;
using System;
using System.Net;
using CybernetCtrl.Modal;

namespace CybernetCtrl.Client
{
    internal class FileLimiter
    {
        private static Dictionary<string, FileSizeCheckStatusType> status = new Dictionary<string, FileSizeCheckStatusType>();
        private static readonly object _lock = new object();
        private static List<string> extensions = new List<string>() { ".zip", ".tar.xz", ".tar.gz", ".tgz", ".tar", ".rar", ".jar", ".gzip", ".cab", ".7z", ".exe", ".msi", ".iso", ".wri", ".dll", ".war", ".torrent", ".bin", ".dmg", ".mdf", ".vcd", ".vhd", ".pkg", ".rpm", ".sys", ".dwg", ".dxf", ".app", ".pdb", ".mdb", ".dbf", ".db", ".accdb", ".wmv", ".vob", ".swf", ".rm", ".mpg", ".mp4", ".mov", ".m4v", ".flv", ".avi", ".mp3", ".mpa", ".wav", ".wma", ".dat" };

        public static bool IsKnowFileType(string fileUrl)
        {
            string url = fileUrl.ToLower();
            foreach (string extension in extensions)
            {
                if (url.EndsWith(extension))
                {
                    return true;
                }
            }

            return false;
        }

        public static FileSizeCheckStatusType CheckLimit(string fileUrl)
        {
            decimal sizeLimit = UserFileLimit.Limit;

            string url = fileUrl.ToLower();

            if (sizeLimit == 0) return FileSizeCheckStatusType.Restricted;

            if (status.ContainsKey(url) == false)
            {
                status.Add(url, FileSizeCheckStatusType.NoResponse);
            }
            else
            {
                status[url] = FileSizeCheckStatusType.NoResponse;
            }

            Thread downloadThread = new Thread(new ParameterizedThreadStart(DownloadThread));
            downloadThread.IsBackground = true;
            downloadThread.Start(new List<object>() { url, sizeLimit });

            // Wait for 30 seconds and then abort if no response received
            if (downloadThread.Join(new TimeSpan(0, 0, 30)) == false)
            {
                downloadThread.Abort();
            }

            return status[url];
        }

        private static void DownloadThread(object o)
        {
            lock (_lock)
            {
                List<object> os = (List<object>)o;
                string url = (string)os[0];
                decimal sizeLimit = (decimal)os[1];
                decimal sizeLimitBytes = sizeLimit * (1014 * 1014);

                HttpWebRequest webRequest = (HttpWebRequest)HttpWebRequest.Create(url);
                webRequest.Method = "HEAD";
                HttpWebResponse response = (HttpWebResponse)webRequest.GetResponse();

                using (System.IO.StreamReader reader = new System.IO.StreamReader(response.GetResponseStream()))
                {
                    status[url] = (response.ContentLength <= sizeLimitBytes ? FileSizeCheckStatusType.InLimit : FileSizeCheckStatusType.OutsizeLimit);
                }
            }
        }
    }

    internal enum FileSizeCheckStatusType
    {
        Restricted,
        InLimit,
        OutsizeLimit,
        NoResponse
    }
}

