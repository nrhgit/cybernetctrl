﻿namespace CybernetCtrl.Client
{
    partial class ClientMainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ClientMainForm));
            this.notifyIcon1 = new System.Windows.Forms.NotifyIcon(this.components);
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.mnuWebsitesVisited = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuBlockedWebsites = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuAllowedWebsites = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuNotAllowedWebsites = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuWebsitesRequested = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.mnuServerIPAddress = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuPurgeHistoryData = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuChangeAdminPassword = new System.Windows.Forms.ToolStripMenuItem();
            this.sepMode = new System.Windows.Forms.ToolStripSeparator();
            this.mnuMode = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuCurrentMode = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.mnuManageLogging = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuLogging = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuOpenLogFolder = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.mnuManageProcesses = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuOpenRegedit = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuOpenMSConfig = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuOpenInternetOptions = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuAllowRundll32 = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuProgramsAndFeatures = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator6 = new System.Windows.Forms.ToolStripSeparator();
            this.mnuRegisterProduct = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuExit = new System.Windows.Forms.ToolStripMenuItem();
            this.contextMenuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // notifyIcon1
            // 
            this.notifyIcon1.BalloonTipIcon = System.Windows.Forms.ToolTipIcon.Info;
            this.notifyIcon1.ContextMenuStrip = this.contextMenuStrip1;
            this.notifyIcon1.Icon = ((System.Drawing.Icon)(resources.GetObject("notifyIcon1.Icon")));
            this.notifyIcon1.Text = "Cybernet-Ctrl Client";
            this.notifyIcon1.Visible = true;
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuWebsitesVisited,
            this.mnuBlockedWebsites,
            this.mnuAllowedWebsites,
            this.mnuNotAllowedWebsites,
            this.mnuWebsitesRequested,
            this.toolStripSeparator2,
            this.mnuServerIPAddress,
            this.mnuPurgeHistoryData,
            this.mnuChangeAdminPassword,
            this.sepMode,
            this.mnuMode,
            this.mnuCurrentMode,
            this.toolStripSeparator3,
            this.mnuManageLogging,
            this.toolStripSeparator5,
            this.mnuManageProcesses,
            this.toolStripSeparator6,
            this.mnuRegisterProduct,
            this.mnuExit});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(226, 364);
            this.contextMenuStrip1.Opening += new System.ComponentModel.CancelEventHandler(this.contextMenuStrip1_Opening);
            // 
            // mnuWebsitesVisited
            // 
            this.mnuWebsitesVisited.Name = "mnuWebsitesVisited";
            this.mnuWebsitesVisited.Size = new System.Drawing.Size(225, 22);
            this.mnuWebsitesVisited.Text = "Visited Websites History";
            this.mnuWebsitesVisited.Click += new System.EventHandler(this.mnuWebsitesVisited_Click);
            // 
            // mnuBlockedWebsites
            // 
            this.mnuBlockedWebsites.Name = "mnuBlockedWebsites";
            this.mnuBlockedWebsites.Size = new System.Drawing.Size(225, 22);
            this.mnuBlockedWebsites.Text = "Blocked Websites History";
            this.mnuBlockedWebsites.Click += new System.EventHandler(this.mnuBlockedWebsites_Click);
            // 
            // mnuAllowedWebsites
            // 
            this.mnuAllowedWebsites.Name = "mnuAllowedWebsites";
            this.mnuAllowedWebsites.Size = new System.Drawing.Size(225, 22);
            this.mnuAllowedWebsites.Text = "Allowed Websites";
            this.mnuAllowedWebsites.Click += new System.EventHandler(this.mnuAllowedWebsites_Click);
            // 
            // mnuNotAllowedWebsites
            // 
            this.mnuNotAllowedWebsites.Name = "mnuNotAllowedWebsites";
            this.mnuNotAllowedWebsites.Size = new System.Drawing.Size(225, 22);
            this.mnuNotAllowedWebsites.Text = "Blocked Websites";
            this.mnuNotAllowedWebsites.Click += new System.EventHandler(this.mnuNotAllowedWebsites_Click);
            // 
            // mnuWebsitesRequested
            // 
            this.mnuWebsitesRequested.Name = "mnuWebsitesRequested";
            this.mnuWebsitesRequested.Size = new System.Drawing.Size(225, 22);
            this.mnuWebsitesRequested.Text = "Requested Websites";
            this.mnuWebsitesRequested.Click += new System.EventHandler(this.mnuWebsitesRequested_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(222, 6);
            // 
            // mnuServerIPAddress
            // 
            this.mnuServerIPAddress.Name = "mnuServerIPAddress";
            this.mnuServerIPAddress.Size = new System.Drawing.Size(225, 22);
            this.mnuServerIPAddress.Text = "Server IP Address";
            this.mnuServerIPAddress.Click += new System.EventHandler(this.mnuServerIPAddress_Click);
            // 
            // mnuPurgeHistoryData
            // 
            this.mnuPurgeHistoryData.Name = "mnuPurgeHistoryData";
            this.mnuPurgeHistoryData.Size = new System.Drawing.Size(225, 22);
            this.mnuPurgeHistoryData.Text = "Purge History Data";
            this.mnuPurgeHistoryData.Click += new System.EventHandler(this.mnuPurgeHistoryData_Click);
            // 
            // mnuChangeAdminPassword
            // 
            this.mnuChangeAdminPassword.Name = "mnuChangeAdminPassword";
            this.mnuChangeAdminPassword.Size = new System.Drawing.Size(225, 22);
            this.mnuChangeAdminPassword.Text = "Change Admin Password";
            this.mnuChangeAdminPassword.Click += new System.EventHandler(this.mnuChangeAdminPassword_Click);
            // 
            // sepMode
            // 
            this.sepMode.Name = "sepMode";
            this.sepMode.Size = new System.Drawing.Size(222, 6);
            // 
            // mnuMode
            // 
            this.mnuMode.Name = "mnuMode";
            this.mnuMode.Size = new System.Drawing.Size(225, 22);
            this.mnuMode.Text = "Enable Selective Allow Mode";
            this.mnuMode.Click += new System.EventHandler(this.mnuMode_Click);
            // 
            // mnuCurrentMode
            // 
            this.mnuCurrentMode.Name = "mnuCurrentMode";
            this.mnuCurrentMode.Size = new System.Drawing.Size(225, 22);
            this.mnuCurrentMode.Text = "What is my current mode ?";
            this.mnuCurrentMode.Click += new System.EventHandler(this.mnuCurrentMode_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(222, 6);
            // 
            // mnuManageLogging
            // 
            this.mnuManageLogging.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuLogging,
            this.mnuOpenLogFolder});
            this.mnuManageLogging.Name = "mnuManageLogging";
            this.mnuManageLogging.Size = new System.Drawing.Size(225, 22);
            this.mnuManageLogging.Text = "Logging";
            this.mnuManageLogging.Click += new System.EventHandler(this.mnuLogging_Click);
            // 
            // mnuLogging
            // 
            this.mnuLogging.Name = "mnuLogging";
            this.mnuLogging.Size = new System.Drawing.Size(157, 22);
            this.mnuLogging.Text = "Enable Logging";
            this.mnuLogging.Click += new System.EventHandler(this.mnuLogging_Click);
            // 
            // mnuOpenLogFolder
            // 
            this.mnuOpenLogFolder.Name = "mnuOpenLogFolder";
            this.mnuOpenLogFolder.Size = new System.Drawing.Size(157, 22);
            this.mnuOpenLogFolder.Text = "Open log folder";
            this.mnuOpenLogFolder.Click += new System.EventHandler(this.mnuOpenLogFolder_Click);
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size(222, 6);
            // 
            // mnuManageProcesses
            // 
            this.mnuManageProcesses.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuOpenRegedit,
            this.mnuOpenMSConfig,
            this.mnuOpenInternetOptions,
            this.mnuAllowRundll32,
            this.mnuProgramsAndFeatures});
            this.mnuManageProcesses.Name = "mnuManageProcesses";
            this.mnuManageProcesses.Size = new System.Drawing.Size(225, 22);
            this.mnuManageProcesses.Text = "Manage Processes";
            // 
            // mnuOpenRegedit
            // 
            this.mnuOpenRegedit.Name = "mnuOpenRegedit";
            this.mnuOpenRegedit.Size = new System.Drawing.Size(195, 22);
            this.mnuOpenRegedit.Text = "Allow Regedit";
            this.mnuOpenRegedit.Click += new System.EventHandler(this.mnuOpenRegedit_Click);
            // 
            // mnuOpenMSConfig
            // 
            this.mnuOpenMSConfig.Name = "mnuOpenMSConfig";
            this.mnuOpenMSConfig.Size = new System.Drawing.Size(195, 22);
            this.mnuOpenMSConfig.Text = "Allow MS Config";
            this.mnuOpenMSConfig.Click += new System.EventHandler(this.mnuOpenMSConfig_Click);
            // 
            // mnuOpenInternetOptions
            // 
            this.mnuOpenInternetOptions.Name = "mnuOpenInternetOptions";
            this.mnuOpenInternetOptions.Size = new System.Drawing.Size(195, 22);
            this.mnuOpenInternetOptions.Text = "Open Internet Options";
            this.mnuOpenInternetOptions.Click += new System.EventHandler(this.mnuOpenInternetOptions_Click);
            // 
            // mnuAllowRundll32
            // 
            this.mnuAllowRundll32.Name = "mnuAllowRundll32";
            this.mnuAllowRundll32.Size = new System.Drawing.Size(195, 22);
            this.mnuAllowRundll32.Text = "Allow Rundll32";
            this.mnuAllowRundll32.Click += new System.EventHandler(this.mnuAllowRundll32_Click);
            // 
            // mnuProgramsAndFeatures
            // 
            this.mnuProgramsAndFeatures.Name = "mnuProgramsAndFeatures";
            this.mnuProgramsAndFeatures.Size = new System.Drawing.Size(195, 22);
            this.mnuProgramsAndFeatures.Text = "Programs and Features";
            this.mnuProgramsAndFeatures.Click += new System.EventHandler(this.mnuProgramsAndFeatures_Click);
            // 
            // toolStripSeparator6
            // 
            this.toolStripSeparator6.Name = "toolStripSeparator6";
            this.toolStripSeparator6.Size = new System.Drawing.Size(222, 6);
            // 
            // mnuRegisterProduct
            // 
            this.mnuRegisterProduct.Name = "mnuRegisterProduct";
            this.mnuRegisterProduct.Size = new System.Drawing.Size(225, 22);
            this.mnuRegisterProduct.Text = "Register Product";
            this.mnuRegisterProduct.Click += new System.EventHandler(this.mnuRegisterProduct_Click);
            // 
            // mnuExit
            // 
            this.mnuExit.Name = "mnuExit";
            this.mnuExit.Size = new System.Drawing.Size(225, 22);
            this.mnuExit.Text = "Exit";
            this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
            // 
            // ClientMainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(116, 16);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "ClientMainForm";
            this.Text = "Client";
            this.Load += new System.EventHandler(this.ClientForm_Load);
            this.Activated += new System.EventHandler(this.ClientForm_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.ClientMainForm_FormClosing);
            this.contextMenuStrip1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.NotifyIcon notifyIcon1;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem mnuServerIPAddress;
        private System.Windows.Forms.ToolStripMenuItem mnuWebsitesVisited;
        private System.Windows.Forms.ToolStripMenuItem mnuBlockedWebsites;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripMenuItem mnuChangeAdminPassword;
        private System.Windows.Forms.ToolStripMenuItem mnuAllowedWebsites;
        private System.Windows.Forms.ToolStripMenuItem mnuWebsitesRequested;
        private System.Windows.Forms.ToolStripMenuItem mnuPurgeHistoryData;
        private System.Windows.Forms.ToolStripMenuItem mnuMode;
        private System.Windows.Forms.ToolStripMenuItem mnuNotAllowedWebsites;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripMenuItem mnuExit;
        private System.Windows.Forms.ToolStripSeparator sepMode;
        private System.Windows.Forms.ToolStripMenuItem mnuCurrentMode;
        private System.Windows.Forms.ToolStripMenuItem mnuRegisterProduct;
        private System.Windows.Forms.ToolStripMenuItem mnuManageLogging;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
        private System.Windows.Forms.ToolStripMenuItem mnuManageProcesses;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator6;
        private System.Windows.Forms.ToolStripMenuItem mnuOpenRegedit;
        private System.Windows.Forms.ToolStripMenuItem mnuOpenMSConfig;
        private System.Windows.Forms.ToolStripMenuItem mnuLogging;
        private System.Windows.Forms.ToolStripMenuItem mnuOpenLogFolder;
        private System.Windows.Forms.ToolStripMenuItem mnuOpenInternetOptions;
        private System.Windows.Forms.ToolStripMenuItem mnuAllowRundll32;
        private System.Windows.Forms.ToolStripMenuItem mnuProgramsAndFeatures;
    }
}

