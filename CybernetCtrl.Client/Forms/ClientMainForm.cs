﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using CybernetCtrl.Modal;
using CybernetCtrl.TcpComm;
using CybernetCtrl.Common;
using CybernetCtrl.Message;
using CybernetCtrl.Database;
using CybernetCtrl.WebCtrl;
using System.Runtime.InteropServices;
using System.IO;
using System.Diagnostics;
using System.Text.RegularExpressions;
using CybernetCtrl.Common.Forms;
using Microsoft.Win32;

namespace CybernetCtrl.Client {
    public partial class ClientMainForm : Form {
        private string serverIP = string.Empty;
        private bool serverIPChanged = true;
        private AccessModeType modeType = AccessMode.ModeType;
        private string localDir = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) + @"\" + Settings.LocalDir;
        private string settingsPath = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) + @"\" + Settings.LocalDir + (@"\winprocess.utr");

        public ClientMainForm() {
            InitializeComponent();

            Logger.Init();

            mnuLogging.Text = Logger.IsLogEnabled ? "Disable Logging" : "Enable Logging";

            Logger.Log("Starting CybernetCtrl.Client", LogType.Information, ApplicationType.Client);

            CreateRequiredRegEntries();
            
            DisableSystemProcesses();

            SetFirewallSettings();

            ClientGlobals.ShowUserNotification += ClientGlobals_ShowUserNotification;
        }

        private void CreateRequiredRegEntries() {
            RegistryKey root = Registry.CurrentUser;
            
            // internet options
            string key = @"Software\Policies\Microsoft\Internet Explorer";
            RegistryAdapter.CreateRegistryKey(root, key);

            key = @"Software\Policies\Microsoft\Internet Explorer\Restrictions";
            RegistryAdapter.CreateRegistryKey(root, key);

            RegistryAdapter.CreateRegistryKeyValue(root, key, "NoBrowserOptions", 1);

            key = @"Software\Microsoft\Windows\CurrentVersion\Policies\Explorer";
            RegistryAdapter.CreateRegistryKey(root, key);

            RegistryAdapter.CreateRegistryKeyValue(root, key, "DisallowCpl", 1, RegistryValueKind.DWord);

            // Disallow CPL
            key = @"Software\Microsoft\Windows\CurrentVersion\Policies\Explorer\DisallowCpl";
            RegistryAdapter.CreateRegistryKey(root, key);

            // 1. internet options
            RegistryAdapter.CreateRegistryKeyValue(root, key, "InetOptions", "Internet Options");

            // 2. add-remove programs
            RegistryAdapter.CreateRegistryKeyValue(root, key, "uninstall", "Programs and Features");

            // 3. firewall
            RegistryAdapter.CreateRegistryKeyValue(root, key, "firewall", "Windows Firewall");
        }

        private void DisableSystemProcesses() {
            if (Directory.Exists(localDir) == false) {
                Directory.CreateDirectory(localDir);
            }

            ProcessConfig processConfig = new ProcessConfig();

            LoadSave.Save(typeof(ProcessConfig), processConfig, settingsPath);
        }

        private void SetFirewallSettings() {
            WinFirewall.EnablePorts();

            WinFirewall.ActivateApplication("CybernetCtrl Client");
        }
        
        private void ClientForm_Load(object sender, EventArgs e) {
            serverIP = ServerIP.GetServerIP();

            if (String.IsNullOrEmpty(serverIP) == false) {
                TcpCommunicator.Instance.RegisterServer(serverIP);
            }

#if WATCHER
            WatcherAdapter.Start("CybernetCtrl.Client.App");
#endif

            InitializeThreads();

            WebNotifier.WebVisited += WebNotifier_WebVisited;
        }

        private void ClientMainForm_FormClosing(object sender, FormClosingEventArgs e) {
            Logger.Log("Closing CybernetCtrl.Client", LogType.Information, ApplicationType.Client);

            WebNotifier.ShutDown();
        }

        private void WebNotifier_WebVisited(object sender, WebVisitedEventArgs e) {
            string url = e.Url;

            Logger.Log("Visiting: " + url, LogType.Information, ApplicationType.Client);

            Logger.Log("Mode type: " + modeType, LogType.Information, ApplicationType.Client);

            switch (modeType) {
                case AccessModeType.SelectiveAllow:
                    bool allow = false;
                    string allowedUrl = url;
#if LICENSING
                    if (CybernetCtrl.License.LicenseManager.HasClientLicense) {
                        allowedUrl = UrlAllowed.IsAllowedUrl(url, DomainParser.GetDomain(url));
                        allow = (String.IsNullOrEmpty(allowedUrl) == false);
                    } else {
                        string trialUrl = CybernetCtrl.License.LicenseManager.TrialWebsite;
                        allow = trialUrl.Equals(url, StringComparison.OrdinalIgnoreCase);
                        licenseError = (allow == false);
                    }
#elif FIXED_LICENSING
                    if (CybernetCtrl.License.FixedLicenseManager.HasClientLicense) {
                        allowedUrl = UrlAllowed.IsAllowedUrl(url, DomainParser.GetDomain(url));
                        allow = (String.IsNullOrEmpty(allowedUrl) == false);
                    }
#else
                    allowedUrl = UrlAllowed.IsAllowedUrl(url, DomainParser.GetDomain(url));
                    allow = (String.IsNullOrEmpty(allowedUrl) == false);
#endif
                    if (allow) {
                        if (FileLimiter.IsKnowFileType(url)) {
                            allow = UserFileLimit.Limit > 0;
                        }
                    }

                    if (allow) {
                        Logger.Log("URL allowed: " + url, LogType.Information, ApplicationType.Client);

                        // does it belong to any group
                        UrlGroup group = null;
                        // TODO: check actuall connection status
                        if (String.IsNullOrEmpty(serverIP) == false) {
                            group = UrlAllowed.GetGroup(allowedUrl);
                        }

                        if (group == null) {
                            Logger.Log("Does not belong to any group. URL: " + url, LogType.Information, ApplicationType.Client);
                            UrlVisited urlVisited = new UrlVisited() { Title = e.Title, Url = e.Url, FullUrl = e.FullUrl, VisitedDateTime = InternalTimer.Instance.ServerDateTime };

                            urlVisited.Save();
                        } else {
                            Logger.Log("Belongs to group: " + group + " URL: " + url, LogType.Information, ApplicationType.Client);
                            bool sessionStarted = SessionAdapter.AllowVisit(allowedUrl, group);
                            if (sessionStarted) {
                                Logger.Log("Session started. URL: " + url, LogType.Information, ApplicationType.Client);
                                UrlVisited urlVisited = new UrlVisited() { Title = e.Title, Url = e.Url, FullUrl = e.FullUrl, VisitedDateTime = InternalTimer.Instance.ServerDateTime };

                                urlVisited.Save();
                            } else {
                                Logger.Log("Session blocked. URL: " + url, LogType.Information, ApplicationType.Client);
                                UrlBlockedDetails urlBlocked = new UrlBlockedDetails() { Title = e.Title, Url = e.Url, FullUrl = e.Url, VisitedDateTime = InternalTimer.Instance.ServerDateTime };

                                WebNotifier.Block(e.Session);

                                urlBlocked.Save();
                            }
                        }
                    } else {
                        Logger.Log("URL blocked: " + url, LogType.Information, ApplicationType.Client);

                        WebNotifier.Block(e.Session);

                        UrlBlockedDetails urlBlocked = new UrlBlockedDetails() { Title = e.Title, Url = e.Url, FullUrl = e.Url, VisitedDateTime = InternalTimer.Instance.ServerDateTime };

                        urlBlocked.Save();

                        NotifyUrlBlocked(url);
                    }

                    break;

                case AccessModeType.SelectiveBlock:
                    bool block = false;

#if LICENSING
                    if (CybernetCtrl.License.LicenseManager.HasClientLicense) {
                        block = UrlBlocked.IsBlockedUrl(url, DomainParser.GetDomain(url));
                    } else {
                        string trialUrl = CybernetCtrl.License.LicenseManager.TrialWebsite;
                        block = trialUrl.Equals(url, StringComparison.OrdinalIgnoreCase);
                    }
#elif FIXED_LICENSING
                    if (CybernetCtrl.License.FixedLicenseManager.HasClientLicense) {
                        block = UrlBlocked.IsBlockedUrl(url, DomainParser.GetDomain(url));
                    }
#else
                    block = UrlBlocked.IsBlockedUrl(url, DomainParser.GetDomain(url));
#endif
                    if (block == false) {
                        if (FileLimiter.IsKnowFileType(url)) {
                            block = (UserFileLimit.Limit > 0) == false;
                        }
                    }

                    if (block) {
                        Logger.Log("URL blocked: " + url, LogType.Information, ApplicationType.Client);

                        WebNotifier.Block(e.Session);

                        UrlBlockedDetails urlBlocked = new UrlBlockedDetails() { Title = e.Title, Url = e.Url, FullUrl = e.Url, VisitedDateTime = InternalTimer.Instance.ServerDateTime };

                        urlBlocked.Save();

                        NotifyUrlBlocked(url);
                    } else {
                        Logger.Log("URL allowed: " + url, LogType.Information, ApplicationType.Client);

                        UrlVisited urlVisited = new UrlVisited() { Title = e.Title, Url = e.Url, FullUrl = e.FullUrl, VisitedDateTime = InternalTimer.Instance.ServerDateTime };

                        urlVisited.Save();
                    }

                    break;
            }
        }

        private void ClientGlobals_ShowUserNotification(object sender, NotificationEventArgs e) {
            notifyIcon1.BalloonTipText = e.Message;
            notifyIcon1.ShowBalloonTip(3000);
        }

        private void NotifyUrlBlocked(string url) {
            notifyIcon1.BalloonTipText = "Blocked" + " : " + url;
            notifyIcon1.ShowBalloonTip(3000);
        }

        private void InitializeThreads() {
            Logger.Log("Initializing threads", LogType.Information, ApplicationType.Client);

            Thread aliveThread = new Thread(new ThreadStart(IsAliveThead));
            aliveThread.IsBackground = true;
            aliveThread.Start();

            Thread commandReceiverThread = new Thread(new ThreadStart(CommandReceiverThead));
            commandReceiverThread.IsBackground = true;
            commandReceiverThread.Start();

#if FIREFOX
            Thread browserCheckThread = new Thread(new ThreadStart(CheckBrowserSettings));
            browserCheckThread.IsBackground = true;
            browserCheckThread.Start();
#endif
        }

        private void IsAliveThead() {
            Logger.Log("Alive thread started", LogType.Information, ApplicationType.Client);
            try {
                while (true) {
                    if (String.IsNullOrEmpty(serverIP) == false) {
                        if (TcpCommunicator.Instance.IsServerConnected == false) {
                            Logger.Log("Registering server: " + serverIP, LogType.Information, ApplicationType.Client);
                            TcpCommunicator.Instance.RegisterServer(serverIP);
                        }

                        IsAliveMessage message = new IsAliveMessage() { IP = serverIP };

                        TcpCommunicator.Instance.EnqueMessage(message);
                        Logger.Log("Enqueue is alive message", LogType.Information, ApplicationType.Client);
                    }

                    Thread.Sleep(5000);
                }
            } finally {
                Logger.Log("Alive thread closing", LogType.Information, ApplicationType.Client);
            }
        }

        private void CommandReceiverThead() {
            Logger.Log("Starting command receiver thread", LogType.Information, ApplicationType.Client);

            ServerMessageProcessor messageProcessor = null;

            try {
                while (true) {
                    if (serverIPChanged) {
                        messageProcessor = new ServerMessageProcessor(serverIP);
                        messageProcessor.AccessModeTypeChanged += messageProcessor_AccessModeTypeChanged;

                        serverIPChanged = false;
                    }

                    List<MessageBase> messages = TcpCommunicator.Instance.DequeMessage();
                    foreach (MessageBase message in messages) {
                        messageProcessor.ProcessMessage(message);
                    }

                    Thread.Sleep(5000);
                }
            } finally {
                Logger.Log("Command receiver thread closing", LogType.Information, ApplicationType.Client);
            }
        }

        private void messageProcessor_AccessModeTypeChanged(object sender, EventArgs e) {
            modeType = AccessMode.ModeType;

            Logger.Log("Access mode type changed: " + modeType, LogType.Information, ApplicationType.Client);
        }

        private void ClientForm_Activated(object sender, EventArgs e) {
            notifyIcon1.Visible = true;
            this.Hide();
        }

        private void mnuServerIPAddress_Click(object sender, EventArgs e) {
            AdminAuthorizationForm authorizationForm = new AdminAuthorizationForm();
            if (authorizationForm.ShowDialog() == System.Windows.Forms.DialogResult.OK) {
                (new SetServerIPForm()).ShowDialog();

                string oldIP = serverIP;

                serverIP = ServerIP.GetServerIP();

                Logger.Log("Server IP changed: Old = " + oldIP + " New = " + serverIP, LogType.Information, ApplicationType.Client);

                serverIPChanged = true;

                if (String.IsNullOrEmpty(serverIP) == false) {
                    TcpCommunicator.Instance.RegisterServer(serverIP);
                }
            }
        }

        private void mnuWebsitesVisited_Click(object sender, EventArgs e) {
            UrlVisitedForm urlVisitedForm = new UrlVisitedForm();
            urlVisitedForm.LoadForm();
            urlVisitedForm.ShowDialog();
        }

        private void mnuBlockedWebsites_Click(object sender, EventArgs e) {
            UrlBlockedDetailsForm urlBlockedForm = new UrlBlockedDetailsForm();
            urlBlockedForm.LoadForm();
            urlBlockedForm.ShowDialog();
        }

        private void mnuAllowedWebsites_Click(object sender, EventArgs e) {
            UrlAllowedForm urlAllowedForm = new UrlAllowedForm();
            urlAllowedForm.LoadForm();
            urlAllowedForm.ShowDialog();
        }

        private void mnuNotAllowedWebsites_Click(object sender, EventArgs e) {
            UrlBlockedForm urlBlockedForm = new UrlBlockedForm();
            urlBlockedForm.LoadForm();
            urlBlockedForm.ShowDialog();
        }

        private void mnuWebsitesRequested_Click(object sender, EventArgs e) {
            UrlRequestedForm urlRequestedForm = new UrlRequestedForm();
            urlRequestedForm.LoadForm();
            urlRequestedForm.ShowDialog();
        }

        private void mnuChangeAdminPassword_Click(object sender, EventArgs e) {
            (new ChangeAdminPasswordForm()).ShowDialog();
        }

        private void mnuPurgeHistoryData_Click(object sender, EventArgs e) {
            AdminAuthorizationForm authorizationForm = new AdminAuthorizationForm();
            if (authorizationForm.ShowDialog() == System.Windows.Forms.DialogResult.OK) {
                Logger.Log("Purging visited and blocked URL history", LogType.Information, ApplicationType.Client);

                DatabaseAdapter.ClearVisited();

                DatabaseAdapter.ClearBlocked();
            }
        }

        private void mnuMode_Click(object sender, EventArgs e) {
            AdminAuthorizationForm authorizationForm = new AdminAuthorizationForm();
            if (authorizationForm.ShowDialog() == System.Windows.Forms.DialogResult.OK) {
                if (mnuMode.Text == "Enable Selective Allow Mode") {
                    AccessMode.ModeType = AccessModeType.SelectiveAllow;
                    modeType = AccessModeType.SelectiveAllow;
                    mnuMode.Text = "Enable Selective Block Mode";
                } else {
                    AccessMode.ModeType = AccessModeType.SelectiveBlock;
                    modeType = AccessModeType.SelectiveBlock;
                    mnuMode.Text = "Enable Selective Allow Mode";
                }
            }
        }

        private void mnuCurrentMode_Click(object sender, EventArgs e) {
            (new CurrentModeForm()).ShowDialog();

        }

        private void contextMenuStrip1_Opening(object sender, CancelEventArgs e) {
            mnuMode.Visible = String.IsNullOrEmpty(serverIP);
            mnuCurrentMode.Visible = String.IsNullOrEmpty(serverIP);
            sepMode.Visible = String.IsNullOrEmpty(serverIP);
            mnuAllowedWebsites.Visible = modeType == AccessModeType.SelectiveAllow;
            mnuWebsitesRequested.Visible = modeType == AccessModeType.SelectiveAllow && String.IsNullOrEmpty(serverIP) == false;
            mnuNotAllowedWebsites.Visible = modeType == AccessModeType.SelectiveBlock;
            mnuChangeAdminPassword.Visible = String.IsNullOrEmpty(serverIP);

            if (String.IsNullOrEmpty(serverIP)) {
                if (AccessMode.ModeType == AccessModeType.SelectiveBlock) {
                    mnuMode.Text = "Enable Selective Allow Mode";
                } else {
                    mnuMode.Text = "Enable Selective Block Mode";
                }
            }
        }

        private void CheckBrowserSettings() {
            while (true) {
                CheckMozillaProxySettings();

                Thread.Sleep(1000 * 60 * 1);
            }
        }

        private void CheckMozillaProxySettings() {
            try {
                string settingsPath = Environment.ExpandEnvironmentVariables("%AppData%") + @"\Mozilla\Firefox\Profiles";
                if (Directory.Exists(settingsPath) == false) return;

                string[] dirs = Directory.GetDirectories(settingsPath, "*.default");
                foreach (string dir in dirs) {
                    string preferenceFile = dir + @"\prefs.js";

                    if (File.Exists(preferenceFile)) {
                        Regex regex = new Regex(@"user_pref\(\""network\.proxy\.type\"", \d\);", RegexOptions.Compiled);
                        string preference = File.ReadAllText(preferenceFile);

                        bool match = regex.IsMatch(preference);

                        if (match) {
                            KillProcess("firefox");

                            string updated = regex.Replace(preference, "");

                            File.WriteAllText(preferenceFile, updated);

                            MessageBox.Show("Firefox is closed automatically to apply required settings", "Settings", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                    }
                }
            } catch (Exception exc) {
                MessageBox.Show(exc.Message + Environment.NewLine + exc.StackTrace, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void KillProcess(string processName) {
            Process[] processes = Process.GetProcessesByName(processName);
            if (processes.Length > 0) {
                processes[0].Kill();
            }
        }

        private void mnuRegisterProduct_Click(object sender, EventArgs e) {
            
        }

        private void mnuExit_Click(object sender, EventArgs e) {
            AdminAuthorizationForm authorizationForm = new AdminAuthorizationForm();
            if (authorizationForm.ShowDialog() == System.Windows.Forms.DialogResult.OK) {

                Process[] processes = Process.GetProcessesByName("Wt");
                if (processes.Length > 0) {
                    Logger.Log("Closing watcher", LogType.Information, ApplicationType.Client);
                    processes[0].Kill();
                }

                ResetRequiredRegEntries();

                Logger.Log("Closing application", LogType.Information, ApplicationType.Client);
                this.Close();
            }
        }

        private void ResetRequiredRegEntries() {
            RegistryKey root = Registry.CurrentUser;

            // internet options
            string key = @"Software\Policies\Microsoft\Internet Explorer";
            RegistryAdapter.CreateRegistryKey(root, key);

            key = @"Software\Policies\Microsoft\Internet Explorer\Restrictions";
            RegistryAdapter.CreateRegistryKey(root, key);

            RegistryAdapter.RemoveValue(root, key, "NoBrowserOptions");

            key = @"Software\Microsoft\Windows\CurrentVersion\Policies\Explorer";
            RegistryAdapter.CreateRegistryKey(root, key);

            RegistryAdapter.RemoveValue(root, key, "DisallowCpl");

            key = @"Software\Microsoft\Windows\CurrentVersion\Policies\Explorer\DisallowCpl";
            RegistryAdapter.RemoveValue(root, key, "InetOptions");
            RegistryAdapter.RemoveValue(root, key, "uninstall");
            RegistryAdapter.RemoveValue(root, key, "firewall");
        }

        private void mnuLogging_Click(object sender, EventArgs e) {
            if (mnuLogging.Text == "Enable Logging") {
                mnuLogging.Text = "Disable Logging";
                Logger.IsLogEnabled = true;
            } else {
                mnuLogging.Text = "Enable Logging";
                Logger.IsLogEnabled = false;
            }
        }

        private void mnuOpenLogFolder_Click(object sender, EventArgs e) {
            if (Directory.Exists(Logger.LogDir)) {
                Process.Start(Logger.LogDir);
            } else {
                MessageBox.Show("Log folder not found. Please enable logging and try to open log folder again.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void mnuOpenRegedit_Click(object sender, EventArgs e) {
            ProcessConfig config = new ProcessConfig();

            UpdateSettings("regedit", "1");           
        }

        private void mnuOpenMSConfig_Click(object sender, EventArgs e) {
            ProcessConfig config = new ProcessConfig();

            UpdateSettings("msconfig", "1");
        }

        private void mnuOpenInternetOptions_Click(object sender, EventArgs e) {
            ProcessConfig config = new ProcessConfig();

            UpdateSettings("inet", "1");
        }

        private void mnuAllowRundll32_Click(object sender, EventArgs e) {
            ProcessConfig config = new ProcessConfig();

            UpdateSettings("rundll", "1");
        }

        private void mnuProgramsAndFeatures_Click(object sender, EventArgs e) {
            ProcessConfig config = new ProcessConfig();

            UpdateSettings("programfeatures", "1");
        }

        private void UpdateSettings(string property, string val) {
            AdminAuthorizationForm authorizationForm = new AdminAuthorizationForm();
            if (authorizationForm.ShowDialog() == System.Windows.Forms.DialogResult.OK) {
                ProcessConfig config = new ProcessConfig();
                if (File.Exists(settingsPath)) {
                    config = (ProcessConfig)LoadSave.Load(typeof(ProcessConfig), settingsPath);
                }

                switch (property) {
                    case "regedit": config.Regedit = val; break;
                    case "msconfig": config.MSConfig = val; break;
                    case "inet": config.InternetOptions = val; break;
                    case "rundll": config.Rundll32 = val; break;
                    case "programfeatures":
                        RegistryKey root = Registry.CurrentUser;
                        string key = @"Software\Microsoft\Windows\CurrentVersion\Policies\Explorer\DisallowCpl";
                        RegistryAdapter.RemoveValue(root, key, "uninstall");

                        config.AddRemovePrograms = val;
                        break;
                }

                LoadSave.Save(typeof(ProcessConfig), config, settingsPath);
            }
        }
    }
}