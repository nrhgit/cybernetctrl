﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using MetroFramework.Forms;
using CybernetCtrl.Modal;
using CybernetCtrl.Common;

namespace CybernetCtrl.Client {
    public partial class CurrentModeForm : MetroForm {
        public CurrentModeForm() {
            InitializeComponent();
        }

        private void CurrentModeForm_Load(object sender, EventArgs e) {
            lblModeType.Text = AccessMode.ModeType == AccessModeType.SelectiveAllow ? "Selective Allow" : "Selective Block";
        }
    }
}
