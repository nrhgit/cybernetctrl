﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Net;
using System.Text;
using System.Windows.Forms;
using CybernetCtrl.Modal;
using MetroFramework.Forms;
using System.Net.NetworkInformation;

namespace CybernetCtrl.Client
{
    public partial class SetServerIPForm : MetroForm
    {
        public SetServerIPForm()
        {
            InitializeComponent();
        }

        private void SetServerIPForm_Load(object sender, EventArgs e)
        {
            txtIPAddress.Text = ServerIP.GetServerIP();
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            if (IsValidInput())
            {
                ServerIP.Save(txtIPAddress.Text);

                this.DialogResult = System.Windows.Forms.DialogResult.OK;
            }
        }

        private void btnRemove_Click(object sender, EventArgs e)
        {
            ServerIP.Save(string.Empty);

            this.DialogResult = System.Windows.Forms.DialogResult.OK;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
        }

        private bool IsValidInput()
        {
            if (String.IsNullOrEmpty(txtIPAddress.Text))
            {
                MessageBox.Show("Please enter IP Address.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }

            IPAddress ip;
            bool b = IPAddress.TryParse(txtIPAddress.Text, out ip);
            if (b == false)
            {
                MessageBox.Show("IP Address entered is not in valid format.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }

            return true;
        }

        private void btnPing_Click(object sender, EventArgs e)
        {
            Ping pinger = new Ping();

            PingReply reply = pinger.Send(txtIPAddress.Text);

            if (reply.Status == IPStatus.Success)
            {
                MessageBox.Show("Ping is successful!", "Ping", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                MessageBox.Show("Ping failed. Please check IP address and network connectivity.", "Ping", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }      
    }
}