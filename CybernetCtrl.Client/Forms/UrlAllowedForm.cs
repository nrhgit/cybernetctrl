﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using CybernetCtrl.Modal;
using MetroFramework.Forms;
using CybernetCtrl.Common.Forms;

namespace CybernetCtrl.Client
{
    public partial class UrlAllowedForm : MetroForm
    {
        public UrlAllowedForm()
        {
            InitializeComponent();
        }

        private void UrlAllowedForm_Load(object sender, EventArgs e)
        {
            dgrWebsitesAllowed.ClearSelection();
        }  

        public void LoadForm()
        {
            dgrWebsitesAllowed.Rows.Clear();

            List<UrlAllowed> urlsAllowed = UrlAllowed.GetAllowedUrls();
            foreach (UrlAllowed urlAllowed in urlsAllowed)
            {
                int index = dgrWebsitesAllowed.Rows.Add();
                dgrWebsitesAllowed[colUrl.Index, index].Value = urlAllowed.Url;

                dgrWebsitesAllowed.Rows[index].Tag = urlAllowed;
            }

            string serverIP = ServerIP.GetServerIP();
            btnBlock.Enabled = String.IsNullOrEmpty(serverIP);
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Do you really want to clear all websites allowed", "Confirmation", MessageBoxButtons.OKCancel, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.OK)
            {
                UrlAllowed.Clear();
            }
        }

        private void btnBlock_Click(object sender, EventArgs e)
        {
            AdminAuthorizationForm authorizationForm = new AdminAuthorizationForm();
            if (dgrWebsitesAllowed.SelectedRows.Count > 0 && authorizationForm.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                UrlAllowed urlAllowed = (UrlAllowed)dgrWebsitesAllowed.SelectedRows[0].Tag;

                urlAllowed.Block();

                dgrWebsitesAllowed.Rows.Remove(dgrWebsitesAllowed.SelectedRows[0]);
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            LoadForm();

            dgrWebsitesAllowed.ClearSelection();
        }      
    }
}
