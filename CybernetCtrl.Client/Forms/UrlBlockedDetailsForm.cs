﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using CybernetCtrl.Modal;
using MetroFramework.Forms;
using CybernetCtrl.Common;
using CybernetCtrl.Common.Forms;

namespace CybernetCtrl.Client
{
    public partial class UrlBlockedDetailsForm : MetroForm
    {
        private int index = 0, pageSize = CybernetCtrl.Common.Settings.PageSize;

        public UrlBlockedDetailsForm()
        {
            InitializeComponent();
        }

        public void LoadForm()
        {
            btnRequestAllow.Visible = (AccessMode.ModeType == AccessModeType.SelectiveAllow);

            string serverIP = ServerIP.GetServerIP();
            btnRequestAllow.Enabled = String.IsNullOrEmpty(serverIP) == false;
            btnAllow.Enabled = String.IsNullOrEmpty(serverIP);

            LoadUrls();
        }

        private void LoadUrls()
        {
            dgrWebsitesBlocked.Rows.Clear();

            List<UrlBlockedDetails> urlsBlocked = UrlBlockedDetails.GetUrlsBlocked(index, pageSize);
            foreach (UrlBlockedDetails urlBlocked in urlsBlocked)
            {
                int rowIndex = dgrWebsitesBlocked.Rows.Add();
                dgrWebsitesBlocked[colUrl.Index, rowIndex].Value = urlBlocked.Url;
                dgrWebsitesBlocked[colDateTime.Index, rowIndex].Value = urlBlocked.VisitedDateTime.ToString("dd-MMM-yyyy HH:mm:ss");

                dgrWebsitesBlocked.Rows[rowIndex].Tag = urlBlocked;
            }
        }

        private void btnRequestAllow_Click(object sender, EventArgs e)
        {
            if (dgrWebsitesBlocked.SelectedRows.Count > 0)
            {
                UrlBlockedDetails urlBlocked = (UrlBlockedDetails)dgrWebsitesBlocked.SelectedRows[0].Tag;

                UrlRequestForm urlRequestForm = new UrlRequestForm();
                urlRequestForm.LoadForm(urlBlocked.Url);
                urlRequestForm.ShowDialog();
            }
        }

        private void btnAllow_Click(object sender, EventArgs e)
        {
            if (dgrWebsitesBlocked.SelectedRows.Count > 0)
            {
                UrlBlockedDetails urlBlocked = (UrlBlockedDetails)dgrWebsitesBlocked.SelectedRows[0].Tag;

                string serverIP = ServerIP.GetServerIP();
                if (String.IsNullOrEmpty(serverIP))
                {
                    BlockAllowUrlConfigForm urlConfigForm = new BlockAllowUrlConfigForm();
                    urlConfigForm.LoadForm(urlBlocked.Url);
                    if (urlConfigForm.ShowDialog() == DialogResult.OK)
                    {
                        AdminAuthorizationForm authorizationForm = new AdminAuthorizationForm();
                        if (authorizationForm.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                        {
                            urlBlocked.Allow(urlConfigForm.FormattedUrl);

                            dgrWebsitesBlocked.Rows.Remove(dgrWebsitesBlocked.SelectedRows[0]);
                        }
                    }
                }
                else
                {
                    MessageBox.Show("Website can be allowed directly when client is running in single user mode. Use Allow Request instead.", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
        }

        private void btnFirst_Click(object sender, EventArgs e)
        {
            index = 0;

            LoadUrls();
        }

        private void btnPrev_Click(object sender, EventArgs e)
        {
            index--;

            LoadUrls();

            if (index < 0) index = 0;

            LoadUrls();
        }

        private void btnNext_Click(object sender, EventArgs e)
        {
            index++;

            int rowsCount = UrlBlockedDetails.GetTotalUrls();

            int pagesCount = (int)Math.Ceiling((decimal)rowsCount / pageSize);

            if (index > pagesCount - 1)
            {
                index = pagesCount - 1;
            }

            LoadUrls();
        }

        private void btnLast_Click(object sender, EventArgs e)
        {
            int rowsCount = UrlBlockedDetails.GetTotalUrls();

            index = (int)Math.Ceiling((decimal)rowsCount / pageSize) - 1;

            if (index < 0)
            {
                index = 0;
            }

            LoadUrls();
        }        
    }
}