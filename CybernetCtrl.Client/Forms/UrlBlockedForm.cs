﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using CybernetCtrl.Modal;
using MetroFramework.Forms;
using CybernetCtrl.Common.Forms;

namespace CybernetCtrl.Client {
    public partial class UrlBlockedForm : MetroForm {
        public UrlBlockedForm() {
            InitializeComponent();
        }

        private void UrlBlockedForm_Load(object sender, EventArgs e) {
            dgrWebsitesBlocked.ClearSelection();
        }

        public void LoadForm() {
            List<UrlBlocked> urlsBlocked = UrlBlocked.GetBlockedUrls();
            foreach (UrlBlocked urlBlocked in urlsBlocked) {
                int index = dgrWebsitesBlocked.Rows.Add();
                dgrWebsitesBlocked[colUrl.Index, index].Value = urlBlocked.Url;

                dgrWebsitesBlocked.Rows[index].Tag = urlBlocked;
            }

            string serverIP = ServerIP.GetServerIP();
            btnAllow.Enabled = String.IsNullOrEmpty(serverIP);
            btnAllowAll.Enabled = String.IsNullOrEmpty(serverIP);
        }

        private void btnAllowAll_Click(object sender, EventArgs e) {
            if (MessageBox.Show("Do you really want to clear all websites blocked", "Confirmation", MessageBoxButtons.OKCancel, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.OK) {
                UrlBlocked.Clear();
            }
        }

        private void btnAllow_Click(object sender, EventArgs e) {
            AdminAuthorizationForm authorizationForm = new AdminAuthorizationForm();
            if (dgrWebsitesBlocked.SelectedRows.Count > 0 && authorizationForm.ShowDialog() == System.Windows.Forms.DialogResult.OK) {
                UrlBlocked urlBlocked = (UrlBlocked)dgrWebsitesBlocked.SelectedRows[0].Tag;

                urlBlocked.Allow();

                dgrWebsitesBlocked.Rows.Remove(dgrWebsitesBlocked.SelectedRows[0]);
            }
        }

        private void btnClose_Click(object sender, EventArgs e) {
            this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
        }
    }
}