﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using CybernetCtrl.Modal;
using MetroFramework.Forms;

namespace CybernetCtrl.Client
{
    public partial class UrlRequestForm : MetroForm
    {
        public UrlRequestForm()
        {
            InitializeComponent();
        }

        public void LoadForm(string url)
        {
            txtWebsite.Text = url;            
        }

        private void btnSendRequest_Click(object sender, EventArgs e)
        {
            UrlRequest urlRequest = new UrlRequest();
            urlRequest.Url = txtWebsite.Text;
            urlRequest.Reason = txtDescription.Text;
            urlRequest.RequestDateTime = InternalTimer.Instance.ServerDateTime;
            urlRequest.Status = RequestStatusType.Requested;

            urlRequest.Save();

            this.DialogResult = System.Windows.Forms.DialogResult.OK;
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
        }
    }
}
