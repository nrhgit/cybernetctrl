﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using CybernetCtrl.Modal;
using MetroFramework.Forms;

namespace CybernetCtrl.Client
{
    public partial class UrlRequestedForm : MetroForm
    {
        public UrlRequestedForm()
        {
            InitializeComponent();
        }

        public void LoadForm()
        {
            List<UrlRequest> urlRequests = UrlRequest.GetUrlsRequested();
            foreach (UrlRequest urlRequest in urlRequests)
            {
                int index = dgrAllowWebsiteRequests.Rows.Add();
                dgrAllowWebsiteRequests.Rows[index].Cells[colUrl.Name].Value = urlRequest.Url;
                dgrAllowWebsiteRequests.Rows[index].Cells[colDateTime.Name].Value = urlRequest.RequestDateTime.ToString("dd-MMM-yyy HH:mm:ss");
                dgrAllowWebsiteRequests.Rows[index].Cells[colStatus.Name].Value = urlRequest.Status.ToString();
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
        }
    }
}
