﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using CybernetCtrl.Modal;
using CybernetCtrl.Common;
using MetroFramework.Forms;
using CybernetCtrl.Common.Forms;

namespace CybernetCtrl.Client
{
    public partial class UrlVisitedForm : MetroForm
    {
        private int index = 0, pageSize = CybernetCtrl.Common.Settings.PageSize;

        public UrlVisitedForm()
        {
            InitializeComponent();
        }

        public void LoadForm()
        {
            btnBlock.Visible = AccessMode.ModeType == AccessModeType.SelectiveBlock;
            string serverIP = ServerIP.GetServerIP();
            btnBlock.Enabled = String.IsNullOrEmpty(serverIP);

            LoadUrls();
        }

        private void LoadUrls()
        {
            dgrWebsitesVisited.Rows.Clear();

            List<UrlVisited> urlsVisited = UrlVisited.GetUrlsVisited(index, pageSize);
            foreach (UrlVisited urlVisited in urlsVisited)
            {
                AddRow(urlVisited.Url, urlVisited.VisitedDateTime);
            }
        }

        delegate void AddRowDelegate(string text, DateTime visitedDateTime);
        private void AddRow(string url, DateTime visitedDateTime)
        {
            if (InvokeRequired)
            {
                this.Invoke(new AddRowDelegate(AddRow), url, visitedDateTime);
            }
            else
            {
                int index = dgrWebsitesVisited.Rows.Add();
                dgrWebsitesVisited[0, index].Value = url;
                dgrWebsitesVisited[1, index].Value = visitedDateTime.ToString("dd-MMM-yyyy HH:mm:ss");

                dgrWebsitesVisited.FirstDisplayedScrollingRowIndex = dgrWebsitesVisited.RowCount - 1;
            }
        }

        private void btnBlock_Click(object sender, EventArgs e)
        {
            if (dgrWebsitesVisited.SelectedRows.Count > 0)
            {
                string url = (string)dgrWebsitesVisited[0, dgrWebsitesVisited.SelectedRows[0].Index].Value;

                BlockAllowUrlConfigForm urlConfigForm = new BlockAllowUrlConfigForm();
                urlConfigForm.LoadForm(url);
                if (urlConfigForm.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    UrlBlocked urlBlocked = new UrlBlocked() { Url = urlConfigForm.FormattedUrl };
                    urlBlocked.Save();
                }
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.OK;
        }

        private void btnFirst_Click(object sender, EventArgs e)
        {
            index = 0;

            LoadUrls();
        }

        private void btnPrev_Click(object sender, EventArgs e)
        {
            index--;

            LoadUrls();

            if (index < 0) index = 0;

            LoadUrls();
        }

        private void btnNext_Click(object sender, EventArgs e)
        {
            index++;

            int rowsCount = UrlVisited.GetTotalUrls();

            int pagesCount = (int)Math.Ceiling((decimal)rowsCount / pageSize);

            if (index > pagesCount - 1)
            {
                index = pagesCount - 1;
            }

            LoadUrls();
        }

        private void btnLast_Click(object sender, EventArgs e)
        {
            int rowsCount = UrlVisited.GetTotalUrls();

            index = (int)Math.Ceiling((decimal)rowsCount / pageSize) - 1;

            if (index < 0)
            {
                index = 0;
            }

            LoadUrls();
        }
    }
}
