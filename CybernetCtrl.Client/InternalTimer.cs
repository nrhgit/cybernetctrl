﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Timers;

namespace CybernetCtrl.Client {
    internal class InternalTimer {
        private int secElapsed = 0;
        private Timer timer { get; set; }
        private DateTime serverDateTime;
        private static InternalTimer instance;

        public InternalTimer() {
            serverDateTime = DateTime.Now;

            timer = new Timer(1000);
            timer.Elapsed += Timer_Elapsed;
            timer.Enabled = true;
            timer.Start();
        }

        public static InternalTimer Instance {
            get {
                if (instance == null) instance = new InternalTimer();

                return instance;
            }
        }

        public DateTime ServerDateTime {
            get {
                return serverDateTime.AddSeconds(secElapsed);
            }
            set {
                serverDateTime = value;
                secElapsed = 0;
            }
        }

        private void Timer_Elapsed(object sender, ElapsedEventArgs e) {
            secElapsed++;
        }
    }
}