﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Windows.Forms;
using CybernetCtrl.Common;

namespace CybernetCtrl.Client {
    static class Program {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main() {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            //Application.ThreadException += new System.Threading.ThreadExceptionEventHandler(Application_ThreadException);
            //AppDomain.CurrentDomain.UnhandledException += new UnhandledExceptionEventHandler(CurrentDomain_UnhandledException);

            //if (IsApplicationAlreadyOpen()) {
            //    return;
            //}

            Application.Run(new ClientMainForm());
        }

        //private static bool IsApplicationAlreadyOpen() {
        //    Process currentProcess = Process.GetCurrentProcess();
        //    Process[] processes = Process.GetProcessesByName(currentProcess.ProcessName);
        //    foreach (Process process in processes) {
        //        if (process.Id != currentProcess.Id && process.MainModule.FileName == currentProcess.MainModule.FileName) {
        //            return true;
        //        }
        //    }

        //    return false;
        //}

        //static void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e) {
        //    LogException((Exception)e.ExceptionObject);
        //}

        //static void Application_ThreadException(object sender, System.Threading.ThreadExceptionEventArgs e) {
        //    LogException(e.Exception);
        //}

        //private static void LogException(Exception exc) {
        //    if (exc != null) {
        //        if (Logger.IsLogEnabled) {
        //            Logger.Log(exc.Message + Environment.NewLine + exc.StackTrace, LogType.Error, ApplicationType.Client);
        //        } else {
        //            MessageBox.Show(exc.Message + Environment.NewLine + exc.StackTrace, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
        //        }
        //    }
        //}
    }
}