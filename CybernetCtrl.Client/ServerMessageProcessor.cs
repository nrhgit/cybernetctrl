﻿using System;
using System.Collections.Generic;
using System.Text;
using CybernetCtrl.Message;
using CybernetCtrl.Modal;
using CybernetCtrl.TcpComm;
using CybernetCtrl.Common;
using System.IO;
using System.Threading;
using System.Windows.Forms;

namespace CybernetCtrl.Client {
    internal class ServerMessageProcessor {

        public event EventHandler AccessModeTypeChanged;

        private string serverIP;

        public ServerMessageProcessor(string serverIP) {
            this.serverIP = serverIP;
        }

        public void ProcessMessage(MessageBase message) {

            if (message is SyncVersionCommand) {
                Process_SyncVersionCommand(message);
            }
                
                /********** Apply patch file ***********/
            else if (message is PatchFileMessage) {
                Process_PatchFileMessage(message);
            } 

            // TODO: remove this comment.. ************************** IMP *********************************
            //if (Settings.IsCompatible == false) {
            //    return;
            //}

            if (message is ServerTimeStampMessage) {
                Process_ServerTimeStampMessage(message);
            } else if (message is AdminPasswordMessage) {
                Process_AdminPasswordMessage(message);
            } else if (message is UserAccessModeMessage) {
                Process_UserAccessModeMessage(message);
            } else if (message is SendUrlsVistedCommand) {
                Process_SendUrlsVistedCommand(message);
            } else if (message is SendUrlsBlockedDetailCommand) {
                Process_SendUrlsBlockedCommand(message);
            } else if (message is SendUrlRequestCommand) {
                Process_SendUrlRequestCommand(message);
            }

              /********** Allowed Urls ******/
              else if (message is UrlAllowedMessage) {
                Process_UrlAllowedMessage(message);
            } else if (message is InitValidateUrlAllowedMessage) {
                Process_InitValidateUrlAllowedMessage(message);
            } else if (message is InvalidUrlAllowedCommand) {
                Process_InvalidUrlAllowedCommand(message);
            }

              /********** Blocked Urls ******/
              else if (message is UrlBlockedMessage) {
                Process_UrlBlockedMessage(message);
            } else if (message is InitValidateUrlBlockedMessage) {
                Process_InitValidateUrlBlockedMessage(message);
            } else if (message is InvalidBlockedUrlCommand) {
                Process_InvalidUrlBlockedCommand(message);
            }

              /********** Groups ***********/
              else if (message is GroupMessage) {
                Process_GroupMessage(message);
            } else if (message is InitValidateGroupMessage) {
                Process_InitValidateGroupMessage(message);
            } else if (message is InvalidGroupCommand) {
                Process_InvalidGroupCommand(message);
            }
           
            /********** Start video capture ***********/
              else if (message is StartCaptureCommand) {
                Process_StartCaptureCommand(message);
            }

            else if (message is SyncPatchCommand) {
                Process_SyncPatchCommand(message);
            }

            /********** User file limit ***********/
            else if (message is UserFileLimitMessage) {
                Process_UserFileLimitMessage(message);
            }

            /********** Send serial command *******/
            else if(message is SendNodeSerialCommand){
                Process_SendSerialCommand(message);
            }

            /********** Apply license key received *******/
            else if (message is ClientLicenseMessage) {
                Process_ClientLicenseMessage(message);
            }
        }

        private void Process_SyncVersionCommand(MessageBase message) {
            Logger.Log("Processing version sync message.", LogType.Information, ApplicationType.Client);

            SyncVersionCommand syncVersionMessage = (SyncVersionCommand)message;

            Settings.IsCompatible = (syncVersionMessage.ExpectedClientVersion == Settings.CompatibleClientVersion);

            ClientVersionMessage clientVersionMessage = new ClientVersionMessage() { IP = serverIP, UserID = syncVersionMessage.UserID, Version = Settings.CompatibleClientVersion };

            TcpCommunicator.Instance.EnqueMessage(clientVersionMessage);
        }

        private void Process_SyncPatchCommand(MessageBase message) {
            Logger.Log("Processing patch sync message.", LogType.Information, ApplicationType.Client);

            SyncPatchCommand syncVersionMessage = (SyncPatchCommand)message;

            string[] names = null;

            string patchdir = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) + @"\CybernetCtrl\lib\patch";
            if (Directory.Exists(patchdir)) {
                names = Directory.GetFiles(patchdir, "*.ctrlpatch");
            }

            string name =(names != null && names.Length > 0 ? Path.GetFileName( names[0]) : "unknown").ToLower().Replace(".ctrlpatch", "");
            PatchAppliedMessage patchAppliedMessage = new PatchAppliedMessage() { IP = serverIP, UserID = syncVersionMessage.UserID, Name = name };

            TcpCommunicator.Instance.EnqueMessage(patchAppliedMessage);
        }

        private void Process_ServerTimeStampMessage(MessageBase message) {
            Logger.Log("Processing server time stamp message.", LogType.Information, ApplicationType.Client);

            ServerTimeStampMessage timeStampMessage = (ServerTimeStampMessage)message;

            Logger.Log("Server date time received is: " + timeStampMessage.ServerDateTime.ToString("yyyy-MM-dd HH:mm:ss"), LogType.Information, ApplicationType.Client);

            InternalTimer.Instance.ServerDateTime = timeStampMessage.ServerDateTime;
        }

        private void Process_AdminPasswordMessage(MessageBase message) {
            Logger.Log("Processing admin password message.", LogType.Information, ApplicationType.Client);

            AdminPasswordMessage adminPasswordMessage = (AdminPasswordMessage)message;

            AdminPassword.Clear();

            AdminPassword.SetNewPassword(adminPasswordMessage.Password);
        }

        private void Process_UserAccessModeMessage(MessageBase message) {
            Logger.Log("Processing user access mode message", LogType.Information, ApplicationType.Client);

            UserAccessModeMessage userAccessModeMessage = (UserAccessModeMessage)message;

            AccessMode.ModeType = userAccessModeMessage.ModType;

            Logger.Log("Access mode is set to = " + AccessMode.ModeType.ToString(), LogType.Information, ApplicationType.Client);

            if (AccessModeTypeChanged != null) {
                AccessModeTypeChanged(this, null);
            }
        }

        private void Process_UserFileLimitMessage(MessageBase message) {
            Logger.Log("Processing user file limit message", LogType.Information, ApplicationType.Client);

            UserFileLimitMessage userFileLimitMessage = (UserFileLimitMessage)message;

            UserFileLimit.Limit = userFileLimitMessage.FileLimit;

            Logger.Log("File limit is set to = " + userFileLimitMessage.FileLimit.ToString(), LogType.Information, ApplicationType.Client);
        }

        private void Process_SendUrlsVistedCommand(MessageBase message) {
            Logger.Log("Processing send visited urls command", LogType.Information, ApplicationType.Client);

            SendUrlsVistedCommand sendVistedWebsitesCommand = (SendUrlsVistedCommand)message;

            List<UrlVisited> urlsVisited = UrlVisited.GetUrlsVisited(0, Int32.MaxValue);
            foreach (UrlVisited urlVisited in urlsVisited) {
                if (urlVisited.MilliSeconds <= sendVistedWebsitesCommand.LastMilliSeconds) {
                    continue;
                }

                UrlVisitedMessage urlVisitedMessage = new UrlVisitedMessage();
                urlVisitedMessage.IP = serverIP;
                urlVisitedMessage.UserID = sendVistedWebsitesCommand.UserID;
                urlVisitedMessage.Title = urlVisited.Title;
                urlVisitedMessage.Url = urlVisited.Url;
                urlVisitedMessage.FullUrl = urlVisited.FullUrl;
                urlVisitedMessage.VisitedDateTime = urlVisited.VisitedDateTime.ToString("yyyy-MM-dd HH:mm:ss");
                urlVisitedMessage.MilliSeconds = urlVisited.MilliSeconds;

                TcpCommunicator.Instance.EnqueMessage(urlVisitedMessage);
                Logger.Log("Enqueuing url visited message. URL: " + urlVisitedMessage.Url, LogType.Information, ApplicationType.Client);
            }
        }

        private void Process_SendUrlsBlockedCommand(MessageBase message) {
            Logger.Log("Processing send blocked urls command", LogType.Information, ApplicationType.Client);

            SendUrlsBlockedDetailCommand sendVistedWebsitesCommand = (SendUrlsBlockedDetailCommand)message;

            List<UrlBlockedDetails> urlsBlocked = UrlBlockedDetails.GetUrlsBlocked(0, Int32.MaxValue);
            foreach (UrlBlockedDetails urlBlocked in urlsBlocked) {
                if (urlBlocked.MilliSeconds <= sendVistedWebsitesCommand.LastMilliSeconds) {
                    continue;
                }

                UrlBlockedDetailMessage urlBlockedMessage = new UrlBlockedDetailMessage();
                urlBlockedMessage.IP = serverIP;
                urlBlockedMessage.UserID = sendVistedWebsitesCommand.UserID;
                urlBlockedMessage.Title = urlBlocked.Title;
                urlBlockedMessage.Url = urlBlocked.Url;
                urlBlockedMessage.FullUrl = urlBlocked.FullUrl;
                urlBlockedMessage.VisitedDateTime = urlBlocked.VisitedDateTime.ToString("yyyy-MM-dd HH:mm:ss");
                urlBlockedMessage.MilliSeconds = urlBlocked.MilliSeconds;

                TcpCommunicator.Instance.EnqueMessage(urlBlockedMessage);
                Logger.Log("Enqueuing url visited message. URL: " + urlBlockedMessage.Url, LogType.Information, ApplicationType.Client);
            }
        }

        private void Process_SendUrlRequestCommand(MessageBase message) {
            Logger.Log("Processing send requested urls command", LogType.Information, ApplicationType.Client);

            SendUrlRequestCommand sendUrlRequestsCommand = (SendUrlRequestCommand)message;

            List<UrlRequest> urlsRequested = UrlRequest.GetUrlsRequested();
            foreach (UrlRequest urlRequest in urlsRequested) {
                if (urlRequest.RequestDateTime <= sendUrlRequestsCommand.LastDateTime) {
                    continue;
                }

                UrlRequestMessage urlRequestMessage = new UrlRequestMessage();
                urlRequestMessage.IP = serverIP;
                urlRequestMessage.UserID = sendUrlRequestsCommand.UserID;
                urlRequestMessage.Url = urlRequest.Url;
                urlRequestMessage.Reason = urlRequest.Reason;
                urlRequestMessage.RequestDateTime = urlRequest.RequestDateTime.ToString("yyyy-MM-dd HH:mm:ss");

                TcpCommunicator.Instance.EnqueMessage(urlRequestMessage);
                Logger.Log("Enqueuing URL requested. URL: " + urlRequestMessage.Url, LogType.Information, ApplicationType.Client);
            }
        }

        // Group messages sync
        private void Process_GroupMessage(MessageBase message) {
            Logger.Log("Processing group message to synchronize groups.", LogType.Information, ApplicationType.Client);

            GroupMessage groupMessage = (GroupMessage)message;

            UrlGroup group = new UrlGroup() { ID = groupMessage.ID, Name = groupMessage.Name, AllowedDuration = groupMessage.AllowedDuration, SessionDuration = groupMessage.SessionDuration };

            group.Save();

            Logger.Log("Received group: " + groupMessage.Name, LogType.Information, ApplicationType.Client);
        }

        private void Process_InitValidateGroupMessage(MessageBase message) {
            Logger.Log("Processing initialize validate group message", LogType.Information, ApplicationType.Client);

            InitValidateGroupMessage initValidateGroupMessage = (InitValidateGroupMessage)message;

            // get all groups and send it to server
            List<UrlGroup> groups = UrlGroup.GetGroups();
            foreach (UrlGroup group in groups) {
                ValidateGroupMessage validateGroupMessage = new ValidateGroupMessage() { IP = serverIP, GroupID = group.ID };

                TcpCommunicator.Instance.EnqueMessage(validateGroupMessage);

                Logger.Log("Enqueuing group ID: " + validateGroupMessage.GroupID + " for validation.", LogType.Information, ApplicationType.Client);
            }
        }

        private void Process_InvalidGroupCommand(MessageBase message) {
            Logger.Log("Processing invalid group command", LogType.Information, ApplicationType.Client);

            InvalidGroupCommand invalidGroupCommand = (InvalidGroupCommand)message;

            (new UrlGroup() { ID = invalidGroupCommand.GroupID }).Remove();

            Logger.Log("Received invalid group with ID: " + invalidGroupCommand.GroupID + ". Same is removed from database.", LogType.Information, ApplicationType.Client);
        }

        // URL Allowed sync
        private void Process_UrlAllowedMessage(MessageBase message) {
            Logger.Log("Processing URL allowed message", LogType.Information, ApplicationType.Client);

            UrlAllowedMessage urlAllowedMessage = (UrlAllowedMessage)message;

            UrlAllowed urlAllowed = new UrlAllowed() { Url = urlAllowedMessage.Url, GroupID = urlAllowedMessage.GroupID };

            if (urlAllowed.IsAlreadyExists() == false) {
                urlAllowed.Save();
            }

            Logger.Log("Received allowed URL: " + urlAllowedMessage.Url, LogType.Information, ApplicationType.Client);
        }

        private void Process_InitValidateUrlAllowedMessage(MessageBase message) {
            Logger.Log("Processing initialize validation of allowed URL message", LogType.Information, ApplicationType.Client);

            InitValidateUrlAllowedMessage initValidateUrlAllowedMessage = (InitValidateUrlAllowedMessage)message;

            // get all groups and send it to server
            List<UrlAllowed> urls = UrlAllowed.GetAllowedUrls();
            foreach (UrlAllowed url in urls) {
                ValidateUrlAllowedMessage validateUrlAllowedMessage = new ValidateUrlAllowedMessage() { IP = serverIP, Url = url.Url };

                TcpCommunicator.Instance.EnqueMessage(validateUrlAllowedMessage);

                Logger.Log("Enqueuing URL allowed: " + validateUrlAllowedMessage.Url + " for validation.", LogType.Information, ApplicationType.Client);
            }
        }

        private void Process_InvalidUrlAllowedCommand(MessageBase message) {
            Logger.Log("Processing invalid url allowed command", LogType.Information, ApplicationType.Client);

            InvalidUrlAllowedCommand invalidUrlAllowedCommand = (InvalidUrlAllowedCommand)message;

            (new UrlAllowed() { Url = invalidUrlAllowedCommand.Url }).Block();

            Logger.Log("Received invalid allowed URL: " + invalidUrlAllowedCommand.Url + ". Same is blocked.", LogType.Information, ApplicationType.Client);
        }

        // URL Blocked sync
        private void Process_UrlBlockedMessage(MessageBase message) {
            Logger.Log("Processing url blocked message", LogType.Information, ApplicationType.Client);

            UrlBlockedMessage urlBlockedMessage = (UrlBlockedMessage)message;

            UrlBlocked urlBlocked = new UrlBlocked() { Url = urlBlockedMessage.Url };

            if (urlBlocked.IsAlreadyExists() == false) {
                urlBlocked.Save();
            }

            Logger.Log("Received blocked URL: " + urlBlockedMessage.Url, LogType.Information, ApplicationType.Client);
        }

        private void Process_InitValidateUrlBlockedMessage(MessageBase message) {
            Logger.Log("Processing initialize validate url blocked message", LogType.Information, ApplicationType.Client);

            InitValidateUrlBlockedMessage initValidateUrlAllowedMessage = (InitValidateUrlBlockedMessage)message;

            // get all groups and send it to server
            List<UrlBlocked> urls = UrlBlocked.GetBlockedUrls();
            foreach (UrlBlocked url in urls) {
                ValidateUrlBlockedMessage validateUrlBlockedMessage = new ValidateUrlBlockedMessage() { IP = serverIP, Url = url.Url };

                TcpCommunicator.Instance.EnqueMessage(validateUrlBlockedMessage);
                Logger.Log("Enqueuing URL blocked: " + validateUrlBlockedMessage.Url + " for validation.", LogType.Information, ApplicationType.Client);
            }
        }

        private void Process_InvalidUrlBlockedCommand(MessageBase message) {
            Logger.Log("Processing invalid url blocked command", LogType.Information, ApplicationType.Client);

            InvalidBlockedUrlCommand invalidUrlCommand = (InvalidBlockedUrlCommand)message;

            (new UrlBlocked() { Url = invalidUrlCommand.Url }).Allow();

            Logger.Log("Received invalid blocked URL: " + invalidUrlCommand.Url + ". Same is allowed.", LogType.Information, ApplicationType.Client);
        }

        // Start video capture
        private void Process_StartCaptureCommand(MessageBase message) {
            Logger.Log("Processing start capture command", LogType.Information, ApplicationType.Client);

            StartCaptureCommand startCaptureCommand = (StartCaptureCommand)message;

            Logger.Log("Starting capture adapter", LogType.Information, ApplicationType.Client);

            CaptureAdapter.Start(serverIP, startCaptureCommand.Port);
        }

        // Process path file
        private void Process_PatchFileMessage(MessageBase message) {
            PatchFileMessage patchFileMessage = (PatchFileMessage)message;

            string patchdir = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) + @"\CybernetCtrl\lib\patch";
            if (Directory.Exists(patchdir) == false) {
                Directory.CreateDirectory(patchdir);
            }

            string[] patchFiles = Directory.GetFiles(patchdir, "*.ctrlpatch");
            foreach (string patchFile in patchFiles) {
                File.Delete(patchFile);
            }

            string patchfile = patchdir + @"\" + patchFileMessage.Name;
            StringBinaryConverter.StringToBinaryFile(patchfile, patchFileMessage.Content);

            string updatedir = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) + @"\CybernetCtrl\lib\update";
            if (Directory.Exists(updatedir) == false) {
                Directory.CreateDirectory(updatedir);
            }

            ZipUnzip.Unzip(patchfile, updatedir);

            string[] dirs = Directory.GetDirectories(updatedir);
            if (dirs.Length > 0) {
                string[] files = Directory.GetFiles(dirs[0]);

                foreach(string file in files){
                    File.Copy(file, updatedir + @"\" + Path.GetFileName(file), true);
                }

                Directory.Delete(dirs[0], true);
            }

            Thread.Sleep(3000);

            Application.Exit();
        }

        private void Process_SendSerialCommand(MessageBase message) {
            SendNodeSerialCommand sendNodeSerialCommand = (SendNodeSerialCommand)message;

            NodeSerialMessage nodeSerialMessage = new NodeSerialMessage() { IP = serverIP, Serial = Guid.NewGuid().ToString() };

            TcpCommunicator.Instance.EnqueMessage(nodeSerialMessage);
        }

        private void Process_ClientLicenseMessage(MessageBase message) {
            ClientLicenseMessage clientLicenseMessage = (ClientLicenseMessage)message;

#if LICENSING
            LicenseManager.Register(clientLicenseMessage.License);
#elif FIXED_LICENSING
            FixedLicenseManager.Register(clientLicenseMessage.License);
#endif
        }
    }
}