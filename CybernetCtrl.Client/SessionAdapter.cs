﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using CybernetCtrl.Modal;
using CybernetCtrl.Common;

namespace CybernetCtrl.Client {
    internal class SessionAdapter {
        public static bool AllowVisit(string url, UrlGroup group) {
            Logger.Log("Checking whether to allow an url which belongs to a group. URL: " + url + " Group: " + group.Name, LogType.Information, ApplicationType.Client);

            int daySessionsCount = Session.GetSessionCount(InternalTimer.Instance.ServerDateTime, group);
            int groupSessionsCount = group.SessionDuration == 0 ? 1 : (int)Math.Ceiling(group.AllowedDuration / (decimal)group.SessionDuration);
            Session lastSession = Session.GetLastSession(InternalTimer.Instance.ServerDateTime, group);

            if (lastSession == null) {
                return StartSession(group, groupSessionsCount, daySessionsCount);
            } else {
                Logger.Log(String.Format("Day sessions count: {0}. Group sessions count: {1}. Last session: {2} {3}", daySessionsCount, groupSessionsCount, lastSession.SessionDate.ToString("yyyy-MMM-dd"), lastSession.StartTime.ToString("HH:mm:ss")), LogType.Information, ApplicationType.Client);

                if (daySessionsCount < groupSessionsCount) {
                    if (lastSession.IsExpired(InternalTimer.Instance.ServerDateTime)) {
                        Logger.Log("Last session is expired. So application will ask user whether to start a new session", LogType.Information, ApplicationType.Client);
                        return StartSession(group, groupSessionsCount, daySessionsCount);
                    } else {
                        Logger.Log("Active session. URL is allowed", LogType.Information, ApplicationType.Client);
                        return true;
                    }
                } else if (daySessionsCount == groupSessionsCount) {
                    if (lastSession.IsExpired(InternalTimer.Instance.ServerDateTime)) {
                        Logger.Log("Last session is expired and number of sessions allowed per day is also used. Respective message is shown to the user.", LogType.Information, ApplicationType.Client);
                        ClientGlobals.ShowNotification(String.Format("Total duration for websites in group : {0} is alredy used. Hence website: {1} blocked.", group.Name, url));

                        return false;
                    } else {
                        Logger.Log("Active session. URL is allowed", LogType.Information, ApplicationType.Client);
                        return true;
                    }
                }
            }

            return false;
        }

        private static bool StartSession(UrlGroup group, int groupSessionsCount, int daySessionsCount) {
            Logger.Log("Asking user whether to start the session.", LogType.Information, ApplicationType.Client);
            string msg = String.Format("Website you are trying to visit belongs to group: {0}. Do you want to start a new session ?" + Environment.NewLine + Environment.NewLine + "Session duration: {1} mins." + Environment.NewLine + "Total number of sessions per day: {2}" + Environment.NewLine + "Number of sessions left: {3}" + Environment.NewLine + Environment.NewLine + "Click OK to start a new session.", group.Name, group.SessionDuration, groupSessionsCount, (groupSessionsCount - daySessionsCount));

            if ((new SessionMessageForm(msg)).ShowDialog() == DialogResult.OK) {
                Logger.Log("Starting session", LogType.Information, ApplicationType.Client);

                Session newSession = new Session() { StartTime = InternalTimer.Instance.ServerDateTime, SessionDate = InternalTimer.Instance.ServerDateTime, UrlGroup = group };
                newSession.Save();

                return true;
            }

            Logger.Log("Use choose not to start a session", LogType.Information, ApplicationType.Client);
            return false;
        }
    }
}