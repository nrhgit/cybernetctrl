﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using CybernetCtrl.Modal;
using MetroFramework.Forms;
using CybernetCtrl.Common;

namespace CybernetCtrl.Common.Forms
{
    public partial class AdminAuthorizationForm : MetroForm
    {
        public AdminAuthorizationForm()
        {
            InitializeComponent();
        }
        private void btnOK_Click(object sender, EventArgs e)
        {
            if (AdminPassword.Password == txtPassword.Text)
            {
                Logger.Log("Admin authorization successful", LogType.Information, ApplicationType.Client);
                this.DialogResult = System.Windows.Forms.DialogResult.OK;
            }
            else
            {
                Logger.Log("Admin authorization failed.", LogType.Information, ApplicationType.Client);
                MessageBox.Show("Admin password entered is invalid", "Invalid password", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.No;
        }
    }
}
