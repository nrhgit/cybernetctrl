﻿namespace CybernetCtrl.Common.Forms
{
    partial class BlockAllowUrlConfigForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnCancel = new MetroFramework.Controls.MetroButton();
            this.btnOK = new MetroFramework.Controls.MetroButton();
            this.txtUrl = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel1 = new MetroFramework.Controls.MetroLabel();
            this.rbSpecificUrl = new MetroFramework.Controls.MetroRadioButton();
            this.rbGenericUrl = new MetroFramework.Controls.MetroRadioButton();
            this.chkAllSubDomains = new MetroFramework.Controls.MetroCheckBox();
            this.SuspendLayout();
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.Location = new System.Drawing.Point(330, 205);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 28);
            this.btnCancel.TabIndex = 38;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseSelectable = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnOK
            // 
            this.btnOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOK.Location = new System.Drawing.Point(249, 205);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(75, 28);
            this.btnOK.TabIndex = 37;
            this.btnOK.Text = "OK";
            this.btnOK.UseSelectable = true;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // txtUrl
            // 
            this.txtUrl.Lines = new string[0];
            this.txtUrl.Location = new System.Drawing.Point(23, 82);
            this.txtUrl.MaxLength = 32767;
            this.txtUrl.Multiline = true;
            this.txtUrl.Name = "txtUrl";
            this.txtUrl.PasswordChar = '\0';
            this.txtUrl.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtUrl.SelectedText = "";
            this.txtUrl.Size = new System.Drawing.Size(382, 54);
            this.txtUrl.TabIndex = 39;
            this.txtUrl.UseSelectable = true;
            // 
            // metroLabel1
            // 
            this.metroLabel1.AutoSize = true;
            this.metroLabel1.Location = new System.Drawing.Point(23, 60);
            this.metroLabel1.Name = "metroLabel1";
            this.metroLabel1.Size = new System.Drawing.Size(158, 19);
            this.metroLabel1.TabIndex = 40;
            this.metroLabel1.Text = "Website URL to configure";
            // 
            // rbSpecificUrl
            // 
            this.rbSpecificUrl.AutoSize = true;
            this.rbSpecificUrl.Checked = true;
            this.rbSpecificUrl.Location = new System.Drawing.Point(23, 142);
            this.rbSpecificUrl.Name = "rbSpecificUrl";
            this.rbSpecificUrl.Size = new System.Drawing.Size(88, 15);
            this.rbSpecificUrl.TabIndex = 41;
            this.rbSpecificUrl.TabStop = true;
            this.rbSpecificUrl.Text = "Specific URL";
            this.rbSpecificUrl.UseSelectable = true;
            this.rbSpecificUrl.CheckedChanged += new System.EventHandler(this.rbSpecificUrl_CheckedChanged);
            // 
            // rbGenericUrl
            // 
            this.rbGenericUrl.AutoSize = true;
            this.rbGenericUrl.Location = new System.Drawing.Point(23, 163);
            this.rbGenericUrl.Name = "rbGenericUrl";
            this.rbGenericUrl.Size = new System.Drawing.Size(172, 15);
            this.rbGenericUrl.TabIndex = 42;
            this.rbGenericUrl.Text = "Generic URL using wildcards";
            this.rbGenericUrl.UseSelectable = true;
            this.rbGenericUrl.CheckedChanged += new System.EventHandler(this.rbGenericUrl_CheckedChanged);
            // 
            // chkAllSubDomains
            // 
            this.chkAllSubDomains.AutoSize = true;
            this.chkAllSubDomains.Location = new System.Drawing.Point(68, 184);
            this.chkAllSubDomains.Name = "chkAllSubDomains";
            this.chkAllSubDomains.Size = new System.Drawing.Size(156, 15);
            this.chkAllSubDomains.TabIndex = 43;
            this.chkAllSubDomains.Text = "Apply to all sub-domains";
            this.chkAllSubDomains.UseSelectable = true;
            this.chkAllSubDomains.CheckedChanged += new System.EventHandler(this.chkAllSubDomains_CheckedChanged);
            // 
            // BlockAllowUrlConfigForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(428, 256);
            this.Controls.Add(this.chkAllSubDomains);
            this.Controls.Add(this.rbGenericUrl);
            this.Controls.Add(this.rbSpecificUrl);
            this.Controls.Add(this.metroLabel1);
            this.Controls.Add(this.txtUrl);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnOK);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "BlockAllowUrlConfigForm";
            this.Text = "Configure URL";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private MetroFramework.Controls.MetroButton btnCancel;
        private MetroFramework.Controls.MetroButton btnOK;
        private MetroFramework.Controls.MetroTextBox txtUrl;
        private MetroFramework.Controls.MetroLabel metroLabel1;
        private MetroFramework.Controls.MetroRadioButton rbSpecificUrl;
        private MetroFramework.Controls.MetroRadioButton rbGenericUrl;
        private MetroFramework.Controls.MetroCheckBox chkAllSubDomains;
    }
}