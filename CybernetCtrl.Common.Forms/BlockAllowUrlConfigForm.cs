﻿using CybernetCtrl.Common;
using MetroFramework.Forms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace CybernetCtrl.Common.Forms
{
    public partial class BlockAllowUrlConfigForm : MetroForm
    {
        private string initUrl = string.Empty;

        public BlockAllowUrlConfigForm()
        {
            InitializeComponent();
        }

        public string FormattedUrl
        {
            get { return txtUrl.Text; }
        }

        public void LoadForm(string url)
        {
            this.initUrl = url;

            txtUrl.Text = url;

            rbGenericUrl.Checked = true;
        }

        private void rbSpecificUrl_CheckedChanged(object sender, EventArgs e)
        {
            rbGenericUrl.Checked = !rbSpecificUrl.Checked;

            if (rbSpecificUrl.Checked)
            {
                txtUrl.Text = initUrl;
            }
        }

        private void rbGenericUrl_CheckedChanged(object sender, EventArgs e)
        {
            rbSpecificUrl.Checked = !rbGenericUrl.Checked;

            chkAllSubDomains.Enabled = rbGenericUrl.Checked;
            chkAllSubDomains.Checked = false;

            GenerateUrl();
        }

        private void chkAllSubDomains_CheckedChanged(object sender, EventArgs e)
        {
            GenerateUrl();
        }

        private void chkAllDomainUrls_CheckedChanged(object sender, EventArgs e)
        {
            GenerateUrl();
        }

        private void GenerateUrl()
        {
            string domain = DomainParser.GetDomain(initUrl);

            if (chkAllSubDomains.Checked)
            {
                domain = "*." + domain;
            }

            txtUrl.Text = domain + "/*";
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.OK;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
        }
    }
}
