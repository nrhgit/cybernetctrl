﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using CybernetCtrl.Modal;
using MetroFramework.Forms;
using CybernetCtrl.Common;

namespace CybernetCtrl.Common.Forms
{
    public partial class ChangeAdminPasswordForm : MetroForm
    {
        public ChangeAdminPasswordForm()
        {
            InitializeComponent();
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            if (IsValidInput())
            {
                AdminPassword.Clear();

                AdminPassword.SetNewPassword(txtNewPassword.Text);

                Logger.Log("Administrator password is changed.", LogType.Information, ApplicationType.Client);

                this.DialogResult = System.Windows.Forms.DialogResult.OK;
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
        }

        private bool IsValidInput()
        {
            bool isValid = AdminPassword.IsCurrentPasswordValid(txtCurrentPassword.Text);

            if (isValid == false)
            {
                Logger.Log("Current password entered is incorrect.", LogType.Information, ApplicationType.Client);
                MessageBox.Show("Current password entered is incorrect.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }

            if (String.IsNullOrEmpty(txtNewPassword.Text))
            {
                Logger.Log("New password cannot be empty.", LogType.Information, ApplicationType.Client);
                MessageBox.Show("New password cannot be empty.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }

            if (txtNewPassword.Text != txtConfirmPassword.Text)
            {
                Logger.Log("New password and confirm password does not match.", LogType.Information, ApplicationType.Client);
                MessageBox.Show("New password and confirm password does not match.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }

            return true;
        }
    }
}
