﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CybernetCtrl.Common
{
    [Serializable]
    public enum AccessModeType
    {
        SelectiveAllow,
        SelectiveBlock
    }
}
