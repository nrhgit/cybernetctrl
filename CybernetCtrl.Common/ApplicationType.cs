﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CybernetCtrl.Common {
    public enum ApplicationType {
        Client,
        Server,
        TCP,
        Misc
    }
}
