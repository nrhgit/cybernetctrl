﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Text;
using System.Diagnostics;
using System.Net.Sockets;
using System.IO;
using System.Text.RegularExpressions;

namespace CybernetCtrl.Common {
    public class IPReader {
        public static string LocalIP {
            get {
#if LOCAL
                return "127.0.0.1";
#else
                string localIP = "";

                ProcessStartInfo processInfo = new ProcessStartInfo("ipconfig");
                processInfo.CreateNoWindow = true;
                processInfo.UseShellExecute = false;
                processInfo.RedirectStandardOutput = true;

                Process process = Process.Start(processInfo);

                StreamReader output = process.StandardOutput;
                string config = output.ReadToEnd();

                List<string> ips = new List<string>();
                Regex regex = new Regex(@"IPv4 Address(\.\s)+:(\s)+(?<ip>[\d]{1,3}\.[\d]{1,3}\.[\d]{1,3}\.[\d]{1,3})", RegexOptions.Compiled);
                MatchCollection matches = regex.Matches(config);
                if (matches != null && matches.Count > 0) {
                    foreach (Match match in matches) {
                        ips.Add(match.Groups["ip"].Value);
                    }
                }

                List<string> gateways = new List<string>();
                string[] lines = config.Split(Environment.NewLine.ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
                foreach (string line in lines) {
                    if (line.Contains("Default Gateway")) {
                        regex = new Regex(@"Default Gateway[\.\s]+:[\s]+(?<gateway>[\d]{1,3}\.[\d]{1,3}\.[\d]{1,3}\.[\d]{1,3})", RegexOptions.Compiled);
                        matches = regex.Matches(line);
                        if (matches != null && matches.Count > 0) {
                            gateways.Add(matches[0].Groups["gateway"].Value);
                        } else {
                            gateways.Add("");
                        }
                    }
                }

                int index = -1;
                for (int i = 0; i < gateways.Count; i++) {
                    string gateway = gateways[i];
                    if (String.IsNullOrEmpty(gateway) == false) {
                        index = i;
                        break;
                    }
                }

                if (ips.Count > index && index != -1) {
                    localIP = ips[index];
                }

                Logger.Log("Local IP address: " + localIP, LogType.Information, ApplicationType.Misc);

                return localIP;
#endif
            }
        }

        public static List<NetworkNode> GetIps() {
            List<NetworkNode> ips = new List<NetworkNode>();

            ProcessStartInfo processInfo = new ProcessStartInfo("net", "view");
            processInfo.CreateNoWindow = true;
            processInfo.UseShellExecute = false;
            processInfo.RedirectStandardOutput = true;
            Process process = Process.Start(processInfo);

            StreamReader output = process.StandardOutput;
            List<string> machines = GetMachinesList(output.ReadToEnd());
            foreach (string machine in machines) {
                string ip = GetIPAddresses(machine);
                ips.Add(new NetworkNode() { ComputerName = machine, IP = ip });
            }

            return ips;
        }

        // this function basically uses net view /all command
        private static List<string> GetMachinesList(string machinesList) {
            List<string> machines = new List<string>();
            string[] lines = machinesList.Split(Environment.NewLine.ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
            foreach (string line in lines) {
                if (line.Trim().StartsWith(@"\")) {
                    string[] parts = line.Split(" ".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
                    if (parts.Length > 0) {
                        string machineName = parts[0].Trim(@" \".ToCharArray());
                        if (machineName.Equals(Environment.MachineName, StringComparison.OrdinalIgnoreCase) == false) {
                            machines.Add(machineName);
                        }
                    }
                }
            }

            return machines;
        }

        private static string GetIPAddresses(string server) {
            try {
                IPHostEntry heserver = Dns.Resolve(server);
                return heserver.AddressList[0].ToString();
            } catch { }

            return string.Empty;
        }
    }

    public class NetworkNode {
        public string IP { get; set; }

        public string ComputerName { get; set; }

        public string Serial { get; set; }
    }
}
