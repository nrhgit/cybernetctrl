﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;
using System.IO;

namespace CybernetCtrl.Common {
    public class LoadSave {
        public static void Save(Type type, object o, string filename) {
            XmlSerializer serializer = new XmlSerializer(type);
            using (TextWriter textWriter = new StreamWriter(filename)) {
                serializer.Serialize(textWriter, o);
            }
        }

        public static object Load(Type type, string filename) {
            object o = null;
            if (File.Exists(filename)) {
                XmlSerializer deserializer = new XmlSerializer(type);
                using (TextReader textReader = new StreamReader(filename)) {
                    o = deserializer.Deserialize(textReader);
                }
            }

            return o;
        }
    }
}
