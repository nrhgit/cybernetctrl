﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace CybernetCtrl.Common {
    public class Logger {
        private static bool isLogEnabled = false;
        private static string logDir = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) + @"\" + Settings.LocalDir + @" Logs\";
        private static string logStatusFile = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) + @"\" + Settings.LocalDir + @" Logs\log.txt";

        public static void Init() {
            if (Directory.Exists(logDir) == false) {
                Directory.CreateDirectory(logDir);
            }

            if (File.Exists(logStatusFile)) {
                isLogEnabled = File.ReadAllText(logStatusFile) == "1";
            }
        }

        public static bool IsLogEnabled {
            get { return isLogEnabled; }
            set {
                isLogEnabled = value;
                File.WriteAllText(logStatusFile, value ? "1" : "0");
            }
        }

        public static string LogDir {
            get { return logDir; }
        }

        public static void Log(string message, LogType logType, ApplicationType applicationType) {
            if (isLogEnabled == false) return;

            string filename = GetFileName(applicationType);

            string log = DateTime.Now.ToString("yyyy-MMM-dd HH:mm:ss.fff") + " : " + logType.ToString().ToUpper() + " : " + message + Environment.NewLine;

            AppendText(filename, log);
        }

        private static void AppendText(string path, string log) {
            FileStream fs = new FileStream(path, FileMode.OpenOrCreate, FileAccess.ReadWrite, FileShare.ReadWrite);
            StreamWriter str = new StreamWriter(fs);

            str.BaseStream.Seek(0, SeekOrigin.End);
            str.Write(log);

            str.Flush();
            str.Close();
            fs.Close();
        }

        private static string GetFileName(ApplicationType applicationType) {
            if (Directory.Exists(logDir) == false) {
                Directory.CreateDirectory(logDir);
            }

            string filename = logDir;

            switch (applicationType) {
                case ApplicationType.Client: filename += "CybernetCtrl.Client" + "_"; break;
                case ApplicationType.Server: filename += "CybernetCtrl.Server" + "_"; break;
                case ApplicationType.TCP: filename += "CybernetCtrl.TCP" + "_"; break;
                case ApplicationType.Misc: filename += "CybernetCtrl.Misc" + "_"; break;
            }

            filename += DateTime.Now.ToString("yyyy-MMM-dd") + ".log";

            return filename;
        }
    }

    public enum LogType {
        Error,
        Warning,
        Information
    }
}
