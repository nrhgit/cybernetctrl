﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CybernetCtrl.Common {
    public class ProcessConfig {
        public string Regedit { get; set; }

        public string MSConfig { get; set; }

        public string InternetOptions { get; set; }

        public string Rundll32 { get; set; }

        public string AddRemovePrograms { get; set; }

        public ProcessConfig() {
            Regedit = "0";
            MSConfig = "0";
            InternetOptions = "0";
            Rundll32 = "0";
            AddRemovePrograms = "0";
        }
    }
}
