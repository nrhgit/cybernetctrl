﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Win32;

namespace CybernetCtrl.Common {
    public class RegistryAdapter {
        public static void CreateRegistryKey(RegistryKey root, string key) {
            if (IsRegistryKeyExists(root, key) == false) {
                root.CreateSubKey(key);
            }
        }

        public static void CreateRegistryKeyValue(RegistryKey root, string key, string name, object value) {
            if (IsRegistryValueExists(root, key, name) == false) {
                RegistryKey registrykey = root.OpenSubKey(key, true);
                registrykey.SetValue(name, value);
            }
        }

        public static void CreateRegistryKeyValue(RegistryKey root, string key, string name, object value, RegistryValueKind valueKind) {
            if (IsRegistryValueExists(root, key, name) == false) {
                RegistryKey registrykey = root.OpenSubKey(key, true);
                registrykey.SetValue(name, value, valueKind);
            }
        }

        public static bool IsRegistryKeyExists(RegistryKey root, string key) {
            RegistryKey registryKey = root.OpenSubKey(key, false);

            return registryKey != null;
        }

        public static bool IsRegistryValueExists(RegistryKey root, string key, string name) {
            RegistryKey registryKey = root.OpenSubKey(key, false);
            if (registryKey != null) {
                object value = registryKey.GetValue(name);

                return value != null;
            }

            return false;
        }

        public static void RemoveKey(RegistryKey root, string key) {
            if (IsRegistryKeyExists(root, key)) {
                root.DeleteSubKey(key);
            }
        }

        public static void RemoveValue(RegistryKey root, string key, string name) {
            if (IsRegistryValueExists(root, key, name)) {
                RegistryKey registrykey = root.OpenSubKey(key, true);
                registrykey.DeleteValue(name);
            }
        }

        public static Dictionary<string, string> GetRegistryKeyValues(RegistryKey root, string key) {
            Dictionary<string, string> vals = new Dictionary<string, string>();
            if (IsRegistryKeyExists(root, key)) {
                RegistryKey registrykey = root.OpenSubKey(key);

                string[] names = registrykey.GetValueNames();
                foreach (string name in names) {
                    string val = (string)registrykey.GetValue(name);

                    vals.Add(name, val);
                }
            }

            return vals;
        }
    }
}
