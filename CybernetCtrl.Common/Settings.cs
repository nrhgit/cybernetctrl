﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CybernetCtrl.Common
{
    public class Settings
    {
        public static int PageSize = 50;

        public static int ArchiveLimit = 1000;

        public const string LocalDir = "CybernetCtrl";

        public static int WatcherTime = 5000;

        public static decimal CompatibleClientVersion = 1.2M;

        public static bool IsCompatible = false;
    }
}
