﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace CybernetCtrl.Common {
    public class StringBinaryConverter {
        public static string BinaryFileToString(string filename) {
            using (FileStream stream = new FileStream(filename, FileMode.Open, FileAccess.Read)) {
                byte[] bytes = new byte[stream.Length];
                stream.Read(bytes, 0, Convert.ToInt32(stream.Length));
                return Convert.ToBase64String(bytes, Base64FormattingOptions.InsertLineBreaks);
            }
        }

        public static void StringToBinaryFile(string filename, string imageStream) {
            byte[] bytes = Convert.FromBase64String(imageStream);
            using (FileStream stream = new FileStream(filename, FileMode.OpenOrCreate, FileAccess.ReadWrite, FileShare.ReadWrite)) {
                stream.Write(bytes, 0, bytes.Length);
            }
        }
    }
}
