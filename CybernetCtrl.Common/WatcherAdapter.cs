﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Diagnostics;
using System.Windows.Forms;
using System.Threading;

namespace CybernetCtrl.Common {
    public class WatcherAdapter {
        public static void Start(string processName) {
            Thread watcherThread = new Thread(new ParameterizedThreadStart(WatcherThread));
            watcherThread.IsBackground = true;
            watcherThread.Start(processName);
        }

        public static void WatcherThread(object o) {
            string processName = (string)o;

            while (true) {
                Process[] processes = Process.GetProcessesByName("Wt");
                if (processes.Length == 0) {
                    ProcessStartInfo startInfo = new ProcessStartInfo(Application.StartupPath + @"\Wt.exe", processName);
                    startInfo.WindowStyle = ProcessWindowStyle.Hidden;
                    Process.Start(startInfo);
                }

                Thread.Sleep(Settings.WatcherTime);
            }
        }
    }
}
