﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Diagnostics;
using CybernetCtrl.Common;
using System.IO;
using Microsoft.Win32;
using System.Windows.Forms;
using System.Reflection;

namespace CybernetCtrl.Common {
    public class WinFirewall {
        private const string FIREWALL_KEY = @"SYSTEM\CurrentControlSet\services\SharedAccess\Parameters\FirewallPolicy\FirewallRules";

        private static string localDir = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) + @"\" + Settings.LocalDir;

        public static void EnablePorts() {
            string scriptPath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + @"\Scripts\Ports.vbs";
            string args = String.Format(@"""{0}""", scriptPath);
            ProcessStartInfo processInfo = new ProcessStartInfo("cscript");
            processInfo.Arguments = args;
            processInfo.CreateNoWindow = true;
            processInfo.UseShellExecute = false;

            Process process = Process.Start(processInfo);
        }

        public static void ActivateApplication(string name) {
            string exePath = Assembly.GetEntryAssembly().Location;
            string scriptPath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + @"\Scripts\Process.vbs";
            string args = String.Format(@"""{0}"" /name:""{1}"" /path:""{2}""", scriptPath, name, exePath);

            ProcessStartInfo processInfo = new ProcessStartInfo("cscript");
            processInfo.Arguments = args;
            processInfo.CreateNoWindow = true;
            processInfo.UseShellExecute = false;

            Process process = Process.Start(processInfo);
        }

        public static void SetFirewallSettings(List<string> apps) {
            if (Environment.OSVersion.Version.Major > 5) {
                foreach (string app in apps) {
                    if (IsApplicationEnabled(app) == false) {
                        string path = Application.StartupPath + @"\" + app;
                        EnableApplication(app, path);
                    }
                }
            } else {
                DisableXpFirewall();
            }
        }

        private static void DisableXpFirewall() {
            if (Directory.Exists(localDir) == false) {
                Directory.CreateDirectory(localDir);
            }

            string batchFile = localDir + @"\xp.bat";

            if (File.Exists(batchFile) == false) {
                File.WriteAllText(batchFile, "netsh firewall set opmode disable");
            }

            ProcessStartInfo ProcessInfo = new ProcessStartInfo(batchFile);
            ProcessInfo.CreateNoWindow = true;
            ProcessInfo.UseShellExecute = false;

            Process.Start(ProcessInfo);
        }

        private static bool IsApplicationEnabled(string app) {
            Dictionary<string, string> names = RegistryAdapter.GetRegistryKeyValues(Registry.LocalMachine, FIREWALL_KEY);

            foreach (string name in names.Keys) {
                string val = names[name].ToLower();

                if(val.Contains(app)){
                    // check whether it is active
                    bool active = val.Contains("active=true");
                    bool allowed = val.Contains("action=allow");

                    if (active == false || allowed == false) {
                        // remove if it is not active
                        RegistryAdapter.RemoveValue(Registry.LocalMachine, FIREWALL_KEY, name);
                        return false;
                    }

                    return true;
                }
            }

            return false;
        }

        private static void EnableApplication(string app, string path) {
            string tcpTemplate = @"v###version|Action=Allow|Active=TRUE|Dir=In|Protocol=6|Profile=Public|App=###path|Name=###name|Desc=###app|Defer=User|";
            string udpTemplate = @"v###version|Action=Allow|Active=TRUE|Dir=In|Protocol=17|Profile=Public|App=###path|Name=###name|Desc=###app|";

            string version = "2.00";
            switch (Environment.OSVersion.Version.Minor) {
                case 0: version = "2.00"; break;
                case 1: version = "2.10"; break;
                case 2: version = "2.20"; break;
            }

            tcpTemplate = tcpTemplate.Replace("###version", version);
            udpTemplate = udpTemplate.Replace("###version", version); 

            tcpTemplate = tcpTemplate.Replace("###path", path);
            udpTemplate = udpTemplate.Replace("###path", path);

            tcpTemplate = tcpTemplate.Replace("###name", app);
            udpTemplate = udpTemplate.Replace("###name", app);

            tcpTemplate = tcpTemplate.Replace("###app", app);
            udpTemplate = udpTemplate.Replace("###app", app);

            RegistryAdapter.CreateRegistryKeyValue(Registry.LocalMachine, FIREWALL_KEY, app + Guid.NewGuid().ToString(), tcpTemplate);
            RegistryAdapter.CreateRegistryKeyValue(Registry.LocalMachine, FIREWALL_KEY, app + Guid.NewGuid().ToString(), udpTemplate);
        }
    }
}
