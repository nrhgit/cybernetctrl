﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SQLite;
using System.IO;
using System.Reflection;
using System.Text;

namespace CybernetCtrl.Database
{    
    public class DatabaseAdapter
    {
        private const string localDirName = CybernetCtrl.Common.Settings.LocalDir;

        public static void ClearVisited()
        {
            string sql = "delete from VisitedUrl";

            ExecuteSql(sql, DatabaseType.WebSitesVisited);
        }

        public static void ClearBlocked()
        {
            string sql = "delete from BlockedUrl";

            ExecuteSql(sql, DatabaseType.WebSitesBlocked);
        }

        public static int GetLastInsertID(string table, DatabaseType databaseType)
        {
            SQLiteConnection connection = GetConnection(databaseType);

            return SqlComm.GetLastInsertID(table, connection);
        }

        public static int ExecuteSql(string sql, DatabaseType databaseType)
        {
            return ExecuteSql(sql, null, databaseType);
        }

        public static int ExecuteSql(string sql, List<SQLiteParameter> parameters, DatabaseType databaseType)
        {
            SQLiteConnection connection = GetConnection(databaseType);

            return SqlComm.ExecuteSql(connection, sql, parameters);
        }

        public static DataTable GetData(string sql, string arvhiveFile)
        {
            SQLiteConnection connection = GetConnection(arvhiveFile);

            return SqlComm.GetData(connection, sql);
        }

        public static DataTable GetData(string sql, DatabaseType databaseType)
        {
            SQLiteConnection connection = GetConnection(databaseType);

            return SqlComm.GetData(connection, sql);
        }

        public static Dictionary<string, object> GetScalarData(string sql, DatabaseType databaseType)
        {
            return GetScalarData(sql, null, databaseType);
        }

        public static Dictionary<string, object> GetScalarData(string sql, List<SQLiteParameter> parameters, DatabaseType databaseType)
        {
            SQLiteConnection connection = GetConnection(databaseType);

            return SqlComm.GetScalarData(connection, sql, parameters);
        }

        public static SQLiteParameter CreateParameter(string name, DbType dbType, object value)
        {
            SQLiteParameter parameter = new SQLiteParameter();
            parameter.DbType = dbType;
            parameter.ParameterName = name;
            parameter.Value = value;

            return parameter;
        }

        public static void CreateVersionBackup(DatabaseType databaseType, decimal version)
        {
            string backFileName = string.Empty;
            switch (databaseType)
            {
                // client
                case DatabaseType.ApplicationData:
                    backFileName = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) + @"\" + localDirName + String.Format(@"\db_{0}.s3db", version);
                    break;
                case DatabaseType.WebSitesVisited:
                    backFileName = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) + @"\" + localDirName + String.Format( @"\visited_{0}.s3db", version);
                    break;
                case DatabaseType.WebSitesBlocked:
                    backFileName = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) + @"\" + localDirName + String.Format(@"\blocked_{0}.s3db", version);
                    break;

                // Server
                case DatabaseType.ServerApplicationData:
                    backFileName = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) + @"\" + localDirName + String.Format(@"\server_{0}.s3db", version);
                    break;
                case DatabaseType.ServerWebSitesVisited:
                    backFileName = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) + @"\" + localDirName + String.Format(@"\visited_server_{0}.s3db", version);
                    break;
                case DatabaseType.ServerWebSitesBlocked:
                    backFileName = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) + @"\" + localDirName + String.Format(@"\blocked_server_.s3db", version);
                    break;
            }

            CreateBackup(backFileName, databaseType);
        }

        public static void CreateBackup(string destinationFile, DatabaseType databaseType)
        {
            string localFilename = string.Empty;
            switch (databaseType)
            {
                // client
                case DatabaseType.ApplicationData:
                    localFilename = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) + @"\" + localDirName + @"\db.s3db";
                    break;
                case DatabaseType.WebSitesVisited:
                    localFilename = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) + @"\" + localDirName + @"\visited.s3db";
                    break;
                case DatabaseType.WebSitesBlocked:
                    localFilename = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) + @"\" + localDirName + @"\blocked.s3db";
                    break;

                // server
                case DatabaseType.ServerApplicationData:
                    localFilename = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) + @"\" + localDirName + @"\server.s3db";
                    break;
                case DatabaseType.ServerWebSitesVisited:
                    localFilename = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) + @"\" + localDirName + @"\visited_server.s3db";
                    break;
                case DatabaseType.ServerWebSitesBlocked:
                    localFilename = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) + @"\" + localDirName + @"\blocked_server.s3db";
                    break;
            }

            if (File.Exists(localFilename))
            {
                File.Copy(localFilename, destinationFile, true);
            }
        }

        public static void CreateArchive(DatabaseType databaseType)
        {
            string localFilename = string.Empty, destinationFile = string.Empty, originalFilename = string.Empty;
            switch (databaseType)
            {
                case DatabaseType.ServerWebSitesVisited:
                    originalFilename = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + @"\visited_server.s3db";
                    localFilename = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) + @"\" + localDirName + @"\visited_server.s3db";
                    destinationFile = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) + @"\" + localDirName + String.Format(@"\archive_visited_server_{0}.s3db", DateTime.Now.ToString("dd_MMM_yyyy"));
                    break;
            }

            // archive existing data
            if (File.Exists(localFilename))
            {
                File.Copy(localFilename, destinationFile, true);
            }

            // create fresh copy
            if (File.Exists(originalFilename))
            {
                File.Copy(originalFilename, localFilename, true);
            }
        }

        private static SQLiteConnection GetConnection(DatabaseType databaseType)
        {
            string originalFilename = string.Empty, localFilename = string.Empty;

            switch(databaseType)
            {
                    // client
                case DatabaseType.ApplicationData:
                    originalFilename = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + @"\db.s3db";
                    localFilename = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) + @"\" + localDirName + @"\db.s3db";
                    break;
                case DatabaseType.WebSitesVisited:
                    originalFilename = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + @"\visited.s3db";
                    localFilename = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) + @"\" + localDirName  + @"\visited.s3db";
                    break;
                case DatabaseType.WebSitesBlocked:
                    originalFilename = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + @"\blocked.s3db";
                    localFilename = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) + @"\" + localDirName + @"\blocked.s3db";
                    break;

                    // server
                case DatabaseType.ServerApplicationData:
                    originalFilename = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + @"\server.s3db";
                    localFilename = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) + @"\" + localDirName + @"\server.s3db";
                    break;
                case DatabaseType.ServerWebSitesVisited:
                    originalFilename = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + @"\visited_server.s3db";
                    localFilename = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) + @"\" + localDirName + @"\visited_server.s3db";
                    break;
                case DatabaseType.ServerWebSitesBlocked:
                    originalFilename = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + @"\blocked_server.s3db";
                    localFilename = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) + @"\" + localDirName + @"\blocked_server.s3db";
                    break;
            }

            if (File.Exists(localFilename) == false)
            {
                if (File.Exists(originalFilename) == false)
                {
                    // TODO: show message that original file not found. needs reinstallation
                }

                if (Directory.Exists(Path.GetDirectoryName(localFilename)) == false)
                {
                    Directory.CreateDirectory(Path.GetDirectoryName(localFilename));
                }

                File.Copy(originalFilename, localFilename, true);
            }

            string connectionString = String.Format("Data Source={0};Version=3;", localFilename);

            SQLiteConnection connection = new SQLiteConnection(connectionString);

            if (DatabaseUpgrader.IsUpgradeComplete(databaseType) == false)
            {
                decimal currentVersion = GetCurrentVersion(connection);
                decimal latestVersion = DatabaseUpgrader.Upgrade(databaseType, connection, currentVersion);
                if (latestVersion > currentVersion)
                {
                    SaveLatestVersion(connection, latestVersion);
                }
            }

            return connection;
        }

        private static SQLiteConnection GetConnection(string archiveFile)
        {
            string filename = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) + @"\" + localDirName + @"\" + archiveFile + ".s3db";

            if (File.Exists(filename))
            {
                string connectionString = String.Format("Data Source={0};Version=3;", filename);

                return new SQLiteConnection(connectionString);
            }

            return null;
        }

        private static decimal GetCurrentVersion(SQLiteConnection connection)
        {
            Dictionary<string, object> values = SqlComm.GetScalarData(connection, "select * from Version", null);

            if (values != null)
            {
                return Convert.ToDecimal(values["VersionNumber"]);
            }

            return 0;
        }

        private static void SaveLatestVersion(SQLiteConnection connection, decimal latestVersion)
        {
            string sql = String.Format("update Version set VersionNumber = {0}", latestVersion);

            SqlComm.ExecuteSql(connection, sql);
        }
    }
}
