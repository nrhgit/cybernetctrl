﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CybernetCtrl.Database
{
    public enum DatabaseType
    {
        WebSitesVisited,
        WebSitesBlocked,
        ApplicationData, 
        ServerApplicationData,
        ServerWebSitesVisited,
        ServerWebSitesBlocked
    }
}
