﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SQLite;
using System.Text;

namespace CybernetCtrl.Database
{
    internal static class DatabaseUpgrader
    {
        private static ApplicationDataUpgrader applicationDataUpgrader;
        private static ServerApplicationDataUpgrader serverApplicationDataUpgrader;
        private static ServerWebSitesBlockedUpgrader serverWebsitesBlockedUpgrader;
        private static ServerWebSitesVisitedUpgrader serverWebsitesVisitedUpgrader;
        private static WebSitesBlockedUpgrader websitesBlockedUpgrader;
        private static WebSitesVisitedUpgrader websitesVisitedUpgrader;

        static DatabaseUpgrader()
        {
            applicationDataUpgrader = new ApplicationDataUpgrader();
            serverApplicationDataUpgrader = new ServerApplicationDataUpgrader();
            serverWebsitesBlockedUpgrader = new ServerWebSitesBlockedUpgrader();
            serverWebsitesVisitedUpgrader = new ServerWebSitesVisitedUpgrader();
            websitesBlockedUpgrader = new WebSitesBlockedUpgrader();
            websitesVisitedUpgrader = new WebSitesVisitedUpgrader();
        }

        public static bool IsUpgradeComplete(DatabaseType databaseType)
        {
            DatabaseUpgraderBase upgrader = null;
            switch (databaseType)
            {
                case DatabaseType.ApplicationData: upgrader = applicationDataUpgrader; break;
                case DatabaseType.ServerApplicationData: upgrader = serverApplicationDataUpgrader; break;
                case DatabaseType.ServerWebSitesBlocked: upgrader = serverWebsitesBlockedUpgrader; break;
                case DatabaseType.ServerWebSitesVisited: upgrader = serverWebsitesVisitedUpgrader; break;
                case DatabaseType.WebSitesBlocked: upgrader = websitesBlockedUpgrader; break;
                case DatabaseType.WebSitesVisited: upgrader = websitesVisitedUpgrader; break;
            }

            if (upgrader != null)
            {
                return upgrader.IsUpgradeComplete;
            }

            return false;
        }

        public static decimal Upgrade(DatabaseType databaseType, SQLiteConnection connection, decimal currentVersion)
        {
            DatabaseUpgraderBase upgrader = null;
            switch (databaseType)
            {
                case DatabaseType.ApplicationData: upgrader = applicationDataUpgrader; break;
                case DatabaseType.ServerApplicationData: upgrader = serverApplicationDataUpgrader; break;
                case DatabaseType.ServerWebSitesBlocked: upgrader = serverWebsitesBlockedUpgrader; break;
                case DatabaseType.ServerWebSitesVisited: upgrader = serverWebsitesVisitedUpgrader; break;
                case DatabaseType.WebSitesBlocked: upgrader = websitesBlockedUpgrader; break;
                case DatabaseType.WebSitesVisited: upgrader = websitesVisitedUpgrader; break;
            }

            if (upgrader != null)
            {
                DatabaseAdapter.CreateVersionBackup(databaseType, currentVersion);

                upgrader.Upgrade(connection, currentVersion);

                return upgrader.LatestVersion;
            }

            return 0;
        }
    }

    internal class DatabaseUpgraderBase
    {
        protected bool isUpgradeComplete;
        protected Dictionary<decimal, string> statements = new Dictionary<decimal, string>();
        protected DatabaseType databaseType;

        public decimal LatestVersion
        {
            get
            {
                decimal version = 0;
                foreach (decimal statementVersion in statements.Keys)
                {
                    if (statementVersion > version)
                    {
                        version = statementVersion;
                    }
                }

                return version;
            }
        }

        public bool IsUpgradeComplete
        {
            get { return isUpgradeComplete; }
            set { isUpgradeComplete = value; }
        }

        public void Upgrade(SQLiteConnection connection, decimal currentVersion)
        {
            foreach (decimal value in statements.Keys)
            {
                if (value > currentVersion)
                {
                    string sql = statements[value];

                    SqlComm.ExecuteSql(connection, sql);
                }
            }

            this.isUpgradeComplete = true;
        }
    }

    internal class ApplicationDataUpgrader : DatabaseUpgraderBase
    {
        public ApplicationDataUpgrader()
        {
            databaseType = DatabaseType.ApplicationData;

            Add_Statements_0_1();

            Add_Statements_0_2();

            Add_Statements_0_3();

            Add_Statements_0_3_1();

            Add_Statements_0_3_11();

            Add_Statements_0_3_14();

            Add_Statements_0_3_15();

            Add_Statements_0_3_16();

            Add_Statements_0_3_17();

            Add_Statements_0_3_18();

            Add_Statements_0_3_19();

            Add_Statements_0_4();
        }

        // version 0.1
        private void Add_Statements_0_1()
        {
            string sql = @"CREATE TABLE [Version] ([VersionNumber] decimal(10,5)  NOT NULL); insert into Version values(0.1);";

            statements.Add(0.1M, sql);
        }

        // version 0.2
        private void Add_Statements_0_2()
        {
            string sql = @"CREATE TABLE [Group] (
[ID] INTEGER  NOT NULL PRIMARY KEY AUTOINCREMENT,
[Name] VARCHAR(32)  NOT NULL,
[AllowedDuration] INTEGER  NOT NULL
);
alter table AllowedUrl add GroupID INTEGER NULL;";

            statements.Add(0.2M, sql);
        }

        // version 0.3
        private void Add_Statements_0_3()
        {
            string sql = @"
drop table [Group];

CREATE TABLE [Group] (
[ID] VARCHAR(40) NOT NULL PRIMARY KEY,
[Name] VARCHAR(32)  NOT NULL,
[AllowedDuration] INTEGER  NOT NULL);

drop table [AllowedUrl];

CREATE TABLE [AllowedUrl] (
[Url] TEXT  NOT NULL, 
GroupID varchar(40) NULL);";

            statements.Add(0.3M, sql);
        }

        // version 0.31
        private void Add_Statements_0_3_1()
        {
            string sql = @"
drop table [Group];

CREATE TABLE UrlGroup (
[ID] VARCHAR(40) NOT NULL PRIMARY KEY,
[Name] VARCHAR(32)  NOT NULL,
[AllowedDuration] INTEGER  NOT NULL);";

            statements.Add(0.31M, sql);
        }

        // version 0.311
        private void Add_Statements_0_3_11()
        {
            string sql = @"
drop table AllowedUrl;

CREATE TABLE AllowedUrl (
[Url] TEXT  NOT NULL PRIMARY KEY, 
GroupID varchar(40) NULL);";

            statements.Add(0.311M, sql);
        }

        // version 0.314
        private void Add_Statements_0_3_14()
        {
            string sql = @"
drop table UrlGroup;

CREATE TABLE UrlGroup (
[ID] VARCHAR(40) NOT NULL PRIMARY KEY,
[Name] VARCHAR(32)  NOT NULL,
[AllowedDuration] INTEGER  NOT NULL,
[SessionDuration] INTEGER  NOT NULL);";

            statements.Add(0.314M, sql);
        }

        // version 0.315
        private void Add_Statements_0_3_15()
        {
            string sql = @"
CREATE TABLE Session (
StartTime datetime NOT NULL,
SessionDate datetime NOT NULL);";

            statements.Add(0.315M, sql);
        }

        // version 0.316
        private void Add_Statements_0_3_16()
        {
            string sql = @"
drop table Session;

CREATE TABLE Session (
StartTime datetime NOT NULL,
SessionDate datetime NOT NULL,
UrlGroup VARCHAR(40) NOT NULL);";

            statements.Add(0.316M, sql);
        }

        // version 0.317
        private void Add_Statements_0_3_17()
        {
            string sql = @"CREATE TABLE AccessMode (ModeType INTEGER NOT NULL);";

            statements.Add(0.317M, sql);
        }

        // version 0.318
        private void Add_Statements_0_3_18()
        {
            string sql = @"CREATE TABLE BlockedUrl ([Url] TEXT  NOT NULL);";

            statements.Add(0.318M, sql);
        }

        // version 0.319
        private void Add_Statements_0_3_19()
        {
            string sql = @"DROP TABLE AccessMode;";

            statements.Add(0.319M, sql);

            sql = @"CREATE TABLE AccessMode (ModeType INTEGER NOT NULL PRIMARY KEY);";

            statements.Add(0.320M, sql);
        }

        // version 0.4
        private void Add_Statements_0_4() {
            string sql = @"CREATE TABLE [UserFileLimit] ([FileLimit] DECIMAL(10,5) DEFAULT '100' NULL)";

            statements.Add(0.4M, sql);
        }
    }

    internal class ServerApplicationDataUpgrader : DatabaseUpgraderBase
    {
        public ServerApplicationDataUpgrader()
        {
            databaseType = DatabaseType.ServerApplicationData;

            Add_Statements_0_1();

            Add_Statements_0_2();

            Add_Statements_0_3();

            Add_Statements_0_3_1();

            Add_Statements_0_3_11();

            Add_Statements_0_3_12();

            Add_Statements_0_3_13();

            Add_Statements_0_3_14();

            Add_Statements_0_3_15();

            Add_Statements_0_3_16();

            Add_Statements_0_3_17();

            Add_Statements_0_4();

            Add_Statements_0_4_1();

            Add_Statements_0_4_2();
        }

        // version 0.1
        private void Add_Statements_0_1()
        {
            string sql = @"CREATE TABLE [Version] ([VersionNumber] decimal(10,5)  NOT NULL); insert into Version values(0.1);";

            statements.Add(0.1M, sql);
        }

        // version 0.2
        private void Add_Statements_0_2()
        {
            string sql = @"CREATE TABLE [Group] (
[ID] INTEGER  NOT NULL PRIMARY KEY AUTOINCREMENT,
[Name] VARCHAR(32)  NOT NULL,
[AllowedDuration] INTEGER  NOT NULL
);
alter table AllowedUrl add GroupID INTEGER NULL;";

            statements.Add(0.2M, sql);
        }

        // version 0.3
        private void Add_Statements_0_3()
        {
            string sql = @"
drop table [Group];

CREATE TABLE [Group] (
[ID] VARCHAR(40) NOT NULL PRIMARY KEY,
[Name] VARCHAR(32)  NOT NULL,
[AllowedDuration] INTEGER  NOT NULL);

drop table [AllowedUrl];

CREATE TABLE [AllowedUrl] (
[Url] TEXT  NOT NULL, 
GroupID varchar(40) NULL);";

            statements.Add(0.3M, sql);
        }

        // version 0.3.1
        private void Add_Statements_0_3_1()
        {
            string sql = @"
drop table [AllowedUrl];

CREATE TABLE [AllowedUrl] (
UserID INTEGER  NOT NULL,
[Url] TEXT  NOT NULL, 
GroupID varchar(40) NULL);";

            statements.Add(0.31M, sql);
        }

        // version 0.311
        private void Add_Statements_0_3_11()
        {
            string sql = @"
drop table [Group];

CREATE TABLE UrlGroup (
[ID] VARCHAR(40) NOT NULL PRIMARY KEY,
[Name] VARCHAR(32)  NOT NULL,
[AllowedDuration] INTEGER  NOT NULL);";

            statements.Add(0.311M, sql);
        }

        // version 0.312
        private void Add_Statements_0_3_12()
        {
            string sql = @"
drop table AllowedUrl;

CREATE TABLE AllowedUrl (
[Url] TEXT  NOT NULL PRIMARY KEY, 
GroupID varchar(40) NULL);";

            statements.Add(0.312M, sql);
        }

        // version 0.313
        private void Add_Statements_0_3_13()
        {
            string sql = @"
drop table AllowedUrl;

CREATE TABLE AllowedUrl (
[UserID] INTEGER  NOT NULL,
[Url] TEXT  NOT NULL, 
GroupID varchar(40) NULL);";

            statements.Add(0.313M, sql);
        }

        // version 0.314
        private void Add_Statements_0_3_14()
        {
            string sql = @"
drop table UrlGroup;

CREATE TABLE UrlGroup (
[ID] VARCHAR(40) NOT NULL PRIMARY KEY,
[Name] VARCHAR(32)  NOT NULL,
[AllowedDuration] INTEGER  NOT NULL,
[SessionDuration] INTEGER  NOT NULL);";

            statements.Add(0.314M, sql);
        }

        // version 0.315
        private void Add_Statements_0_3_15()
        {
            string sql = @"
CREATE TABLE AccessMode (
[UserID] INTEGER  NOT NULL,
ModeType INTEGER NOT NULL);";

            statements.Add(0.315M, sql);
        }

        // version 0.316
        private void Add_Statements_0_3_16()
        {
            string sql = @"
CREATE TABLE BlockedUrl (
[UserID] INTEGER  NOT NULL,
[Url] TEXT  NOT NULL);";

            statements.Add(0.316M, sql);
        }

        // version 0.317
        private void Add_Statements_0_3_17()
        {
            string sql = @"DROP TABLE AccessMode;";

            statements.Add(0.317M, sql);

            sql = @"CREATE TABLE AccessMode ([UserID] INTEGER  NOT NULL PRIMARY KEY, ModeType INTEGER NOT NULL);";

            statements.Add(0.318M, sql);
        }

        // version 0.4
        private void Add_Statements_0_4() {
            string sql = @"CREATE TABLE [UserFileLimit] ([UserID] INTEGER  NOT NULL PRIMARY KEY, [FileLimit] DECIMAL(10,5) DEFAULT '100' NOT NULL)";

            statements.Add(0.4M, sql);
        }

        // version 0.4.1
        private void Add_Statements_0_4_1() {
            string sql = @"DROP TABLE [User];";

            statements.Add(0.41M, sql);

            sql = @"CREATE TABLE [User] ([ID] INTEGER  PRIMARY KEY AUTOINCREMENT NOT NULL,[Name] VARCHAR(256)  NULL,[IP] VARCHAR(32)  NULL, [Serial] VARCHAR(16)  NULL)";

            statements.Add(0.411M, sql);
        }

        // version 0.4.2(
        private void Add_Statements_0_4_2() {
            string sql = @"CREATE TABLE ClientLicense([ID] INTEGER  PRIMARY KEY AUTOINCREMENT NOT NULL, [Serial] VARCHAR(32) NULL, [License] TEXT NULL);";

            statements.Add(0.42M, sql);
        }
    }

    internal class ServerWebSitesBlockedUpgrader : DatabaseUpgraderBase
    {
        public ServerWebSitesBlockedUpgrader()
        {
            databaseType = DatabaseType.ServerWebSitesBlocked;

            Add_Statements_0_1();
        }

        // version 0.1
        private void Add_Statements_0_1()
        {
            string sql = @"CREATE TABLE [Version] ([VersionNumber] decimal(10,5)  NOT NULL); insert into Version values(0.1);";

            statements.Add(0.1M, sql);
        }
    }

    internal class ServerWebSitesVisitedUpgrader : DatabaseUpgraderBase
    {
        public ServerWebSitesVisitedUpgrader()
        {
            databaseType = DatabaseType.ServerWebSitesVisited;

            Add_Statements_0_1();
        }

        // version 0.1
        private void Add_Statements_0_1()
        {
            string sql = @"CREATE TABLE [Version] ([VersionNumber] decimal(10,5)  NOT NULL); insert into Version values(0.1);";

            statements.Add(0.1M, sql);
        }
    }

    internal class WebSitesBlockedUpgrader : DatabaseUpgraderBase
    {
        public WebSitesBlockedUpgrader()
        {
            databaseType = DatabaseType.WebSitesBlocked;

            Add_Statements_0_1();
        }

        // version 0.1
        private void Add_Statements_0_1()
        {
            string sql = @"CREATE TABLE [Version] ([VersionNumber] decimal(10,5)  NOT NULL); insert into Version values(0.1);";

            statements.Add(0.1M, sql);
        }
    }

    internal class WebSitesVisitedUpgrader : DatabaseUpgraderBase
    {
        public WebSitesVisitedUpgrader()
        {
            databaseType = DatabaseType.WebSitesVisited;

            Add_Statements_0_1();
        }

        // version 0.1
        private void Add_Statements_0_1()
        {
            string sql = @"CREATE TABLE [Version] ([VersionNumber] decimal(10,5)  NOT NULL); insert into Version values(0.1);";

            statements.Add(0.1M, sql);
        }
    }
}
