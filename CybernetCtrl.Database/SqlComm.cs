﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SQLite;
using System.Text;

namespace CybernetCtrl.Database
{
    internal class SqlComm
    {
        public static int ExecuteSql(SQLiteConnection connection, string sql)
        {
            return ExecuteSql(connection, sql, null);
        }

        public static int ExecuteSql(SQLiteConnection connection, string sql, List<SQLiteParameter> parameters)
        {
            try
            {
                connection.Open();

                SQLiteCommand cmd = new SQLiteCommand(sql, connection);
                cmd.CommandType = CommandType.Text;
                if (parameters != null)
                {
                    cmd.Parameters.AddRange(parameters.ToArray());
                }
                return cmd.ExecuteNonQuery();
            }
            //catch { }
            finally
            {
                connection.Close();
            }
        }

        public static DataTable GetData(SQLiteConnection connection, string sql)
        {
            if (connection == null) return null;

            try
            {
                connection.Open();

                DataSet ds = new DataSet();
                SQLiteDataAdapter adapter = new SQLiteDataAdapter(sql, connection);
                adapter.Fill(ds);

                if (ds.Tables.Count > 0)
                {
                    return ds.Tables[0];
                }
            }
            //catch { }
            finally
            {
                connection.Close();
            }

            return null;
        }

        public static int GetLastInsertID(string table, SQLiteConnection connection)
        {
            try
            {
                connection.Open();
                string sql = "SELECT MAX(ID) from " + table;
                SQLiteCommand cmd = new SQLiteCommand(sql, connection);

                return Convert.ToInt32(cmd.ExecuteScalar());
            }
            finally
            {
                connection.Close();
            }
        }

        public static Dictionary<string, object> GetScalarData(SQLiteConnection connection, string sql, List<SQLiteParameter> parameters)
        {
            if (connection == null) return null;

            try
            {
                connection.Open();

                SQLiteCommand cmd = new SQLiteCommand(sql, connection);
                if (parameters != null)
                {
                    cmd.Parameters.AddRange(parameters.ToArray());
                }

                DataSet ds = new DataSet();
                SQLiteDataAdapter adapter = new SQLiteDataAdapter(cmd);
                adapter.Fill(ds);

                if (ds.Tables.Count > 0)
                {
                    DataTable dt = ds.Tables[0];
                    if (dt.Rows.Count > 0)
                    {
                        Dictionary<string, object> vals = new Dictionary<string, object>();
                        DataRow row = dt.Rows[0];
                        foreach (DataColumn column in dt.Columns)
                        {
                            object val = row[column];
                            vals.Add(column.ColumnName, val);
                        }

                        return vals;
                    }
                }
            }
            catch { }
            finally
            {
                connection.Close();
            }

            return null;
        }
    }
}
