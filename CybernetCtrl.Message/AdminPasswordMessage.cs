﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CybernetCtrl.Message
{
    [Serializable]
    public class AdminPasswordMessage : MessageBase
    {
        public string Password { get; set; }
    }
}
