﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CybernetCtrl.Message
{
    [Serializable]
    public class ClientLicenseMessage : MessageBase
    {
        public string License { get; set; }
    }
}
