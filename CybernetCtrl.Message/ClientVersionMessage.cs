﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CybernetCtrl.Message
{
    [Serializable]
    public class ClientVersionMessage : MessageBase
    {
        public int UserID { get; set; }

        public decimal Version { get; set; }
    }
}
