﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CybernetCtrl.Message
{
    [Serializable]
    public class GroupMessage : MessageBase
    {
        public string ID { get; set; }

        public string Name { get; set; }

        public int AllowedDuration { get; set; }

        public int SessionDuration { get; set; }
    }
}
