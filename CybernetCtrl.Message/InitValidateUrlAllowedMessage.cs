﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CybernetCtrl.Message
{
    [Serializable]
    public class InitValidateUrlAllowedMessage : MessageBase
    {
        public int UserID { get; set; }
    }
}
