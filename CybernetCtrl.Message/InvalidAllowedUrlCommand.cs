﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CybernetCtrl.Message
{
    [Serializable]
    public class InvalidUrlAllowedCommand : MessageBase
    {
        public string Url { get; set; }
    }
}
