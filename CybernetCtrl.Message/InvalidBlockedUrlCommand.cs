﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CybernetCtrl.Message
{
    [Serializable]
    public class InvalidBlockedUrlCommand : MessageBase
    {
        public string Url { get; set; }
    }
}
