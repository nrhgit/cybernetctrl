﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CybernetCtrl.Message
{
    [Serializable]
    public class InvalidGroupCommand : MessageBase
    {
        public string GroupID { get; set; }
    }
}
