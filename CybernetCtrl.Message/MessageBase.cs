﻿using CybernetCtrl.Common;
using System;
using System.Collections.Generic;
using System.Text;

namespace CybernetCtrl.Message
{
    [Serializable]
    public class MessageBase
    {
        public string SourceIP { get; set; }

        public string IP { get; set; }

        public MessageBase()
        {
            SourceIP = IPReader.LocalIP;
        }
    }
}
