﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CybernetCtrl.Message
{
    [Serializable]
    public class NodeSerialMessage : MessageBase
    {
        public string Serial { get; set; }
    }
}
