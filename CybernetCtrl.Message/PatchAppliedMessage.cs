﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CybernetCtrl.Message
{
    [Serializable]
    public class PatchAppliedMessage : MessageBase
    {
        public int UserID { get; set; }

        public string Name { get; set; }
    }
}
