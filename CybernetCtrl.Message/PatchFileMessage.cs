﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CybernetCtrl.Message
{
    [Serializable]
    public class PatchFileMessage : MessageBase
    {
        public string Name { get; set; }

        public string Content { get; set; }
    }
}
