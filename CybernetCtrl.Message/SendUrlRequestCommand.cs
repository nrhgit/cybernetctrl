﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CybernetCtrl.Message
{
    [Serializable]
    public class SendUrlRequestCommand : MessageBase
    {
        public int UserID { get; set; }

        public DateTime LastDateTime { get; set; }

    }
}
