﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CybernetCtrl.Message
{
    [Serializable]
    public class SendUrlsBlockedDetailCommand : MessageBase
    {
        public int UserID { get; set; }

        public long LastMilliSeconds { get; set; }
    }
}
