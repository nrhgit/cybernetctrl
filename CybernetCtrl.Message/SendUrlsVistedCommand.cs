﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CybernetCtrl.Message
{
    [Serializable]
    public class SendUrlsVistedCommand : MessageBase
    {
        public int UserID { get; set; }

        public long LastMilliSeconds { get; set; }
    }
}
