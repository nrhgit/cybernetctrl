﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CybernetCtrl.Message
{
    [Serializable]
    public class ServerTimeStampMessage : MessageBase
    {
        public DateTime ServerDateTime { get; set; }
    }
}
