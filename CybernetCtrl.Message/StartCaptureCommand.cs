﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CybernetCtrl.Message
{
    [Serializable]
    public class StartCaptureCommand : MessageBase
    {
        public int Port { get; set; }
    }
}
