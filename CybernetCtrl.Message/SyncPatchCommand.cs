﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CybernetCtrl.Message
{
    [Serializable]
    public class SyncPatchCommand : MessageBase
    {
        public int UserID { get; set; }
    }
}
