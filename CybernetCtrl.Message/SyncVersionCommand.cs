﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CybernetCtrl.Message
{
    [Serializable]
    public class SyncVersionCommand : MessageBase
    {
        public int UserID { get; set; }

        public decimal ExpectedClientVersion { get; set; }
    }
}
