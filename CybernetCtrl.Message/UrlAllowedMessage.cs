﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CybernetCtrl.Message
{
    [Serializable]
    public class UrlAllowedMessage : MessageBase
    {
        public string Url { get; set; }

        public string GroupID { get; set; }
    }
}
