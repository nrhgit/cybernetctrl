﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CybernetCtrl.Message
{
    [Serializable]
    public class UrlRequestMessage : MessageBase
    {
        public int UserID { get; set; }

        public string Url { get; set; }

        public string Reason { get; set; }

        public string RequestDateTime { get; set; }
    }
}
