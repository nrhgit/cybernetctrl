﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CybernetCtrl.Message
{
    [Serializable]
    public class UrlVisitedMessage : MessageBase
    {
        public int UserID { get; set; }

        public string Title { get; set; }

        public string Url { get; set; }

        public string FullUrl { get; set; }

        public string VisitedDateTime { get; set; }

        public long MilliSeconds { get; set; }
    }
}
