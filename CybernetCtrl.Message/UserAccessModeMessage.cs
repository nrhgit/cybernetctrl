﻿using System;
using System.Collections.Generic;
using System.Text;
using CybernetCtrl.Common;

namespace CybernetCtrl.Message
{
    [Serializable]
    public class UserAccessModeMessage : MessageBase
    {
        public AccessModeType ModType { get; set; }
    }
}
