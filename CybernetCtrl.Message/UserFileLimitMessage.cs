﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CybernetCtrl.Message
{
    [Serializable]
    public class UserFileLimitMessage : MessageBase
    {
        public decimal FileLimit { get; set; }
    }
}
