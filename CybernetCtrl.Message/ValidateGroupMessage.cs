﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CybernetCtrl.Message
{
    [Serializable]
    public class ValidateGroupMessage : MessageBase
    {
        public string GroupID { get; set; }
    }
}
