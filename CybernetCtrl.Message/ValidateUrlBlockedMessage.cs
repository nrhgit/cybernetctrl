﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CybernetCtrl.Message
{
    [Serializable]
    public class ValidateUrlBlockedMessage : MessageBase
    {
        public string Url { get; set; }
    }
}
