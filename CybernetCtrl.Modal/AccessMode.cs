﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SQLite;
using System.Text;
using CybernetCtrl.Database;
using CybernetCtrl.Common;

namespace CybernetCtrl.Modal
{
    public class AccessMode
    {
        public static AccessModeType ModeType
        {
            get
            {
                string sql = "select * from AccessMode";
                Dictionary<string, object> vals = DatabaseAdapter.GetScalarData(sql, DatabaseType.ApplicationData);
                if (vals != null && vals.Count > 0)
                {
                    return (AccessModeType)(int)(long)vals["ModeType"];
                }

                return AccessModeType.SelectiveBlock;
            }
            set
            {
                string sql = "delete from AccessMode";

                DatabaseAdapter.ExecuteSql(sql, DatabaseType.ApplicationData);

                sql = "insert or replace into AccessMode values(@mode)";

                List<SQLiteParameter> paramters = new List<SQLiteParameter>();
                paramters.Add(DatabaseAdapter.CreateParameter("@mode", DbType.Int32, (int)value));

                DatabaseAdapter.ExecuteSql(sql, paramters, DatabaseType.ApplicationData);
            }
        }
    }
}