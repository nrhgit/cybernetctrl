﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SQLite;
using System.Text;
using CybernetCtrl.Common;
using CybernetCtrl.Database;

namespace CybernetCtrl.Modal
{
    public class AccessModeServer
    {
        public static AccessModeType GetAccessMode(int userID)
        {
            string sql = "select * from AccessMode where UserID = " + userID;
            Dictionary<string, object> vals = DatabaseAdapter.GetScalarData(sql, DatabaseType.ServerApplicationData);
            if (vals != null && vals.Count > 0)
            {
                return (AccessModeType)(int)(long)vals["ModeType"];
            }

            return AccessModeType.SelectiveBlock;
        }

        public static void SaveAccessMode(int userID, AccessModeType modeType)
        {
            string sql = "insert or replace into AccessMode values(@userID, @mode)";

            List<SQLiteParameter> paramters = new List<SQLiteParameter>();
            paramters.Add(DatabaseAdapter.CreateParameter("@userID", DbType.Int32, userID));
            paramters.Add(DatabaseAdapter.CreateParameter("@mode", DbType.Int32, (int)modeType));

            DatabaseAdapter.ExecuteSql(sql, paramters, DatabaseType.ServerApplicationData);
        }
    }
}