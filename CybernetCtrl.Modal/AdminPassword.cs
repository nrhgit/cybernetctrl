﻿using System;
using System.Collections.Generic;
using System.Text;
using CybernetCtrl.Database;

namespace CybernetCtrl.Modal
{
    public class AdminPassword
    {
        public static string Password
        {
            get
            {
                string sql = "select * from adminpassword";
                Dictionary<string, object> vals = DatabaseAdapter.GetScalarData(sql, DatabaseType.ApplicationData);
                if (vals != null && vals.ContainsKey("Password"))
                {
                    return (string)vals["Password"];
                }

                return string.Empty;
            }
        }

        public static void Clear()
        {
            string sql = "delete from adminpassword";

            DatabaseAdapter.ExecuteSql(sql, DatabaseType.ApplicationData);
        }

        public static void SetNewPassword(string password)
        {
            string sql = String.Format("insert into adminpassword values('{0}')", password);

            DatabaseAdapter.ExecuteSql(sql, DatabaseType.ApplicationData);
        }

        public static bool IsCurrentPasswordValid(string currentPassword)
        {
            string sql = "select * from adminpassword";
            Dictionary<string, object> vals = DatabaseAdapter.GetScalarData(sql, DatabaseType.ApplicationData);
            if (vals != null)
            {
                string password = (string)vals["Password"];
                
                return currentPassword == password;

                //MessageBox.Show("Current password entered is incorrect.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            return false;
        }
    }
}
