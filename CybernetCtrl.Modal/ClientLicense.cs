﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SQLite;
using System.Text;
using CybernetCtrl.Database;

namespace CybernetCtrl.Modal
{
    [Serializable]
    public class ClientLicense
    {
        public int ID { get; set; }
        public string Serial { get; set; }
        public string License { get; set; }

        public ClientLicense() {
            ID = -1;
        }

        public void Save() {
            if (ID == -1) {
                string sql = "insert into ClientLicense values(NULL, @serial, @license)";

                List<SQLiteParameter> parameters = new List<SQLiteParameter>();
                parameters.Add(DatabaseAdapter.CreateParameter("@serial", DbType.String, Serial));
                parameters.Add(DatabaseAdapter.CreateParameter("@license", DbType.String, License));

                DatabaseAdapter.ExecuteSql(sql, parameters, DatabaseType.ServerApplicationData);
            } else {
                string sql = String.Format("update ClientLicense set Serial = '{0}', License = '{1}' where ID = {2}", Serial, License, ID);

                DatabaseAdapter.ExecuteSql(sql, DatabaseType.ServerApplicationData);
            }
        }

        public static ClientLicense GetLicense(string serial) {
            string sql = String.Format("select * from ClientLicense where Serial = '{0}'", serial);
            DataTable dt = DatabaseAdapter.GetData(sql, DatabaseType.ServerApplicationData);
            if (dt != null && dt.Rows.Count > 0) {
                DataRow row = dt.Rows[0];

                return new ClientLicense() { ID = (int)(long)row["ID"], Serial = (string)row["Serial"], License = (string)row["License"] };
            }

            return null;
        }
    }
}