﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CybernetCtrl.Modal
{
    public enum RequestStatusType
    {
        Requested,
        Approved,
        Rejected
    }
}
