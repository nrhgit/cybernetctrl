﻿using System;
using System.Collections.Generic;
using System.Text;
using CybernetCtrl.Database;

namespace CybernetCtrl.Modal
{
    public class ServerIP
    {
        public static void Save(string ip)
        {
            string sql = "delete from server";
            DatabaseAdapter.ExecuteSql(sql, DatabaseType.ApplicationData);

            sql = String.Format("insert into server values('{0}')", ip);
            DatabaseAdapter.ExecuteSql(sql, DatabaseType.ApplicationData);
        }

        public static string GetServerIP()
        {
            string ip = string.Empty;

            string sql = "select * from server";
            Dictionary<string, object> vals = DatabaseAdapter.GetScalarData(sql, DatabaseType.ApplicationData);
            if (vals != null)
            {
                ip = (string)vals["IPAddress"];
            }

            return ip;
        }
    }
}
