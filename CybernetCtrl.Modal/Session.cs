﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SQLite;
using System.Text;
using CybernetCtrl.Database;

namespace CybernetCtrl.Modal
{
    public class Session
    {
        public DateTime StartTime { get; set; }

        public DateTime SessionDate { get; set; }

        public UrlGroup UrlGroup { get; set; }

        public void Save()
        {
            string sql = "insert into Session values(@startTime, @sessionDate, @group)";

            List<SQLiteParameter> paramters = new List<SQLiteParameter>();
            paramters.Add(DatabaseAdapter.CreateParameter("@startTime", DbType.String, StartTime.ToString("yyyy-MM-dd HH:mm:ss")));
            paramters.Add(DatabaseAdapter.CreateParameter("@sessionDate", DbType.String, SessionDate.ToString("yyyy-MM-dd")));
            paramters.Add(DatabaseAdapter.CreateParameter("@group", DbType.String, UrlGroup.ID));

            DatabaseAdapter.ExecuteSql(sql, paramters, DatabaseType.ApplicationData);
        }

        public static int GetSessionCount(DateTime date, UrlGroup group)
        {
            string sql = String.Format("select * from Session where SessionDate = '{0}' and UrlGroup = '{1}'", date.ToString("yyyy-MM-dd"), group.ID);
            DataTable dt = DatabaseAdapter.GetData(sql, DatabaseType.ApplicationData);
            if (dt != null)
            {
                return dt.Rows.Count;
            }

            return 0;
        }

        public static Session GetLastSession(DateTime date, UrlGroup group)
        {
            DateTime lastTime = DateTime.MinValue;

            string sql = String.Format("select * from Session where SessionDate = '{0}' and UrlGroup = '{1}'", date.ToString("yyyy-MM-dd"), group.ID);
            DataTable dt = DatabaseAdapter.GetData(sql, DatabaseType.ApplicationData);
            if (dt != null)
            {
                foreach (DataRow row in dt.Rows)
                {
                    DateTime startTime = Convert.ToDateTime(row["StartTime"]);
                    if (startTime > lastTime)
                    {
                        lastTime = startTime;
                    }
                }
            }

            if (lastTime != DateTime.MinValue)
            {
                return new Session() { StartTime = lastTime, SessionDate = date, UrlGroup = group };
            }

            return null;
        }

        public bool IsExpired(DateTime serverDateTime)
        {
            DateTime sessionEndTime = StartTime.AddMinutes(UrlGroup.SessionDuration);

            return serverDateTime > sessionEndTime;
        }
    }
}
