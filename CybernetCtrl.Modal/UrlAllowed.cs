﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SQLite;
using System.Text;
using CybernetCtrl.Database;

namespace CybernetCtrl.Modal {
    public class UrlAllowed {
        public string Url { get; set; }

        public string GroupID { get; set; }

        public void Save() {
            string sql = "insert or replace into AllowedUrl values(@url, @groupId)";

            List<SQLiteParameter> paramters = new List<SQLiteParameter>();
            paramters.Add(DatabaseAdapter.CreateParameter("@url", DbType.String, Url));
            paramters.Add(DatabaseAdapter.CreateParameter("@groupId", DbType.String, GroupID));

            DatabaseAdapter.ExecuteSql(sql, paramters, DatabaseType.ApplicationData);
        }

        public void Block() {
            string sql = "delete from AllowedUrl where Url = @url";

            List<SQLiteParameter> parameters = new List<SQLiteParameter>();
            parameters.Add(DatabaseAdapter.CreateParameter("@url", DbType.String, Url));

            DatabaseAdapter.ExecuteSql(sql, parameters, DatabaseType.ApplicationData);
        }

        public static void Clear() {
            string sql = String.Format("delete from AllowedUrl");
            DatabaseAdapter.ExecuteSql(sql, DatabaseType.ApplicationData);
        }

        public static string IsAllowedUrl(string url, string domain) {
            string urlFormatted = url;
            urlFormatted = urlFormatted.TrimEnd("/".ToCharArray());
            if (url.StartsWith("http://")) urlFormatted = url.Replace("http://", "");
            if (url.StartsWith("https://")) urlFormatted = url.Replace("http://", "");

            string sql = String.Format("select Url from AllowedUrl where Url like '%{0}%'", domain);

            DataTable vals = DatabaseAdapter.GetData(sql, DatabaseType.ApplicationData);

            if (vals != null && vals.Rows.Count > 0) {
                foreach (DataRow row in vals.Rows) {
                    string rowUrl = (string)row["Url"];
                    string allowedUrl = rowUrl;
                    allowedUrl = allowedUrl.TrimEnd("/".ToCharArray());
                    if (allowedUrl.StartsWith("http://")) allowedUrl = allowedUrl.Replace("http://", "");
                    if (allowedUrl.StartsWith("https://")) allowedUrl = allowedUrl.Replace("http://", "");

                    // match specific URL
                    if (allowedUrl.Equals(urlFormatted, StringComparison.OrdinalIgnoreCase)) {
                        return rowUrl;
                    } else {
                        // match generic URL
                        string[] parts = allowedUrl.Split("/".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);

                        string domainPart = parts[0].TrimStart("*.".ToCharArray());
                        string urlPart = allowedUrl.TrimStart("*.".ToCharArray()).Replace(domainPart, "").Trim("/".ToCharArray()).TrimEnd(".*".ToCharArray());

                        Uri uri = new Uri(url);
                        bool isDomainAllowed = uri.Host.EndsWith(domainPart);

                        if (allowedUrl.StartsWith("*") == false && uri.Host.Length > domainPart.Length) {
                            isDomainAllowed = false;
                        }

                        string urlPath = urlFormatted.Replace(uri.Host, "").TrimStart("/".ToCharArray());
                        bool isUrlAllowed = urlPath.StartsWith(urlPart);

                        if (allowedUrl.EndsWith("*") == false && urlPath.Length > 0) {
                            isUrlAllowed = false;
                        }

                        if (isDomainAllowed && isUrlAllowed) {
                            return rowUrl;
                        }
                    }
                }
            }

            return string.Empty;
        }

        public static List<UrlAllowed> GetAllowedUrls() {
            List<UrlAllowed> urls = new List<UrlAllowed>();

            string sql = "select * from AllowedUrl";
            DataTable dt = DatabaseAdapter.GetData(sql, DatabaseType.ApplicationData);
            if (dt != null) {
                foreach (DataRow row in dt.Rows) {
                    UrlAllowed urlAllowed = new UrlAllowed() { Url = (string)row["Url"] };

                    urls.Add(urlAllowed);
                }
            }

            return urls;
        }

        public static bool Exists(string url) {
            string sql = String.Format("select * from AllowedUrl where Url = '{0}'", url);
            DataTable dt = DatabaseAdapter.GetData(sql, DatabaseType.ApplicationData);
            return dt != null && dt.Rows.Count > 0;
        }

        public bool IsAlreadyExists() {
            string sql = String.Format("select * from AllowedUrl where Url = '{0}'", Url);

            DataTable dt = DatabaseAdapter.GetData(sql, DatabaseType.ApplicationData);

            return (dt != null && dt.Rows.Count > 0);
        }

        public static UrlGroup GetGroup(string url) {
            string sql = String.Format("select * from AllowedUrl where Url = '{0}'", url);
            DataTable dt = DatabaseAdapter.GetData(sql, DatabaseType.ApplicationData);
            if (dt != null && dt.Rows.Count > 0 && dt.Rows[0]["GroupID"] != DBNull.Value) {
                string groupId = (string)dt.Rows[0]["GroupID"];

                return UrlGroup.GetGroup(groupId);
            }

            return null;
        }
    }
}