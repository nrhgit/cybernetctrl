﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SQLite;
using System.Text;
using CybernetCtrl.Database;

namespace CybernetCtrl.Modal
{
    public class UrlAllowedServer
    {
        public int UserID { get; set; }
        public string Url { get; set; }
        public string GroupID { get; set; }

        public UrlGroupServer Group
        {
            get
            {
                return UrlGroupServer.GetGroup(GroupID);
            }
        }

        public void Insert()
        {
            string sql = "insert into AllowedUrl values(@userID, @url, @group)";

            List<SQLiteParameter> parameters = new List<SQLiteParameter>();
            parameters.Add(DatabaseAdapter.CreateParameter("@userID", DbType.Int32, UserID));
            parameters.Add(DatabaseAdapter.CreateParameter("@url", DbType.String, Url));
            parameters.Add(DatabaseAdapter.CreateParameter("@group", DbType.String, GroupID));

            DatabaseAdapter.ExecuteSql(sql, parameters, DatabaseType.ServerApplicationData);
        }

        public void Update()
        {
            string sql = "update AllowedUrl set UserID = @userID, Url = @url, GroupID = @group where Url = @url";

            List<SQLiteParameter> parameters = new List<SQLiteParameter>();
            parameters.Add(DatabaseAdapter.CreateParameter("@userID", DbType.Int32, UserID));
            parameters.Add(DatabaseAdapter.CreateParameter("@url", DbType.String, Url));
            parameters.Add(DatabaseAdapter.CreateParameter("@group", DbType.String, GroupID));

            DatabaseAdapter.ExecuteSql(sql, parameters, DatabaseType.ServerApplicationData);
        }

        public void Remove()
        {
            string sql = "delete from AllowedUrl where Url = @url";

            List<SQLiteParameter> parameters = new List<SQLiteParameter>();
            parameters.Add(DatabaseAdapter.CreateParameter("@url", DbType.String, Url));

            DatabaseAdapter.ExecuteSql(sql, parameters, DatabaseType.ServerApplicationData);
        }

        public void Remove(int userId)
        {
            string sql = "delete from AllowedUrl where Url = @url AND UserID = @userId";

            List<SQLiteParameter> parameters = new List<SQLiteParameter>();
            parameters.Add(DatabaseAdapter.CreateParameter("@url", DbType.String, Url));
            parameters.Add(DatabaseAdapter.CreateParameter("@userId", DbType.Int32, userId));

            DatabaseAdapter.ExecuteSql(sql, parameters, DatabaseType.ServerApplicationData);
        }

        public static bool IsExists(string url)
        {
            string sql = "select Url from AllowedUrl where Url = @url";

            List<SQLiteParameter> parameters = new List<SQLiteParameter>();
            parameters.Add(DatabaseAdapter.CreateParameter("@url", DbType.String, url));
            Dictionary<string, object> vals = DatabaseAdapter.GetScalarData(sql, parameters, DatabaseType.ServerApplicationData);

            return vals != null && vals.Count > 0;
        }

        public static List<UrlAllowedServer> GetAllowedUrls()
        {
            List<string> urlsAllowed = new List<string>();

            List<UrlAllowedServer> urls = new List<UrlAllowedServer>();

            string sql = "select * from AllowedUrl";
            DataTable dt = DatabaseAdapter.GetData(sql, DatabaseType.ServerApplicationData);
            if (dt != null)
            {
                foreach (DataRow row in dt.Rows)
                {
                    string url = (string)row["Url"];
                    if (urlsAllowed.Contains(url) == false)
                    {
                        UrlAllowedServer urlAllowed = new UrlAllowedServer() { UserID = (int)(long)row["UserID"], Url = (string)row["Url"] };
                        if (row["GroupID"] == DBNull.Value)
                        {
                            urlAllowed.GroupID = null;
                        }
                        else
                        {
                            urlAllowed.GroupID = (string)row["GroupID"];
                        }

                        urls.Add(urlAllowed);

                        urlsAllowed.Add(url); // used only to add unique URL
                    }
                }
            }

            return urls;
        }

        public static List<UrlAllowedServer> GetAllowedUrls(User user)
        {
            List<UrlAllowedServer> urls = new List<UrlAllowedServer>();

            string sql = String.Format("select * from AllowedUrl where UserID = {0}", user.ID);
            DataTable dt = DatabaseAdapter.GetData(sql, DatabaseType.ServerApplicationData);
            if (dt != null)
            {
                foreach (DataRow row in dt.Rows)
                {
                    UrlAllowedServer urlAllowed = new UrlAllowedServer() { UserID = (int)(long)row["UserID"], Url = (string)row["Url"] };
                    if (row["GroupID"] == DBNull.Value)
                    {
                        urlAllowed.GroupID = null;
                    }
                    else
                    {
                        urlAllowed.GroupID = (string)row["GroupID"];
                    }

                    urls.Add(urlAllowed);
                }
            }

            return urls;
        }

        public static List<User> GetAllowedUrlUsers(string url)
        {
            List<User> users = new List<User>();

            string sql = String.Format("select * from AllowedUrl where Url = '{0}' group by UserID", url);
            DataTable dt = DatabaseAdapter.GetData(sql, DatabaseType.ServerApplicationData);
            if (dt != null)
            {
                foreach (DataRow row in dt.Rows)
                {
                    int userId = (int)(long)row["UserID"];

                    users.Add(User.GetUser(userId));
                }
            }

            return users;
        }
    }
}