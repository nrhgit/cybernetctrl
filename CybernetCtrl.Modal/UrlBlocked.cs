﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SQLite;
using System.Text;
using CybernetCtrl.Database;

namespace CybernetCtrl.Modal
{
    public class UrlBlocked
    {
        public string Url { get; set; }

        public void Save()
        {
            string sql = "insert or replace into BlockedUrl values(@url)";

            List<SQLiteParameter> paramters = new List<SQLiteParameter>();
            paramters.Add(DatabaseAdapter.CreateParameter("@url", DbType.String, Url));

            DatabaseAdapter.ExecuteSql(sql, paramters, DatabaseType.ApplicationData);
        }

        public void Allow()
        {
            string sql = "delete from BlockedUrl where Url = @url";

            List<SQLiteParameter> parameters = new List<SQLiteParameter>();
            parameters.Add(DatabaseAdapter.CreateParameter("@url", DbType.String, Url));

            DatabaseAdapter.ExecuteSql(sql, parameters, DatabaseType.ApplicationData);
        }

        public static void Clear()
        {
            string sql = String.Format("delete from BlockedUrl");
            DatabaseAdapter.ExecuteSql(sql, DatabaseType.ApplicationData);
        }

        public static bool IsBlockedUrl(string url, string domain)
        {
            string urlFormatted = url;
            if (url.StartsWith("http://")) urlFormatted = url.Replace("http://", "");
            if (url.StartsWith("https://")) urlFormatted = url.Replace("http://", "");

            string sql = String.Format("select Url from BlockedUrl where Url like '%{0}%'", domain);

            DataTable vals = DatabaseAdapter.GetData(sql, DatabaseType.ApplicationData);

            if (vals != null && vals.Rows.Count > 0)
            {
                foreach (DataRow row in vals.Rows)
                {
                    string blockedUrl = (string)row["Url"];
                    if (blockedUrl.StartsWith("http://")) blockedUrl = blockedUrl.Replace("http://", "");
                    if (blockedUrl.StartsWith("https://")) blockedUrl = blockedUrl.Replace("http://", "");

                    // match specific URL
                    if (blockedUrl == urlFormatted)
                    {
                        return true;
                    }
                    else
                    {
                        // match generic URL
                        string[] parts = blockedUrl.Split("/".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);

                        string domainPart = parts[0].TrimStart("*.".ToCharArray());
                        string urlPart = blockedUrl.TrimStart("*.".ToCharArray()).Replace(domainPart, "").Trim("/".ToCharArray()).TrimEnd(".*".ToCharArray());

                        Uri uri = new Uri(url);
                        bool isDomainBlocked = uri.Host.EndsWith(domainPart);

                        if (blockedUrl.StartsWith("*") == false && uri.Host.Length > domainPart.Length)
                        {
                            isDomainBlocked = false;
                        }

                        string urlPath = urlFormatted.Replace(uri.Host, "").TrimStart("/".ToCharArray());
                        bool isUrlBlocked = urlPath.StartsWith(urlPart);

                        if (blockedUrl.EndsWith("*") == false && urlPath.Length > 0)
                        {
                            isUrlBlocked = false;
                        }

                        if (isDomainBlocked && isUrlBlocked)
                        {
                            return true;
                        }
                    }
                }
            }

            return false;


        }

        public static List<UrlBlocked> GetBlockedUrls()
        {
            List<UrlBlocked> urls = new List<UrlBlocked>();

            string sql = "select * from BlockedUrl";
            DataTable dt = DatabaseAdapter.GetData(sql, DatabaseType.ApplicationData);
            if (dt != null)
            {
                foreach (DataRow row in dt.Rows)
                {
                    UrlBlocked urlBlocked = new UrlBlocked() { Url = (string)row["Url"] };

                    urls.Add(urlBlocked);
                }
            }

            return urls;
        }

        public bool IsAlreadyExists() {
            string sql = String.Format("select * from BlockedUrl where Url = '{0}'", Url);

            DataTable dt = DatabaseAdapter.GetData(sql, DatabaseType.ApplicationData);

            return (dt != null && dt.Rows.Count > 0);
        }
    }
}
