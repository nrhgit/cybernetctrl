﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SQLite;
using System.Text;
using CybernetCtrl.Database;

namespace CybernetCtrl.Modal
{
    public class UrlBlockedDetails
    {
        public string Title { get; set; }
        public string Url { get; set; }
        public string FullUrl { get; set; }
        public DateTime VisitedDateTime { get; set; }
        public long MilliSeconds { get; set; }

        public void Save()
        {
            string sql = "insert into BlockedUrl values(@title, @url, @fulllurl, @datetime, @milli)";

            List<SQLiteParameter> parameters = new List<SQLiteParameter>();
            parameters.Add(DatabaseAdapter.CreateParameter("@title", DbType.String, Title));
            parameters.Add(DatabaseAdapter.CreateParameter("@url", DbType.String, Url));
            parameters.Add(DatabaseAdapter.CreateParameter("@fulllurl", DbType.String, FullUrl));
            parameters.Add(DatabaseAdapter.CreateParameter("@datetime", DbType.DateTime2, DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")));
            parameters.Add(DatabaseAdapter.CreateParameter("@milli", DbType.Int64, (VisitedDateTime - new DateTime(1900, 1, 1)).TotalMilliseconds));

            DatabaseAdapter.ExecuteSql(sql, parameters, DatabaseType.WebSitesBlocked);
        }

        public void Allow(string url)
        {
            UrlAllowed urlAllowed = new UrlAllowed() { Url = url };

            urlAllowed.Save();
        }

        public static List<UrlBlockedDetails> GetUrlsBlocked(int index, int pageSize)
        {
            List<UrlBlockedDetails> urlsBlocked = new List<UrlBlockedDetails>();

            string sql = String.Format("select * from BlockedUrl order by VisitedDateTime desc limit {0}, {1}", index * pageSize, pageSize);
            DataTable dt = DatabaseAdapter.GetData(sql, DatabaseType.WebSitesBlocked);
            if (dt != null)
            {
                foreach (DataRow row in dt.Rows)
                {
                    UrlBlockedDetails urlBlocked = new UrlBlockedDetails();
                    urlBlocked.Title = (string)row["Title"];
                    urlBlocked.Url = (string)row["Url"];
                    urlBlocked.FullUrl = (string)row["FullUrl"];
                    urlBlocked.VisitedDateTime = (DateTime)row["VisitedDateTime"];
                    urlBlocked.MilliSeconds = (long)row["MilliSeconds"];

                    urlsBlocked.Add(urlBlocked);
                }
            }

            return urlsBlocked;
        }

        public static int GetTotalUrls()
        {
            string sql = "select count(*) as RowsCount from BlockedUrl";

            DataTable dt = DatabaseAdapter.GetData(sql, DatabaseType.WebSitesBlocked);
            if (dt != null && dt.Rows.Count > 0)
            {
                return (int)(long)dt.Rows[0]["RowsCount"];
            }

            return 0;
        }
    }
}