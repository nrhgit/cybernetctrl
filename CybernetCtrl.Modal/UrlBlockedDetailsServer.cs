﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SQLite;
using System.Text;
using CybernetCtrl.Database;

namespace CybernetCtrl.Modal
{
    public class UrlBlockedDetailsServer
    {
        public int UserID { get; set; }
        public string Title { get; set; }
        public string Url { get; set; }
        public string FullUrl { get; set; }
        public DateTime VisitedDateTime { get; set; }
        public long MilliSeconds { get; set; }

        public void Save()
        {
            string sql = "insert into BlockedUrl values(@userID, @title, @url, @fulllurl, @datetime, @milli)";

            List<SQLiteParameter> parameters = new List<SQLiteParameter>();
            parameters.Add(DatabaseAdapter.CreateParameter("@userID", DbType.Int32, UserID));
            parameters.Add(DatabaseAdapter.CreateParameter("@title", DbType.String, Title));
            parameters.Add(DatabaseAdapter.CreateParameter("@url", DbType.String, Url));
            parameters.Add(DatabaseAdapter.CreateParameter("@fulllurl", DbType.String, FullUrl));
            parameters.Add(DatabaseAdapter.CreateParameter("@datetime", DbType.DateTime2, DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")));
            parameters.Add(DatabaseAdapter.CreateParameter("@milli", DbType.Int64, MilliSeconds));

            DatabaseAdapter.ExecuteSql(sql, parameters, DatabaseType.ServerWebSitesBlocked);
        }

        public static List<UrlBlockedDetailsServer> GetUrlsBlocked(int userId, string searchText, DateTime dateFrom, DateTime dateTo) {
            List<UrlBlockedDetailsServer> urlsBlocked = new List<UrlBlockedDetailsServer>();

            string sql = String.Format("select * from BlockedUrl where VisitedDateTime >= '{0}' and VisitedDateTime < '{1}' and UserID = {2} order by VisitedDateTime desc", dateFrom.ToString("yyyy-MM-dd"), dateTo.AddDays(1).ToString("yyyy-MM-dd"), userId);
            if (String.IsNullOrEmpty(searchText) == false) {
                sql = String.Format("select * from BlockedUrl where VisitedDateTime >= '{0}' and VisitedDateTime < '{1}' and UserID = {2} and Url like '%{3}%' order by VisitedDateTime desc", dateFrom.ToString("yyyy-MM-dd"), dateTo.AddDays(1).ToString("yyyy-MM-dd"), userId, searchText);
            }

            DataTable dt = DatabaseAdapter.GetData(sql, DatabaseType.ServerWebSitesBlocked);
            if (dt != null) {
                foreach (DataRow row in dt.Rows) {
                    UrlBlockedDetailsServer urlBlocked = new UrlBlockedDetailsServer();
                    urlBlocked.UserID = (int)(long)row["UserID"];
                    urlBlocked.Title = (string)row["Title"];
                    urlBlocked.Url = (string)row["Url"];
                    urlBlocked.FullUrl = (string)row["FullUrl"];
                    urlBlocked.VisitedDateTime = (DateTime)row["VisitedDateTime"];
                    urlBlocked.MilliSeconds = (long)row["MilliSeconds"];

                    urlsBlocked.Add(urlBlocked);
                }
            }

            return urlsBlocked;
        }

        public static List<UrlBlockedDetailsServer> GetUrlsBlocked(int userId, int index, int pageSize, string searchText, DateTime dateFrom, DateTime dateTo)
        {
            List<UrlBlockedDetailsServer> urlsBlocked = new List<UrlBlockedDetailsServer>();

            string sql = String.Format("select * from BlockedUrl where VisitedDateTime >= '{0}' and VisitedDateTime < '{1}' and UserID = {2} order by VisitedDateTime desc limit {3}, {4}", dateFrom.ToString("yyyy-MM-dd"), dateTo.AddDays(1).ToString("yyyy-MM-dd"), userId, index * pageSize, pageSize);
            if (String.IsNullOrEmpty(searchText) == false) {
                sql = String.Format("select * from BlockedUrl where VisitedDateTime >= '{0}' and VisitedDateTime < '{1}' and UserID = {2} and Url like '%{3}%' order by VisitedDateTime desc limit {4}, {5}", dateFrom.ToString("yyyy-MM-dd"), dateTo.AddDays(1).ToString("yyyy-MM-dd"), userId, searchText, index * pageSize, pageSize);
            }

            DataTable dt = DatabaseAdapter.GetData(sql, DatabaseType.ServerWebSitesBlocked);
            if (dt != null)
            {
                foreach (DataRow row in dt.Rows)
                {
                    UrlBlockedDetailsServer urlBlocked = new UrlBlockedDetailsServer();
                    urlBlocked.UserID = (int)(long)row["UserID"];
                    urlBlocked.Title = (string)row["Title"];
                    urlBlocked.Url = (string)row["Url"];
                    urlBlocked.FullUrl = (string)row["FullUrl"];
                    urlBlocked.VisitedDateTime = (DateTime)row["VisitedDateTime"];
                    urlBlocked.MilliSeconds = (long)row["MilliSeconds"];

                    urlsBlocked.Add(urlBlocked);
                }
            }

            return urlsBlocked;
        }

        public static int GetTotalUrls(int userID, params DateTime[] dateRange)
        {
            string sql = String.Format("select count(*) as RowsCount from BlockedUrl where VisitedDateTime >= '{0}' and VisitedDateTime < '{1}' and UserId = {2} order by VisitedDateTime", dateRange[0].ToString("yyyy-MM-dd"), dateRange[1].AddDays(1).ToString("yyyy-MM-dd"), userID);

            DataTable dt = DatabaseAdapter.GetData(sql, DatabaseType.ServerWebSitesBlocked);
            if (dt != null && dt.Rows.Count > 0)
            {
                return (int)(long)dt.Rows[0]["RowsCount"];
            }

            return 0;
        }

        public static long GetLastVisitedMilliSeconds(int userID)
        {
            long lastMilliSeconds = 0;

            string sql = String.Format("select * from BlockedUrl where UserID = {0} order by MilliSeconds", userID);
            DataTable dt = DatabaseAdapter.GetData(sql, DatabaseType.ServerWebSitesBlocked);
            if (dt != null)
            {
                foreach (DataRow row in dt.Rows)
                {
                    long visitedMilliSeconds = (long)row["MilliSeconds"];
                    if (visitedMilliSeconds > lastMilliSeconds)
                    {
                        lastMilliSeconds = visitedMilliSeconds;
                    }
                }
            }

            return lastMilliSeconds;
        }
    }
}