﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SQLite;
using System.Text;
using CybernetCtrl.Database;

namespace CybernetCtrl.Modal
{
    public class UrlBlockedServer
    {
        public int UserID { get; set; }
        
        public string Url { get; set; }

        public void Insert()
        {
            string sql = "insert into BlockedUrl values(@userID, @url)";

            List<SQLiteParameter> parameters = new List<SQLiteParameter>();
            parameters.Add(DatabaseAdapter.CreateParameter("@userID", DbType.Int32, UserID));
            parameters.Add(DatabaseAdapter.CreateParameter("@url", DbType.String, Url));

            DatabaseAdapter.ExecuteSql(sql, parameters, DatabaseType.ServerApplicationData);
        }

        public void Remove()
        {
            string sql = "delete from BlockedUrl where Url = @url";

            List<SQLiteParameter> parameters = new List<SQLiteParameter>();
            parameters.Add(DatabaseAdapter.CreateParameter("@url", DbType.String, Url));

            DatabaseAdapter.ExecuteSql(sql, parameters, DatabaseType.ServerApplicationData);
        }

        public void Remove(int userId)
        {
            string sql = "delete from BlockedUrl where Url = @url AND UserID = @userId";

            List<SQLiteParameter> parameters = new List<SQLiteParameter>();
            parameters.Add(DatabaseAdapter.CreateParameter("@url", DbType.String, Url));
            parameters.Add(DatabaseAdapter.CreateParameter("@userId", DbType.Int32, userId));

            DatabaseAdapter.ExecuteSql(sql, parameters, DatabaseType.ServerApplicationData);
        }

        public static bool IsExists(string url)
        {
            string sql = "select Url from BlockedUrl where Url = @url";

            List<SQLiteParameter> parameters = new List<SQLiteParameter>();
            parameters.Add(DatabaseAdapter.CreateParameter("@url", DbType.String, url));
            Dictionary<string, object> vals = DatabaseAdapter.GetScalarData(sql, parameters, DatabaseType.ServerApplicationData);

            return vals != null && vals.Count > 0;
        }

        public static List<UrlBlockedServer> GetBlockedUrls()
        {
            List<string> urlsBlocked = new List<string>();

            List<UrlBlockedServer> urls = new List<UrlBlockedServer>();

            string sql = "select * from BlockedUrl";
            DataTable dt = DatabaseAdapter.GetData(sql, DatabaseType.ServerApplicationData);
            if (dt != null)
            {
                foreach (DataRow row in dt.Rows)
                {
                    string url = (string)row["Url"];
                    if (urlsBlocked.Contains(url) == false)
                    {
                        UrlBlockedServer urlBlocked = new UrlBlockedServer() { UserID = (int)(long)row["UserID"], Url = (string)row["Url"] };

                        urls.Add(urlBlocked);

                        urlsBlocked.Add(url); // used only to add unique URL
                    }
                }
            }

            return urls;
        }

        public static List<UrlBlockedServer> GetBlockedUrls(User user)
        {
            List<UrlBlockedServer> urls = new List<UrlBlockedServer>();

            string sql = String.Format("select * from BlockedUrl where UserID = {0}", user.ID);
            DataTable dt = DatabaseAdapter.GetData(sql, DatabaseType.ServerApplicationData);
            if (dt != null)
            {
                foreach (DataRow row in dt.Rows)
                {
                    UrlBlockedServer urlBlocked = new UrlBlockedServer() { UserID = (int)(long)row["UserID"], Url = (string)row["Url"] };

                    urls.Add(urlBlocked);
                }
            }

            return urls;
        }

        public static List<User> GetBlockedUrlUsers(string url)
        {
            List<User> users = new List<User>();

            string sql = String.Format("select * from BlockedUrl where Url = '{0}' group by UserID", url);
            DataTable dt = DatabaseAdapter.GetData(sql, DatabaseType.ServerApplicationData);
            if (dt != null)
            {
                foreach (DataRow row in dt.Rows)
                {
                    int userId = (int)(long)row["UserID"];

                    users.Add(User.GetUser(userId));
                }
            }

            return users;
        }
    }
}
