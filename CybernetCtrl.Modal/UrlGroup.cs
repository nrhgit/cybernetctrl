﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using CybernetCtrl.Database;

namespace CybernetCtrl.Modal
{
    public class UrlGroup
    {
        public string ID { get; set; }
        public string Name { get; set; }
        public int AllowedDuration { get; set; }
        public int SessionDuration { get; set; }

        public UrlGroup()
        {
            ID = string.Empty;
            AllowedDuration = 0;
            SessionDuration = 0;
        }

        public void Save()
        {
            string sql = String.Format("insert or replace into UrlGroup values('{0}', '{1}', {2}, {3})", ID, Name, AllowedDuration, SessionDuration);

            DatabaseAdapter.ExecuteSql(sql, DatabaseType.ApplicationData);
        }

        public void Remove()
        {
            // TODO: set group of all allowed URL to null

            string sql = String.Format("delete from UrlGroup where ID = '{0}'", ID);

            DatabaseAdapter.ExecuteSql(sql, DatabaseType.ApplicationData);
        }

        public static bool Exists(string id)
        {
            string sql = String.Format("select * from UrlGroup where ID='{0}'", id);

            DataTable dt = DatabaseAdapter.GetData(sql, DatabaseType.ApplicationData);

            return dt != null && dt.Rows.Count > 0;
        }

        public static UrlGroup GetGroup(string id)
        {
            if (String.IsNullOrEmpty(id)) return null;

            string sql = String.Format("select * from UrlGroup where ID = '{0}'", id);
            DataTable dt = DatabaseAdapter.GetData(sql, DatabaseType.ApplicationData);
            if (dt != null && dt.Rows.Count > 0)
            {
                DataRow row = dt.Rows[0];

                return new UrlGroup() { ID = (string)row["ID"], Name = (string)row["Name"], AllowedDuration = (int)(long)row["AllowedDuration"], SessionDuration = (int)(long)row["SessionDuration"] };
            }

            return null;
        }

        public static List<UrlGroup> GetGroups()
        {
            List<UrlGroup> groups = new List<UrlGroup>();

            string sql = "select * from UrlGroup";
            DataTable dt = DatabaseAdapter.GetData(sql, DatabaseType.ApplicationData);
            if (dt != null)
            {
                foreach (DataRow row in dt.Rows)
                {
                    UrlGroup group = new UrlGroup() { ID = (string)row["ID"], Name = (string)row["Name"], AllowedDuration = (int)(long)row["AllowedDuration"], SessionDuration = (int)(long)row["SessionDuration"] };

                    groups.Add(group);
                }
            }

            return groups;
        }
    }
}
