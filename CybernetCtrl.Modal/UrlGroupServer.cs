﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using CybernetCtrl.Database;

namespace CybernetCtrl.Modal
{
    public class UrlGroupServer
    {
        public string ID { get; set; }
        public string Name { get; set; }
        public int AllowedDuration { get; set; }
        public int SessionDuration { get; set; }

        public UrlGroupServer()
        {
            ID = string.Empty;
            AllowedDuration = 60;
            SessionDuration = 15;
        }

        public void Save()
        {
            if (ID == string.Empty)
            {
                this.ID = Guid.NewGuid().ToString();
                string sql = String.Format("insert into UrlGroup values('{0}', '{1}', {2}, {3})", ID, Name, AllowedDuration, SessionDuration);

                DatabaseAdapter.ExecuteSql(sql, DatabaseType.ServerApplicationData);
            }
            else
            {
                string sql = String.Format("update UrlGroup set Name = '{0}', AllowedDuration = {1}, SessionDuration = {2} where ID = '{3}'", Name, AllowedDuration, SessionDuration, ID);

                DatabaseAdapter.ExecuteSql(sql, DatabaseType.ServerApplicationData);
            }
        }

        public void Remove()
        {
            // TODO: set group of all allowed URL to null

            string sql = String.Format("delete from UrlGroup where ID = '{0}'", ID);

            DatabaseAdapter.ExecuteSql(sql, DatabaseType.ServerApplicationData);
        }

        public static UrlGroupServer GetGroup(string id)
        {
            if (String.IsNullOrEmpty(id)) return null;

            string sql = String.Format("select * from UrlGroup where ID = '{0}'", id);
            DataTable dt = DatabaseAdapter.GetData(sql, DatabaseType.ServerApplicationData);
            if (dt != null && dt.Rows.Count > 0)
            {
                DataRow row = dt.Rows[0];

                return new UrlGroupServer() { ID = (string)row["ID"], Name = (string)row["Name"], AllowedDuration = (int)(long)row["AllowedDuration"], SessionDuration = (int)(long)row["SessionDuration"] };
            }

            return null;
        }

        public static List<UrlGroupServer> GetGroups()
        {
            List<UrlGroupServer> groups = new List<UrlGroupServer>();

            string sql = "select * from UrlGroup";
            DataTable dt = DatabaseAdapter.GetData(sql, DatabaseType.ServerApplicationData);
            if (dt != null)
            {
                foreach (DataRow row in dt.Rows)
                {
                    UrlGroupServer group = new UrlGroupServer() { ID = (string)row["ID"], Name = (string)row["Name"], AllowedDuration = (int)(long)row["AllowedDuration"], SessionDuration = (int)(long)row["SessionDuration"] };

                    groups.Add(group);
                }
            }

            return groups;
        }
    }
}
