﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using CybernetCtrl.Database;

namespace CybernetCtrl.Modal
{
    public class UrlRequest
    {
        public string Url { get; set; }

        public string Reason { get; set; }

        public DateTime RequestDateTime { get; set; }

        public RequestStatusType Status { get; set; }

        public void Save()
        {
            string sql = String.Format("insert into UrlRequest values('{0}', '{1}', '{2}', {3})", Url, Reason, RequestDateTime.ToString("yyyy-MM-dd HH:mm:ss"), (int)Status);

            DatabaseAdapter.ExecuteSql(sql, DatabaseType.ApplicationData);
        }

        public static List<UrlRequest> GetUrlsRequested()
        {
            List<UrlRequest> urls = new List<UrlRequest>();

            string sql = @"select * from UrlRequest";
            DataTable dt = DatabaseAdapter.GetData(sql, DatabaseType.ApplicationData);
            if (dt != null)
            {
                foreach (DataRow row in dt.Rows)
                {
                    UrlRequest urlRequest = new UrlRequest();
                    urlRequest.Url = (string)row["Url"];
                    urlRequest.Reason = (string)row["Reason"];
                    urlRequest.RequestDateTime = (DateTime)row["RequestDateTime"];
                    urlRequest.Status = (RequestStatusType)(int)(long)row["Status"];

                    urls.Add(urlRequest);
                }
            }

            return urls;
        }
    }
}
