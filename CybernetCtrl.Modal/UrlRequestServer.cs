﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using CybernetCtrl.Database;

namespace CybernetCtrl.Modal
{
    public class UrlRequestServer
    {
        public int UserID { get; set; }

        public string Url { get; set; }

        public string Reason { get; set; }

        public DateTime RequestDateTime { get; set; }

        public RequestStatusType Status { get; set; }

        public string UserName { get; set; }

        public string IP { get; set; }

        public void Save()
        {
            string sql = String.Format("insert into UrlRequest values({0}, '{1}', '{2}', '{3}', {4})", UserID, Url, Reason, RequestDateTime.ToString("yyyy-MM-dd HH:mm:ss"), (int)Status);

            DatabaseAdapter.ExecuteSql(sql, DatabaseType.ServerApplicationData);            
        }

        public void SetAllow()
        {
            string sql = String.Format("update UrlRequest set Status = {0} where UserID = {1} and Url = '{2}' and Reason = '{3}' and RequestDateTime = '{4}' and Status = 0", (int)RequestStatusType.Approved, UserID, Url, Reason, RequestDateTime.ToString("yyyy-MM-dd HH:mm:ss"));

            DatabaseAdapter.ExecuteSql(sql, DatabaseType.ServerApplicationData);

            this.Status = RequestStatusType.Approved;
        }

        public void SetDecline()
        {
            string sql = String.Format("update UrlRequest set Status = {0} where UserID = {1} and Url = '{2}' and Reason = '{3}' and RequestDateTime = '{4}' and Status = 0", (int)RequestStatusType.Rejected, UserID, Url, Reason, RequestDateTime.ToString("yyyy-MM-dd HH:mm:ss"));

            DatabaseAdapter.ExecuteSql(sql, DatabaseType.ServerApplicationData);

            this.Status = RequestStatusType.Rejected;
        }

        public static List<UrlRequestServer> GetUrlsRequested()
        {
            List<UrlRequestServer> urls = new List<UrlRequestServer>();

            string sql = @"select u.ID as UserID, u.Name as UserName, u.IP as IP, 
url.Url as Url, url.Reason as Reason, url.RequestDateTime as RequestDateTime, url.Status as Status 
from UrlRequest url inner join User u on u.ID = url.UserID";
            DataTable dt = DatabaseAdapter.GetData(sql, DatabaseType.ServerApplicationData);
            if (dt != null)
            {
                foreach (DataRow row in dt.Rows)
                {
                    UrlRequestServer urlRequest = new UrlRequestServer();
                    urlRequest.UserID = (int)(long)row["UserID"];
                    urlRequest.UserName = (string)row["UserName"];
                    urlRequest.IP = (string)row["IP"];
                    urlRequest.Url = (string)row["Url"];
                    urlRequest.Reason = (string)row["Reason"];
                    urlRequest.RequestDateTime = (DateTime)row["RequestDateTime"];
                    urlRequest.Status = (RequestStatusType)(int)(long)row["Status"];

                    urls.Add(urlRequest);
                }
            }

            return urls;
        }

        public static DateTime GetLastRequestDateTime(int userID)
        {
            DateTime lastDateTime = DateTime.MinValue;

            string sql = String.Format("select * from UrlRequest where UserID = {0} order by RequestDateTime", userID);
            DataTable dt = DatabaseAdapter.GetData(sql, DatabaseType.ServerApplicationData);
            if (dt != null)
            {
                foreach (DataRow row in dt.Rows)
                {
                    DateTime requestDateTime = (DateTime)row["RequestDateTime"];
                    if (requestDateTime > lastDateTime)
                    {
                        lastDateTime = requestDateTime;
                    }
                }
            }

            return lastDateTime;
        }

        public static List<string> GetUrlsAllowed(int userID)
        {
            List<string> urls = new List<string>();

            string sql = String.Format("select * from UrlRequest where UserID = {0} and Status = {1}", userID, (int)RequestStatusType.Approved);
            DataTable dt = DatabaseAdapter.GetData(sql, DatabaseType.ServerApplicationData);
            if (dt != null)
            {
                foreach (DataRow row in dt.Rows)
                {
                    urls.Add((string)row["Url"]);
                }
            }

            return urls;
        }

        public bool IsAlreadyExists()
        {
            string sql = String.Format("select * from UrlRequest where UserID = {0} and Url = '{1}' and RequestDateTime = '{2}'", this.UserID, this.Url, this.RequestDateTime.ToString("yyyy-MM-dd HH:mm:ss"));
            
            DataTable dt = DatabaseAdapter.GetData(sql, DatabaseType.ServerApplicationData);
            
            return (dt != null && dt.Rows.Count > 0);
        }
    }
}
