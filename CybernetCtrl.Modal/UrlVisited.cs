﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SQLite;
using System.Text;
using CybernetCtrl.Database;

namespace CybernetCtrl.Modal
{
    public class UrlVisited
    {
        public string Title { get; set; }
        public string Url { get; set; }
        public string FullUrl { get; set; }
        public DateTime VisitedDateTime { get; set; }
        public long MilliSeconds { get; set; }

        public void Save()
        {
            string sql = "insert into VisitedUrl values(@title, @url, @fullurl, @datetime, @milli)";

            List<SQLiteParameter> parameters = new List<SQLiteParameter>();
            parameters.Add(DatabaseAdapter.CreateParameter("@title", DbType.String, Title));
            parameters.Add(DatabaseAdapter.CreateParameter("@url", DbType.String, Url));
            parameters.Add(DatabaseAdapter.CreateParameter("@fullurl", DbType.String, FullUrl));
            parameters.Add(DatabaseAdapter.CreateParameter("@datetime", DbType.DateTime2, VisitedDateTime.ToString("yyyy-MM-dd HH:mm:ss")));
            parameters.Add(DatabaseAdapter.CreateParameter("@milli", DbType.Int64, (VisitedDateTime - new DateTime(1900, 1, 1)).TotalMilliseconds));

            DatabaseAdapter.ExecuteSql(sql, parameters, DatabaseType.WebSitesVisited);
        }

        public static List<UrlVisited> GetUrlsVisited(int index, int pageSize)
        {
            List<UrlVisited> urlsVisisted = new List<UrlVisited>();

            string sql = String.Format("select * from VisitedUrl order by VisitedDateTime desc limit {0}, {1}", index * pageSize, pageSize);
            DataTable dt = DatabaseAdapter.GetData(sql, DatabaseType.WebSitesVisited);
            if (dt != null)
            {
                foreach (DataRow row in dt.Rows)
                {
                    UrlVisited urlVisited = new UrlVisited();
                    urlVisited.Title = (string)row["Title"];
                    urlVisited.Url = (string)row["Url"];
                    urlVisited.FullUrl = (string)row["FullUrl"];
                    urlVisited.VisitedDateTime = (DateTime)row["VisitedDateTime"];
                    urlVisited.MilliSeconds = (long)row["MilliSeconds"];

                    urlsVisisted.Add(urlVisited);
                }
            }

            return urlsVisisted;
        }

        public static int GetTotalUrls()
        {
            string sql = "select count(*) as RowsCount from VisitedUrl";

            DataTable dt = DatabaseAdapter.GetData(sql, DatabaseType.WebSitesVisited);
            if (dt != null && dt.Rows.Count > 0)
            {
                return (int)(long)dt.Rows[0]["RowsCount"];
            }

            return 0;
        }
    }
}