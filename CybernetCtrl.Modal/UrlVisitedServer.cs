﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SQLite;
using System.Text;
using CybernetCtrl.Database;

namespace CybernetCtrl.Modal
{
    [Serializable]
    public class UrlVisitedServer
    {
        public int UserID { get; set; }
        public string Title { get; set; }
        public string Url { get; set; }
        public string FullUrl { get; set; }
        public DateTime VisitedDateTime { get; set; }
        public long MilliSeconds { get; set; }

        public void Save()
        {
            string sql = "insert into VisitedUrl values(@userID, @title, @url, @fullurl, @datetime, @milli)";

            List<SQLiteParameter> parameters = new List<SQLiteParameter>();
            parameters.Add(DatabaseAdapter.CreateParameter("@userID", DbType.Int32, UserID));
            parameters.Add(DatabaseAdapter.CreateParameter("@title", DbType.String, Title));
            parameters.Add(DatabaseAdapter.CreateParameter("@url", DbType.String, Url));
            parameters.Add(DatabaseAdapter.CreateParameter("@fullurl", DbType.String, FullUrl));
            parameters.Add(DatabaseAdapter.CreateParameter("@datetime", DbType.DateTime2, VisitedDateTime.ToString("yyyy-MM-dd HH:mm:ss")));
            parameters.Add(DatabaseAdapter.CreateParameter("@milli", DbType.Int64, MilliSeconds));

            DatabaseAdapter.ExecuteSql(sql, parameters, DatabaseType.ServerWebSitesVisited);

            int totalUrls = GetTotalUrls();
            if (totalUrls > CybernetCtrl.Common.Settings.ArchiveLimit)
            {
                DatabaseAdapter.CreateArchive(DatabaseType.ServerWebSitesVisited);
            }
        }

        public static List<UrlVisitedServer> GetUrlsVisited(int userId, string fileName, string searchText, DateTime dateFrom, DateTime dateTo) {
            List<UrlVisitedServer> urlsVisisted = new List<UrlVisitedServer>();

            string sql = String.Format("select * from VisitedUrl where VisitedDateTime >= '{0}' and VisitedDateTime < '{1}' and UserID = {2} order by VisitedDateTime desc", dateFrom.ToString("yyyy-MM-dd"), dateTo.AddDays(1).ToString("yyyy-MM-dd"), userId);
            if (String.IsNullOrEmpty(searchText) == false) {
                sql = String.Format("select * from VisitedUrl where VisitedDateTime >= '{0}' and VisitedDateTime < '{1}' and UserID = {2} and Url like '%{3}%' order by VisitedDateTime desc", dateFrom.ToString("yyyy-MM-dd"), dateTo.AddDays(1).ToString("yyyy-MM-dd"), userId, searchText);
            }

            DataTable dt = String.IsNullOrEmpty(fileName) ? DatabaseAdapter.GetData(sql, DatabaseType.ServerWebSitesVisited) : DatabaseAdapter.GetData(sql, fileName);
            if (dt != null) {
                foreach (DataRow row in dt.Rows) {
                    UrlVisitedServer urlVisited = new UrlVisitedServer();
                    urlVisited.UserID = (int)(long)row["UserID"];
                    urlVisited.Title = (string)row["Title"];
                    urlVisited.Url = (string)row["Url"];
                    urlVisited.FullUrl = (string)row["FullUrl"];
                    urlVisited.VisitedDateTime = (DateTime)row["VisitedDateTime"];
                    urlVisited.MilliSeconds = (long)row["MilliSeconds"];

                    urlsVisisted.Add(urlVisited);
                }
            }

            return urlsVisisted;
        }

        public static List<UrlVisitedServer> GetUrlsVisited(int userId, int index, int pageSize, string fileName, string searchText, DateTime dateFrom, DateTime dateTo)
        {
            List<UrlVisitedServer> urlsVisisted = new List<UrlVisitedServer>();

            string sql = String.Format("select * from VisitedUrl where VisitedDateTime >= '{0}' and VisitedDateTime < '{1}' and UserID = {2} order by VisitedDateTime desc limit {3}, {4}", dateFrom.ToString("yyyy-MM-dd"), dateTo.AddDays(1).ToString("yyyy-MM-dd"), userId, index * pageSize, pageSize);
            if (String.IsNullOrEmpty(searchText) == false) {
                sql = String.Format("select * from VisitedUrl where VisitedDateTime >= '{0}' and VisitedDateTime < '{1}' and UserID = {2} and Url like '%{3}%' order by VisitedDateTime desc limit {4}, {5}", dateFrom.ToString("yyyy-MM-dd"), dateTo.AddDays(1).ToString("yyyy-MM-dd"), userId, searchText, index * pageSize, pageSize);
            }

            DataTable dt = String.IsNullOrEmpty(fileName) ? DatabaseAdapter.GetData(sql, DatabaseType.ServerWebSitesVisited) : DatabaseAdapter.GetData(sql, fileName);
            if (dt != null)
            {
                foreach (DataRow row in dt.Rows)
                {
                    UrlVisitedServer urlVisited = new UrlVisitedServer();
                    urlVisited.UserID = (int)(long)row["UserID"];
                    urlVisited.Title = (string)row["Title"];
                    urlVisited.Url = (string)row["Url"];
                    urlVisited.FullUrl = (string)row["FullUrl"];
                    urlVisited.VisitedDateTime = (DateTime)row["VisitedDateTime"];
                    urlVisited.MilliSeconds = (long)row["MilliSeconds"];

                    urlsVisisted.Add(urlVisited);
                }
            }

            return urlsVisisted;
        }

        public static int GetTotalUrls(int userID, string fileName, params DateTime[] dateRange)
        {
            string sql = String.Format("select count(*) as RowsCount from VisitedUrl where VisitedDateTime >= '{0}' and VisitedDateTime < '{1}' and UserId = {2} order by VisitedDateTime", dateRange[0].ToString("yyyy-MM-dd"), dateRange[1].AddDays(1).ToString("yyyy-MM-dd"), userID);

            DataTable dt = String.IsNullOrEmpty(fileName) ? DatabaseAdapter.GetData(sql, DatabaseType.ServerWebSitesVisited): DatabaseAdapter.GetData(sql, fileName);
            if (dt != null && dt.Rows.Count > 0)
            {
                return (int)(long)dt.Rows[0]["RowsCount"];
            }

            return 0;
        }

        public static int GetTotalUrls()
        {
            string sql = "select count(*) as RowsCount from VisitedUrl";

            DataTable dt = DatabaseAdapter.GetData(sql, DatabaseType.ServerWebSitesVisited);
            if (dt != null && dt.Rows.Count > 0)
            {
                return (int)(long)dt.Rows[0]["RowsCount"];
            }

            return 0;
        }

        public static long GetLastVisitedMilliSeconds(int userID)
        {
            long lastMilliSeconds = 0;

            string sql = String.Format("select * from VisitedUrl where UserID = {0} order by MilliSeconds", userID);
            DataTable dt = DatabaseAdapter.GetData(sql, DatabaseType.ServerWebSitesVisited);
            if (dt != null)
            {
                foreach (DataRow row in dt.Rows)
                {
                    long visitedMilliSeconds = (long)row["MilliSeconds"];
                    if (visitedMilliSeconds > lastMilliSeconds)
                    {
                        lastMilliSeconds = visitedMilliSeconds;
                    }
                }
            }

            return lastMilliSeconds;
        }
    }
}