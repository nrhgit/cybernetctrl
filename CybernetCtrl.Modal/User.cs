﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using CybernetCtrl.Database;

namespace CybernetCtrl.Modal
{
    public class User
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string IP { get; set; }
        public string Serial { get; set; }

        public bool IPVerified { get; set; }

        public User()
        {
            ID = -1;

            IPVerified = false;
        }

        public void Save()
        {
            if (ID == -1)
            {
                string sql = String.Format("insert into User values(NULL, '{0}', '{1}', '{2}')", Name, IP, Serial);

                DatabaseAdapter.ExecuteSql(sql, DatabaseType.ServerApplicationData);

                this.ID = DatabaseAdapter.GetLastInsertID("User", DatabaseType.ServerApplicationData);
            }
            else
            {
                string sql = String.Format("update User set Name = '{0}', IP = '{1}', Serial = '{2}' where ID = {3}", Name, IP, Serial, ID);

                DatabaseAdapter.ExecuteSql(sql, DatabaseType.ServerApplicationData);
            }
        }

        public void Remove()
        {
            string sql = "delete from User where ID = " + ID;

            DatabaseAdapter.ExecuteSql(sql, DatabaseType.ServerApplicationData);
        }

        public static User GetUser(int id)
        {
            string sql = "select * from User where ID = " + id;
            DataTable dt = DatabaseAdapter.GetData(sql, DatabaseType.ServerApplicationData);
            if (dt != null && dt.Rows.Count > 0)
            {
                DataRow row = dt.Rows[0];

                return new User() { ID = (int)(long)row["ID"], Name = (string)row["Name"], IP = (string)row["IP"], Serial = (string)row["Serial"] };
            }

            return null;
        }

        public static List<User> GetUsers()
        {
            List<User> users = new List<User>();

            string sql = "select * from User";
            DataTable dt = DatabaseAdapter.GetData(sql, DatabaseType.ServerApplicationData);
            if (dt != null)
            {
                foreach (DataRow row in dt.Rows)
                {
                    User user = new User() { ID = (int)(long)row["ID"], Name = (string)row["Name"], IP = (string)row["IP"], Serial = (string)row["Serial"] };

                    users.Add(user);
                }
            }

            return users;
        }
    }
}
