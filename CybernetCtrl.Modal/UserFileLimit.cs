﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SQLite;
using System.Text;
using CybernetCtrl.Database;

namespace CybernetCtrl.Modal {
    public class UserFileLimit {
        public static decimal Limit {
            get {
                string sql = "select * from UserFileLimit";
                DataTable dt = DatabaseAdapter.GetData(sql, DatabaseType.ApplicationData);
                if (dt != null && dt.Rows.Count > 0) {
                    DataRow row = dt.Rows[0];

                    return (decimal)row["FileLimit"];
                } else {
                    SetLimit(100);

                    return 100;
                }
            }
            set {
                string sql = "delete from UserFileLimit";

                DatabaseAdapter.ExecuteSql(sql, null, DatabaseType.ApplicationData);

                SetLimit(value);
            }
        }

        private static void SetLimit(decimal limit) {
            string sql = "insert into UserFileLimit values(@limit)";

            List<SQLiteParameter> parameters = new List<SQLiteParameter>();
            parameters.Add(DatabaseAdapter.CreateParameter("@limit", DbType.Decimal, limit));

            DatabaseAdapter.ExecuteSql(sql, parameters, DatabaseType.ApplicationData);
        }
    }
}