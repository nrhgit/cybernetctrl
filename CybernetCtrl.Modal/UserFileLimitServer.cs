﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SQLite;
using System.Text;
using CybernetCtrl.Database;

namespace CybernetCtrl.Modal {
    public class UserFileLimitServer {
        public static decimal GetUserFileLimit(int userID) {
            string sql = "select * from UserFileLimit where UserID = " + userID;
            Dictionary<string, object> vals = DatabaseAdapter.GetScalarData(sql, DatabaseType.ServerApplicationData);
            if (vals != null && vals.Count > 0) {
                return (decimal)vals["FileLimit"];
            } else {
                SaveUserFileLimit(userID, 100);

                return 100;
            }
        }

        public static void SaveUserFileLimit(int userID, decimal limit) {
            string sql = "insert or replace into UserFileLimit values(@userID, @limit)";

            List<SQLiteParameter> paramters = new List<SQLiteParameter>();
            paramters.Add(DatabaseAdapter.CreateParameter("@userID", DbType.Int32, userID));
            paramters.Add(DatabaseAdapter.CreateParameter("@limit", DbType.Decimal, limit));

            DatabaseAdapter.ExecuteSql(sql, paramters, DatabaseType.ServerApplicationData);
        }
    }
}