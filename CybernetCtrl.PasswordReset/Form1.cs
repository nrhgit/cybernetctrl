﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using CybernetCtrl.Modal;

namespace CybernetCtrl.PasswordReset {
    public partial class Form1 : Form {
        public Form1() {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e) {
            string serverIP = ServerIP.GetServerIP();
            if (String.IsNullOrEmpty(serverIP)) {
                AdminPassword.Clear();

                AdminPassword.SetNewPassword("admin");
                
                MessageBox.Show("Password is successfully reset", "Reset", MessageBoxButtons.OK, MessageBoxIcon.Information);
            } else {
                MessageBox.Show("Your client is configured to work in connected environment. Hence password can be reset only using Cybernet-ctrl server", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
