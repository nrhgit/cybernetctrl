﻿using System;
using System.Collections.Generic;
using System.Text;
using CybernetCtrl.Message;
using CybernetCtrl.Modal;
using CybernetCtrl.Common;

namespace CybernetCtrl.Server {
    internal class ClientMessageProcessor {
        public event EventHandler<ClientVersionEventArgs> ClientVersionReceived;
        public event EventHandler<PatchAppliedEventArgs> PatchAppliedReceived;

        public void ProcessMessage(MessageBase message) {
            if (message is UrlVisitedMessage) {
                UrlVisitedMessage urlVisitedMessage = (UrlVisitedMessage)message;

                UrlVisitedServer urlVisitedServer = new UrlVisitedServer();
                urlVisitedServer.Title = urlVisitedMessage.Title;
                urlVisitedServer.UserID = urlVisitedMessage.UserID;
                urlVisitedServer.Url = urlVisitedMessage.Url;
                urlVisitedServer.FullUrl = urlVisitedMessage.FullUrl;
                urlVisitedServer.VisitedDateTime = Convert.ToDateTime(urlVisitedMessage.VisitedDateTime);
                urlVisitedServer.MilliSeconds = urlVisitedMessage.MilliSeconds;

                urlVisitedServer.Save();

                Logger.Log("Received visited URL message. Url: " + urlVisitedMessage.Url + " User ID: " + urlVisitedMessage.UserID, LogType.Information, ApplicationType.Server);

            } else if (message is UrlBlockedDetailMessage) {
                UrlBlockedDetailMessage urlBlockedMessage = (UrlBlockedDetailMessage)message;

                UrlBlockedDetailsServer urlBlockedServer = new UrlBlockedDetailsServer();
                urlBlockedServer.Title = urlBlockedMessage.Title;
                urlBlockedServer.UserID = urlBlockedMessage.UserID;
                urlBlockedServer.Url = urlBlockedMessage.Url;
                urlBlockedServer.FullUrl = urlBlockedMessage.FullUrl;
                urlBlockedServer.VisitedDateTime = Convert.ToDateTime(urlBlockedMessage.VisitedDateTime);
                urlBlockedServer.MilliSeconds = urlBlockedMessage.MilliSeconds;

                urlBlockedServer.Save();

                Logger.Log("Received blocked URL message. Url: " + urlBlockedMessage.Url + " User ID: " + urlBlockedMessage.UserID, LogType.Information, ApplicationType.Server);

            } else if (message is UrlRequestMessage) {
                UrlRequestMessage urlRequestMessage = (UrlRequestMessage)message;

                UrlRequestServer urlRequestServer = new UrlRequestServer();
                urlRequestServer.UserID = urlRequestMessage.UserID;
                urlRequestServer.Url = urlRequestMessage.Url;
                urlRequestServer.Reason = urlRequestMessage.Reason;
                urlRequestServer.RequestDateTime = Convert.ToDateTime(urlRequestMessage.RequestDateTime);
                urlRequestServer.Status = RequestStatusType.Requested;

                Logger.Log("Received requested URL message. Url: " + urlRequestMessage.Url + " User ID: " + urlRequestMessage.UserID, LogType.Information, ApplicationType.Server);

                if (urlRequestServer.IsAlreadyExists() == false) {
                    urlRequestServer.Save();
                }

            } else if (message is ValidateGroupMessage || message is ValidateUrlAllowedMessage || message is ValidateUrlBlockedMessage) {
                Synchronizer.Validate(message);
            } else if (message is ClientVersionMessage) {
                ClientVersionMessage clientVersionMessage = (ClientVersionMessage)message;

                if (ClientVersionReceived != null) {
                    ClientVersionReceived(this, new ClientVersionEventArgs() { UserID = clientVersionMessage.UserID, IsCompatible = (clientVersionMessage.Version == Settings.CompatibleClientVersion) });
                }
            } else if (message is PatchAppliedMessage) {
                PatchAppliedMessage patchAppliedMessage = (PatchAppliedMessage)message;

                if (PatchAppliedReceived != null) {
                    PatchAppliedReceived(this, new PatchAppliedEventArgs() { UserID = patchAppliedMessage.UserID, Name = patchAppliedMessage.Name });
                }
            } else if (message is NodeSerialMessage) {
                NodeSerialMessage nodeSerialMessage = (NodeSerialMessage)message;
                NetworkSerialAdapter.SetNodeSerial(nodeSerialMessage.SourceIP, nodeSerialMessage.Serial);
            }
        }
    }

    public class ClientVersionEventArgs : EventArgs {
        public int UserID { get; set; }

        public bool IsCompatible { get; set; }
    }

    public class PatchAppliedEventArgs : EventArgs {
        public int UserID { get; set; }

        public string Name { get; set; }
    }
}
