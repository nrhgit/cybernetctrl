﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using CybernetCtrl.Modal;
using MetroFramework.Controls;

namespace CybernetCtrl.Server
{
    public partial class GroupsControl : MetroUserControl
    {
        public event EventHandler GroupSettingsChanged;

        public GroupsControl()
        {
            InitializeComponent();
        }

        public void LoadControl()
        {
            List<UrlGroupServer> groups = UrlGroupServer.GetGroups();
            foreach (UrlGroupServer group in groups)
            {
                AddRow(group);
            }
        }

        private void AddRow(UrlGroupServer group)
        {
            int index = dgrGroups.Rows.Add();
            dgrGroups[colName.Name, index].Value = group.Name;
            dgrGroups[colAllowedDuration.Name, index].Value = group.AllowedDuration;
            dgrGroups.Rows[index].Tag = group;
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            UrlGroupServer group = new UrlGroupServer();
            AddEditGroupForm addEditGroupForm = new AddEditGroupForm();
            addEditGroupForm.LoadForm(group);
            if (addEditGroupForm.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                AddRow(group);
            }
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            if (dgrGroups.SelectedRows.Count > 0)
            {
                UrlGroupServer group = (UrlGroupServer)dgrGroups.SelectedRows[0].Tag;

                AddEditGroupForm addEditGroupForm = new AddEditGroupForm();
                addEditGroupForm.LoadForm(group);
                if (addEditGroupForm.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    dgrGroups.SelectedRows[0].Cells[colName.Name].Value = group.Name;
                    dgrGroups.SelectedRows[0].Cells[colAllowedDuration.Name].Value = group.AllowedDuration;

                    if (GroupSettingsChanged != null)
                    {
                        GroupSettingsChanged(this, null);
                    }
                }
            }
        }

        private void btnRemove_Click(object sender, EventArgs e)
        {
            if (dgrGroups.SelectedRows.Count > 0)
            {
                if (MessageBox.Show("Do you really want to remove this group ?", "Remove", MessageBoxButtons.OKCancel, MessageBoxIcon.Question) != DialogResult.OK)
                {
                    return;
                }

                UrlGroupServer group = (UrlGroupServer)dgrGroups.SelectedRows[0].Tag;

                group.Remove();

                dgrGroups.Rows.Remove(dgrGroups.SelectedRows[0]);
            }
        }
    }
}
