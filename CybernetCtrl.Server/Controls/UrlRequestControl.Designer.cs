﻿namespace CybernetCtrl.Server
{
    partial class UrlRequestControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dgrAllowWebsiteRequests = new System.Windows.Forms.DataGridView();
            this.colUser = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colUrl = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colRequestDateTime = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colStatus = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btnAllow = new MetroFramework.Controls.MetroButton();
            this.btnDecline = new MetroFramework.Controls.MetroButton();
            this.btnRefresh = new MetroFramework.Controls.MetroButton();
            ((System.ComponentModel.ISupportInitialize)(this.dgrAllowWebsiteRequests)).BeginInit();
            this.SuspendLayout();
            // 
            // dgrAllowWebsiteRequests
            // 
            this.dgrAllowWebsiteRequests.AllowUserToAddRows = false;
            this.dgrAllowWebsiteRequests.AllowUserToDeleteRows = false;
            this.dgrAllowWebsiteRequests.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgrAllowWebsiteRequests.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgrAllowWebsiteRequests.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgrAllowWebsiteRequests.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colUser,
            this.colUrl,
            this.colRequestDateTime,
            this.colStatus});
            this.dgrAllowWebsiteRequests.Location = new System.Drawing.Point(3, 3);
            this.dgrAllowWebsiteRequests.MultiSelect = false;
            this.dgrAllowWebsiteRequests.Name = "dgrAllowWebsiteRequests";
            this.dgrAllowWebsiteRequests.ReadOnly = true;
            this.dgrAllowWebsiteRequests.RowHeadersVisible = false;
            this.dgrAllowWebsiteRequests.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgrAllowWebsiteRequests.Size = new System.Drawing.Size(634, 325);
            this.dgrAllowWebsiteRequests.TabIndex = 1;
            this.dgrAllowWebsiteRequests.RowStateChanged += new System.Windows.Forms.DataGridViewRowStateChangedEventHandler(this.dgrAllowWebsiteRequests_RowStateChanged);
            // 
            // colUser
            // 
            this.colUser.HeaderText = "User Name";
            this.colUser.Name = "colUser";
            this.colUser.ReadOnly = true;
            // 
            // colUrl
            // 
            this.colUrl.HeaderText = "Website Requested";
            this.colUrl.Name = "colUrl";
            this.colUrl.ReadOnly = true;
            // 
            // colRequestDateTime
            // 
            this.colRequestDateTime.HeaderText = "Date/Time";
            this.colRequestDateTime.Name = "colRequestDateTime";
            this.colRequestDateTime.ReadOnly = true;
            // 
            // colStatus
            // 
            this.colStatus.HeaderText = "Status";
            this.colStatus.Name = "colStatus";
            this.colStatus.ReadOnly = true;
            // 
            // btnAllow
            // 
            this.btnAllow.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAllow.Location = new System.Drawing.Point(481, 334);
            this.btnAllow.Name = "btnAllow";
            this.btnAllow.Size = new System.Drawing.Size(75, 28);
            this.btnAllow.TabIndex = 35;
            this.btnAllow.Text = "Allow";
            this.btnAllow.UseSelectable = true;
            this.btnAllow.Click += new System.EventHandler(this.btnAllow_Click);
            // 
            // btnDecline
            // 
            this.btnDecline.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnDecline.Location = new System.Drawing.Point(562, 334);
            this.btnDecline.Name = "btnDecline";
            this.btnDecline.Size = new System.Drawing.Size(75, 28);
            this.btnDecline.TabIndex = 36;
            this.btnDecline.Text = "Decline";
            this.btnDecline.UseSelectable = true;
            this.btnDecline.Click += new System.EventHandler(this.btnDecline_Click);
            // 
            // btnRefresh
            // 
            this.btnRefresh.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnRefresh.Location = new System.Drawing.Point(3, 334);
            this.btnRefresh.Name = "btnRefresh";
            this.btnRefresh.Size = new System.Drawing.Size(75, 28);
            this.btnRefresh.TabIndex = 37;
            this.btnRefresh.Text = "Refresh";
            this.btnRefresh.UseSelectable = true;
            this.btnRefresh.Click += new System.EventHandler(this.btnRefresh_Click);
            // 
            // UrlRequestControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.btnRefresh);
            this.Controls.Add(this.btnDecline);
            this.Controls.Add(this.btnAllow);
            this.Controls.Add(this.dgrAllowWebsiteRequests);
            this.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name = "UrlRequestControl";
            this.Size = new System.Drawing.Size(640, 365);
            this.Load += new System.EventHandler(this.UrlRequestControl_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgrAllowWebsiteRequests)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dgrAllowWebsiteRequests;
        private System.Windows.Forms.DataGridViewTextBoxColumn colUser;
        private System.Windows.Forms.DataGridViewTextBoxColumn colUrl;
        private System.Windows.Forms.DataGridViewTextBoxColumn colRequestDateTime;
        private System.Windows.Forms.DataGridViewTextBoxColumn colStatus;
        private MetroFramework.Controls.MetroButton btnAllow;
        private MetroFramework.Controls.MetroButton btnDecline;
        private MetroFramework.Controls.MetroButton btnRefresh;
    }
}
