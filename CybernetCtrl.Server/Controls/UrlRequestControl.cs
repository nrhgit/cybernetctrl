﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using CybernetCtrl.Modal;
using MetroFramework.Controls;
using CybernetCtrl.Common;
using CybernetCtrl.Common.Forms;

namespace CybernetCtrl.Server
{
    public partial class UrlRequestControl : MetroUserControl
    {
        public UrlRequestControl()
        {
            InitializeComponent();
        }

        public void LoadControl()
        {
            dgrAllowWebsiteRequests.Rows.Clear();

            List<UrlRequestServer> urlRequests = UrlRequestServer.GetUrlsRequested();
            foreach (UrlRequestServer urlRequest in urlRequests)
            {
                int index = dgrAllowWebsiteRequests.Rows.Add();
                dgrAllowWebsiteRequests.Rows[index].Cells[colUser.Name].Value = urlRequest.UserName;
                dgrAllowWebsiteRequests.Rows[index].Cells[colUrl.Name].Value = urlRequest.Url;
                dgrAllowWebsiteRequests.Rows[index].Cells[colRequestDateTime.Name].Value = urlRequest.RequestDateTime.ToString("dd-MMM-yyyy HH:mm:ss");
                dgrAllowWebsiteRequests.Rows[index].Cells[colStatus.Name].Value = urlRequest.Status.ToString();

                dgrAllowWebsiteRequests.Rows[index].Tag = urlRequest;
            }            
        }

        private void UrlRequestControl_Load(object sender, EventArgs e)
        {
            dgrAllowWebsiteRequests.ClearSelection();
        }

        private void btnAllow_Click(object sender, EventArgs e)
        {
            if (dgrAllowWebsiteRequests.SelectedRows.Count > 0)
            {
                UrlRequestServer urlRequest = (UrlRequestServer)dgrAllowWebsiteRequests.SelectedRows[0].Tag;

                BlockAllowUrlConfigForm urlConfigForm = new BlockAllowUrlConfigForm();
                urlConfigForm.LoadForm(urlRequest.Url);
                if (urlConfigForm.ShowDialog() == DialogResult.OK)
                {
                    urlRequest.SetAllow();

                    UrlAllowedServer urlAllowedServer = new UrlAllowedServer() { UserID = urlRequest.UserID, Url = urlConfigForm.FormattedUrl, GroupID = string.Empty };

                    urlAllowedServer.Insert();

                    dgrAllowWebsiteRequests.SelectedRows[0].Cells[colStatus.Name].Value = urlRequest.Status.ToString();

                    SetButtonState();
                }
            }
        }

        private void btnDecline_Click(object sender, EventArgs e)
        {
            if (dgrAllowWebsiteRequests.SelectedRows.Count > 0)
            {
                UrlRequestServer urlRequest = (UrlRequestServer)dgrAllowWebsiteRequests.SelectedRows[0].Tag;

                urlRequest.SetDecline();

                dgrAllowWebsiteRequests.SelectedRows[0].Cells[colStatus.Name].Value = urlRequest.Status.ToString();

                SetButtonState();
            }
        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            LoadControl();

            dgrAllowWebsiteRequests.ClearSelection();
        }

        private void dgrAllowWebsiteRequests_RowStateChanged(object sender, DataGridViewRowStateChangedEventArgs e)
        {
            if (e.StateChanged != DataGridViewElementStates.Selected) return;

            btnAllow.Enabled = btnDecline.Enabled = true;

            SetButtonState();
        }

        private void SetButtonState()
        {
            if (dgrAllowWebsiteRequests.SelectedRows.Count > 0 && dgrAllowWebsiteRequests.SelectedRows[0].Tag != null)
            {
                UrlRequestServer urlRequest = (UrlRequestServer)dgrAllowWebsiteRequests.SelectedRows[0].Tag;

                if (urlRequest.Status == RequestStatusType.Approved || urlRequest.Status == RequestStatusType.Rejected)
                {
                    btnAllow.Enabled = btnDecline.Enabled = false;
                }
            }
        }
    }
}
