﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using CybernetCtrl.Modal;
using MetroFramework.Controls;

namespace CybernetCtrl.Server
{
    public partial class UrlsAllowedControl : MetroUserControl
    {
        public UrlsAllowedControl()
        {
            InitializeComponent();
        }

        public void LoadControl()
        {
            dgrWebsitesAllowed.Rows.Clear();

            List<UrlAllowedServer> urlsAllowed = UrlAllowedServer.GetAllowedUrls();
            foreach (UrlAllowedServer urlAllowed in urlsAllowed)
            {
                AddRow(urlAllowed);
            }
        }

        private void AddRow(UrlAllowedServer urlAllowed)
        {
            int index = dgrWebsitesAllowed.Rows.Add();
            dgrWebsitesAllowed[colUrl.Index, index].Value = urlAllowed.Url;
            if (urlAllowed.Group != null)
            {
                dgrWebsitesAllowed[colGroup.Index, index].Value = urlAllowed.Group.Name;
                dgrWebsitesAllowed[colDuration.Index, index].Value = urlAllowed.Group.AllowedDuration;
            }

            dgrWebsitesAllowed.Rows[index].Tag = urlAllowed;
        }

        private void btnAssignGroup_Click(object sender, EventArgs e)
        {
            if (dgrWebsitesAllowed.SelectedRows.Count > 0)
            {
                UrlAllowedServer urlAllowed = (UrlAllowedServer)dgrWebsitesAllowed.SelectedRows[0].Tag;

                ChooseGroupForm chooseGroupForm = new ChooseGroupForm();
                chooseGroupForm.LoadForm();
                if (chooseGroupForm.ShowDialog() == DialogResult.OK)
                {
                    UrlGroupServer group = chooseGroupForm.SelectedGroup;

                    urlAllowed.GroupID = group.ID;
                    urlAllowed.Update();

                    dgrWebsitesAllowed[colGroup.Index, dgrWebsitesAllowed.SelectedRows[0].Index].Value = group.Name;
                    dgrWebsitesAllowed[colDuration.Index, dgrWebsitesAllowed.SelectedRows[0].Index].Value = group.AllowedDuration;
                }
            }
        }

        private void btnClearGroup_Click(object sender, EventArgs e)
        {
            if (dgrWebsitesAllowed.SelectedRows.Count > 0)
            {
                UrlAllowedServer urlAllowed = (UrlAllowedServer)dgrWebsitesAllowed.SelectedRows[0].Tag;

                urlAllowed.GroupID = null;

                urlAllowed.Update();

                dgrWebsitesAllowed[colGroup.Index, dgrWebsitesAllowed.SelectedRows[0].Index].Value = string.Empty;
                dgrWebsitesAllowed[colDuration.Index, dgrWebsitesAllowed.SelectedRows[0].Index].Value = string.Empty;
            }
        }

        private void btnListAllowedUsers_Click(object sender, EventArgs e)
        {
            if (dgrWebsitesAllowed.SelectedRows.Count > 0)
            {
                UrlAllowedServer urlAllowed = (UrlAllowedServer)dgrWebsitesAllowed.SelectedRows[0].Tag;

                List<User> users = UrlAllowedServer.GetAllowedUrlUsers(urlAllowed.Url);

                UsersForm usersForm = new UsersForm();
                usersForm.LoadForm(users);
                usersForm.ShowDialog();
            }
        }

        private void btnAllowUser_Click(object sender, EventArgs e)
        {
            if (dgrWebsitesAllowed.SelectedRows.Count > 0)
            {
                UrlAllowedServer urlAllowed = (UrlAllowedServer)dgrWebsitesAllowed.SelectedRows[0].Tag;

                ChooseUserForm chooseUserForm = new ChooseUserForm();
                chooseUserForm.LoadForm();
                if (chooseUserForm.ShowDialog() == DialogResult.OK)
                {
                    User user = chooseUserForm.SelectedUser;

                    UrlAllowedServer url = new UrlAllowedServer();
                    url.UserID = user.ID;
                    url.Url = urlAllowed.Url;
                    url.GroupID = urlAllowed.GroupID;

                    url.Insert();
                }
            }
        }

        private void btnBlock_Click(object sender, EventArgs e)
        {
            if (dgrWebsitesAllowed.SelectedRows.Count > 0)
            {
                if (MessageBox.Show("Blocking allowed website will remove access to all users. Do you really want to continue ?", "Remove", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    UrlAllowedServer urlAllowed = (UrlAllowedServer)dgrWebsitesAllowed.SelectedRows[0].Tag;

                    urlAllowed.Remove();

                    dgrWebsitesAllowed.Rows.Remove(dgrWebsitesAllowed.SelectedRows[0]);
                }
            }
        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            LoadControl();
        }
    }
}