﻿namespace CybernetCtrl.Server
{
    partial class UrlsBlockedControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dgrWebsitesBlocked = new System.Windows.Forms.DataGridView();
            this.colUrl = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btnRefresh = new MetroFramework.Controls.MetroButton();
            this.btnListBlockedUsers = new MetroFramework.Controls.MetroButton();
            this.btnBlockUser = new MetroFramework.Controls.MetroButton();
            this.btnAllow = new MetroFramework.Controls.MetroButton();
            ((System.ComponentModel.ISupportInitialize)(this.dgrWebsitesBlocked)).BeginInit();
            this.SuspendLayout();
            // 
            // dgrWebsitesBlocked
            // 
            this.dgrWebsitesBlocked.AllowUserToAddRows = false;
            this.dgrWebsitesBlocked.AllowUserToDeleteRows = false;
            this.dgrWebsitesBlocked.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgrWebsitesBlocked.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgrWebsitesBlocked.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgrWebsitesBlocked.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colUrl});
            this.dgrWebsitesBlocked.Location = new System.Drawing.Point(3, 3);
            this.dgrWebsitesBlocked.MultiSelect = false;
            this.dgrWebsitesBlocked.Name = "dgrWebsitesBlocked";
            this.dgrWebsitesBlocked.ReadOnly = true;
            this.dgrWebsitesBlocked.RowHeadersVisible = false;
            this.dgrWebsitesBlocked.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgrWebsitesBlocked.Size = new System.Drawing.Size(720, 364);
            this.dgrWebsitesBlocked.TabIndex = 2;
            // 
            // colUrl
            // 
            this.colUrl.HeaderText = "Website";
            this.colUrl.Name = "colUrl";
            this.colUrl.ReadOnly = true;
            // 
            // btnRefresh
            // 
            this.btnRefresh.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnRefresh.Location = new System.Drawing.Point(3, 372);
            this.btnRefresh.Name = "btnRefresh";
            this.btnRefresh.Size = new System.Drawing.Size(75, 28);
            this.btnRefresh.TabIndex = 40;
            this.btnRefresh.Text = "Refresh";
            this.btnRefresh.UseSelectable = true;
            this.btnRefresh.Click += new System.EventHandler(this.btnRefresh_Click);
            // 
            // btnListBlockedUsers
            // 
            this.btnListBlockedUsers.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnListBlockedUsers.Location = new System.Drawing.Point(425, 372);
            this.btnListBlockedUsers.Name = "btnListBlockedUsers";
            this.btnListBlockedUsers.Size = new System.Drawing.Size(115, 28);
            this.btnListBlockedUsers.TabIndex = 41;
            this.btnListBlockedUsers.Text = "List Blocked Users";
            this.btnListBlockedUsers.UseSelectable = true;
            this.btnListBlockedUsers.Click += new System.EventHandler(this.btnListBlockedUsers_Click);
            // 
            // btnBlockUser
            // 
            this.btnBlockUser.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnBlockUser.Location = new System.Drawing.Point(546, 372);
            this.btnBlockUser.Name = "btnBlockUser";
            this.btnBlockUser.Size = new System.Drawing.Size(96, 28);
            this.btnBlockUser.TabIndex = 42;
            this.btnBlockUser.Text = "Block to User";
            this.btnBlockUser.UseSelectable = true;
            this.btnBlockUser.Click += new System.EventHandler(this.btnBlockUser_Click);
            // 
            // btnAllow
            // 
            this.btnAllow.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAllow.Location = new System.Drawing.Point(648, 372);
            this.btnAllow.Name = "btnAllow";
            this.btnAllow.Size = new System.Drawing.Size(75, 28);
            this.btnAllow.TabIndex = 43;
            this.btnAllow.Text = "Allow";
            this.btnAllow.UseSelectable = true;
            this.btnAllow.Click += new System.EventHandler(this.btnAllow_Click);
            // 
            // UrlsBlockedControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.btnAllow);
            this.Controls.Add(this.btnBlockUser);
            this.Controls.Add(this.btnListBlockedUsers);
            this.Controls.Add(this.btnRefresh);
            this.Controls.Add(this.dgrWebsitesBlocked);
            this.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name = "UrlsBlockedControl";
            this.Size = new System.Drawing.Size(726, 403);
            ((System.ComponentModel.ISupportInitialize)(this.dgrWebsitesBlocked)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dgrWebsitesBlocked;
        private System.Windows.Forms.DataGridViewTextBoxColumn colUrl;
        private MetroFramework.Controls.MetroButton btnRefresh;
        private MetroFramework.Controls.MetroButton btnListBlockedUsers;
        private MetroFramework.Controls.MetroButton btnBlockUser;
        private MetroFramework.Controls.MetroButton btnAllow;

    }
}
