﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using CybernetCtrl.Modal;
using MetroFramework.Controls;

namespace CybernetCtrl.Server
{
    public partial class UrlsBlockedControl : MetroUserControl
    {
        public UrlsBlockedControl()
        {
            InitializeComponent();
        }

        public void LoadControl()
        {
            dgrWebsitesBlocked.Rows.Clear();

            List<UrlBlockedServer> urlsBlocked = UrlBlockedServer.GetBlockedUrls();
            foreach (UrlBlockedServer urlBlocked in urlsBlocked)
            {
                AddRow(urlBlocked);
            }
        }

        private void AddRow(UrlBlockedServer urlBlocked)
        {
            int index = dgrWebsitesBlocked.Rows.Add();
            dgrWebsitesBlocked[colUrl.Index, index].Value = urlBlocked.Url;

            dgrWebsitesBlocked.Rows[index].Tag = urlBlocked;
        }

        private void btnListBlockedUsers_Click(object sender, EventArgs e)
        {
            if (dgrWebsitesBlocked.SelectedRows.Count > 0)
            {
                UrlBlockedServer urlBlocked = (UrlBlockedServer)dgrWebsitesBlocked.SelectedRows[0].Tag;

                List<User> users = UrlBlockedServer.GetBlockedUrlUsers(urlBlocked.Url);

                UsersForm usersForm = new UsersForm();
                usersForm.LoadForm(users);
                usersForm.ShowDialog();
            }
        }

        private void btnBlockUser_Click(object sender, EventArgs e)
        {
            if (dgrWebsitesBlocked.SelectedRows.Count > 0)
            {
                UrlBlockedServer urlBlocked = (UrlBlockedServer)dgrWebsitesBlocked.SelectedRows[0].Tag;

                ChooseUserForm chooseUserForm = new ChooseUserForm();
                chooseUserForm.LoadForm();
                if (chooseUserForm.ShowDialog() == DialogResult.OK)
                {
                    User user = chooseUserForm.SelectedUser;

                    UrlBlockedServer url = new UrlBlockedServer();
                    url.UserID = user.ID;
                    url.Url = urlBlocked.Url;

                    url.Insert();
                }
            }
        }

        private void btnAllow_Click(object sender, EventArgs e)
        {
            if (dgrWebsitesBlocked.SelectedRows.Count > 0)
            {
                UrlBlockedServer urlBlocked = (UrlBlockedServer)dgrWebsitesBlocked.SelectedRows[0].Tag;

                urlBlocked.Remove();

                dgrWebsitesBlocked.Rows.Remove(dgrWebsitesBlocked.SelectedRows[0]);
            }
        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            LoadControl();
        }
    }
}