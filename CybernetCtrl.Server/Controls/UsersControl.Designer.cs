﻿namespace CybernetCtrl.Server
{
    partial class UsersControl
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.dgrUsers = new System.Windows.Forms.DataGridView();
            this.colStatus = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colIP = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colSerial = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colCompatibility = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colLastStatusUpdate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colLastPatch = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btnRemove = new MetroFramework.Controls.MetroButton();
            this.btnAdd = new MetroFramework.Controls.MetroButton();
            this.btnEdit = new MetroFramework.Controls.MetroButton();
            this.btnWebsitesNotAllowed = new MetroFramework.Controls.MetroButton();
            this.btnWebsitesVisited = new MetroFramework.Controls.MetroButton();
            this.btnWebsitesBlocked = new MetroFramework.Controls.MetroButton();
            this.btnWebsitesAllowed = new MetroFramework.Controls.MetroButton();
            this.metroLabel1 = new MetroFramework.Controls.MetroLabel();
            this.rbSelectiveBlock = new MetroFramework.Controls.MetroRadioButton();
            this.rbSelectiveAllow = new MetroFramework.Controls.MetroRadioButton();
            this.btnClearSelection = new MetroFramework.Controls.MetroButton();
            this.btnViewRemotely = new MetroFramework.Controls.MetroButton();
            this.btnApplyPatchSelectedUser = new MetroFramework.Controls.MetroButton();
            this.chkBlockDownloads = new MetroFramework.Controls.MetroCheckBox();
            this.btnLicense = new MetroFramework.Controls.MetroButton();
            ((System.ComponentModel.ISupportInitialize)(this.dgrUsers)).BeginInit();
            this.SuspendLayout();
            // 
            // dgrUsers
            // 
            this.dgrUsers.AllowUserToAddRows = false;
            this.dgrUsers.AllowUserToDeleteRows = false;
            this.dgrUsers.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.dgrUsers.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgrUsers.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgrUsers.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colStatus,
            this.colName,
            this.colIP,
            this.colSerial,
            this.colCompatibility,
            this.colLastStatusUpdate,
            this.colLastPatch});
            this.dgrUsers.Location = new System.Drawing.Point(3, 3);
            this.dgrUsers.MultiSelect = false;
            this.dgrUsers.Name = "dgrUsers";
            this.dgrUsers.ReadOnly = true;
            this.dgrUsers.RowHeadersVisible = false;
            this.dgrUsers.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgrUsers.Size = new System.Drawing.Size(486, 277);
            this.dgrUsers.TabIndex = 0;
            this.dgrUsers.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgrUsers_CellDoubleClick);
            this.dgrUsers.RowStateChanged += new System.Windows.Forms.DataGridViewRowStateChangedEventHandler(this.dgrUsers_RowStateChanged);
            // 
            // colStatus
            // 
            this.colStatus.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.colStatus.HeaderText = "";
            this.colStatus.Name = "colStatus";
            this.colStatus.ReadOnly = true;
            this.colStatus.Width = 25;
            // 
            // colName
            // 
            this.colName.HeaderText = "User Name";
            this.colName.Name = "colName";
            this.colName.ReadOnly = true;
            // 
            // colIP
            // 
            this.colIP.HeaderText = "IP";
            this.colIP.Name = "colIP";
            this.colIP.ReadOnly = true;
            // 
            // colSerial
            // 
            this.colSerial.HeaderText = "Serial";
            this.colSerial.Name = "colSerial";
            this.colSerial.ReadOnly = true;
            // 
            // colCompatibility
            // 
            this.colCompatibility.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.colCompatibility.DefaultCellStyle = dataGridViewCellStyle1;
            this.colCompatibility.HeaderText = "Compatible ?";
            this.colCompatibility.Name = "colCompatibility";
            this.colCompatibility.ReadOnly = true;
            this.colCompatibility.Width = 90;
            // 
            // colLastStatusUpdate
            // 
            this.colLastStatusUpdate.HeaderText = "Last status update";
            this.colLastStatusUpdate.Name = "colLastStatusUpdate";
            this.colLastStatusUpdate.ReadOnly = true;
            this.colLastStatusUpdate.Visible = false;
            // 
            // colLastPatch
            // 
            this.colLastPatch.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.colLastPatch.DefaultCellStyle = dataGridViewCellStyle2;
            this.colLastPatch.HeaderText = "Last Patch Applied";
            this.colLastPatch.Name = "colLastPatch";
            this.colLastPatch.ReadOnly = true;
            this.colLastPatch.Width = 130;
            // 
            // btnRemove
            // 
            this.btnRemove.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnRemove.Location = new System.Drawing.Point(165, 331);
            this.btnRemove.Name = "btnRemove";
            this.btnRemove.Size = new System.Drawing.Size(75, 28);
            this.btnRemove.TabIndex = 40;
            this.btnRemove.Text = "Remove";
            this.btnRemove.UseSelectable = true;
            this.btnRemove.Click += new System.EventHandler(this.btnRemove_Click);
            // 
            // btnAdd
            // 
            this.btnAdd.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnAdd.Location = new System.Drawing.Point(3, 331);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(75, 28);
            this.btnAdd.TabIndex = 41;
            this.btnAdd.Text = "Add";
            this.btnAdd.UseSelectable = true;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // btnEdit
            // 
            this.btnEdit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnEdit.Location = new System.Drawing.Point(84, 331);
            this.btnEdit.Name = "btnEdit";
            this.btnEdit.Size = new System.Drawing.Size(75, 28);
            this.btnEdit.TabIndex = 42;
            this.btnEdit.Text = "Edit";
            this.btnEdit.UseSelectable = true;
            this.btnEdit.Click += new System.EventHandler(this.btnEdit_Click);
            // 
            // btnWebsitesNotAllowed
            // 
            this.btnWebsitesNotAllowed.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnWebsitesNotAllowed.Location = new System.Drawing.Point(495, 37);
            this.btnWebsitesNotAllowed.Name = "btnWebsitesNotAllowed";
            this.btnWebsitesNotAllowed.Size = new System.Drawing.Size(160, 28);
            this.btnWebsitesNotAllowed.TabIndex = 43;
            this.btnWebsitesNotAllowed.Text = "Websites Blocked";
            this.btnWebsitesNotAllowed.UseSelectable = true;
            this.btnWebsitesNotAllowed.Click += new System.EventHandler(this.btnWebsitesNotAllowed_Click);
            // 
            // btnWebsitesVisited
            // 
            this.btnWebsitesVisited.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnWebsitesVisited.Location = new System.Drawing.Point(495, 71);
            this.btnWebsitesVisited.Name = "btnWebsitesVisited";
            this.btnWebsitesVisited.Size = new System.Drawing.Size(160, 28);
            this.btnWebsitesVisited.TabIndex = 44;
            this.btnWebsitesVisited.Text = "Websites Visited History";
            this.btnWebsitesVisited.UseSelectable = true;
            this.btnWebsitesVisited.Click += new System.EventHandler(this.btnWebsitesVisited_Click);
            // 
            // btnWebsitesBlocked
            // 
            this.btnWebsitesBlocked.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnWebsitesBlocked.Location = new System.Drawing.Point(495, 105);
            this.btnWebsitesBlocked.Name = "btnWebsitesBlocked";
            this.btnWebsitesBlocked.Size = new System.Drawing.Size(160, 28);
            this.btnWebsitesBlocked.TabIndex = 45;
            this.btnWebsitesBlocked.Text = "Websites Blocked History";
            this.btnWebsitesBlocked.UseSelectable = true;
            this.btnWebsitesBlocked.Click += new System.EventHandler(this.btnWebsitesBlocked_Click);
            // 
            // btnWebsitesAllowed
            // 
            this.btnWebsitesAllowed.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnWebsitesAllowed.Location = new System.Drawing.Point(495, 3);
            this.btnWebsitesAllowed.Name = "btnWebsitesAllowed";
            this.btnWebsitesAllowed.Size = new System.Drawing.Size(160, 28);
            this.btnWebsitesAllowed.TabIndex = 46;
            this.btnWebsitesAllowed.Text = "Websites Allowed";
            this.btnWebsitesAllowed.UseSelectable = true;
            this.btnWebsitesAllowed.Click += new System.EventHandler(this.btnWebsitesAllowed_Click);
            // 
            // metroLabel1
            // 
            this.metroLabel1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.metroLabel1.AutoSize = true;
            this.metroLabel1.Location = new System.Drawing.Point(3, 283);
            this.metroLabel1.Name = "metroLabel1";
            this.metroLabel1.Size = new System.Drawing.Size(86, 19);
            this.metroLabel1.TabIndex = 49;
            this.metroLabel1.Text = "Access Mode";
            // 
            // rbSelectiveBlock
            // 
            this.rbSelectiveBlock.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.rbSelectiveBlock.AutoSize = true;
            this.rbSelectiveBlock.Location = new System.Drawing.Point(143, 305);
            this.rbSelectiveBlock.Name = "rbSelectiveBlock";
            this.rbSelectiveBlock.Size = new System.Drawing.Size(135, 15);
            this.rbSelectiveBlock.TabIndex = 50;
            this.rbSelectiveBlock.Text = "Selective block mode";
            this.rbSelectiveBlock.UseSelectable = true;
            this.rbSelectiveBlock.CheckedChanged += new System.EventHandler(this.rbSelectiveBlock_CheckedChanged);
            // 
            // rbSelectiveAllow
            // 
            this.rbSelectiveAllow.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.rbSelectiveAllow.AutoSize = true;
            this.rbSelectiveAllow.Location = new System.Drawing.Point(3, 305);
            this.rbSelectiveAllow.Name = "rbSelectiveAllow";
            this.rbSelectiveAllow.Size = new System.Drawing.Size(134, 15);
            this.rbSelectiveAllow.TabIndex = 51;
            this.rbSelectiveAllow.Text = "Selective allow mode";
            this.rbSelectiveAllow.UseSelectable = true;
            this.rbSelectiveAllow.CheckedChanged += new System.EventHandler(this.rbSelectiveAllow_CheckedChanged);
            // 
            // btnClearSelection
            // 
            this.btnClearSelection.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnClearSelection.Location = new System.Drawing.Point(246, 331);
            this.btnClearSelection.Name = "btnClearSelection";
            this.btnClearSelection.Size = new System.Drawing.Size(106, 28);
            this.btnClearSelection.TabIndex = 52;
            this.btnClearSelection.Text = "Clear Selection";
            this.btnClearSelection.UseSelectable = true;
            this.btnClearSelection.Click += new System.EventHandler(this.btnClearSelection_Click);
            // 
            // btnViewRemotely
            // 
            this.btnViewRemotely.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnViewRemotely.Location = new System.Drawing.Point(495, 233);
            this.btnViewRemotely.Name = "btnViewRemotely";
            this.btnViewRemotely.Size = new System.Drawing.Size(160, 28);
            this.btnViewRemotely.TabIndex = 53;
            this.btnViewRemotely.Text = "View Remotely";
            this.btnViewRemotely.UseSelectable = true;
            this.btnViewRemotely.Click += new System.EventHandler(this.btnViewRemotely_Click);
            // 
            // btnApplyPatchSelectedUser
            // 
            this.btnApplyPatchSelectedUser.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnApplyPatchSelectedUser.Location = new System.Drawing.Point(495, 199);
            this.btnApplyPatchSelectedUser.Name = "btnApplyPatchSelectedUser";
            this.btnApplyPatchSelectedUser.Size = new System.Drawing.Size(160, 28);
            this.btnApplyPatchSelectedUser.TabIndex = 54;
            this.btnApplyPatchSelectedUser.Text = "Apply Patch";
            this.btnApplyPatchSelectedUser.UseSelectable = true;
            this.btnApplyPatchSelectedUser.Click += new System.EventHandler(this.btnApplyPatchSelectedUser_Click);
            // 
            // chkBlockDownloads
            // 
            this.chkBlockDownloads.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.chkBlockDownloads.AutoSize = true;
            this.chkBlockDownloads.Location = new System.Drawing.Point(251, 287);
            this.chkBlockDownloads.Name = "chkBlockDownloads";
            this.chkBlockDownloads.Size = new System.Drawing.Size(238, 15);
            this.chkBlockDownloads.TabIndex = 55;
            this.chkBlockDownloads.Text = "Block Downloads (Known file types only)";
            this.chkBlockDownloads.UseSelectable = true;
            this.chkBlockDownloads.CheckedChanged += new System.EventHandler(this.chkBlockDownloads_CheckedChanged);
            // 
            // btnLicense
            // 
            this.btnLicense.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnLicense.Location = new System.Drawing.Point(495, 165);
            this.btnLicense.Name = "btnLicense";
            this.btnLicense.Size = new System.Drawing.Size(160, 28);
            this.btnLicense.TabIndex = 56;
            this.btnLicense.Text = "Client License";
            this.btnLicense.UseSelectable = true;
            this.btnLicense.Click += new System.EventHandler(this.btnLicense_Click);
            // 
            // UsersControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.btnLicense);
            this.Controls.Add(this.chkBlockDownloads);
            this.Controls.Add(this.btnApplyPatchSelectedUser);
            this.Controls.Add(this.btnViewRemotely);
            this.Controls.Add(this.btnClearSelection);
            this.Controls.Add(this.rbSelectiveAllow);
            this.Controls.Add(this.rbSelectiveBlock);
            this.Controls.Add(this.metroLabel1);
            this.Controls.Add(this.btnWebsitesAllowed);
            this.Controls.Add(this.btnWebsitesBlocked);
            this.Controls.Add(this.btnWebsitesVisited);
            this.Controls.Add(this.btnWebsitesNotAllowed);
            this.Controls.Add(this.btnEdit);
            this.Controls.Add(this.btnAdd);
            this.Controls.Add(this.btnRemove);
            this.Controls.Add(this.dgrUsers);
            this.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name = "UsersControl";
            this.Size = new System.Drawing.Size(658, 362);
            ((System.ComponentModel.ISupportInitialize)(this.dgrUsers)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dgrUsers;
        private MetroFramework.Controls.MetroButton btnRemove;
        private MetroFramework.Controls.MetroButton btnAdd;
        private MetroFramework.Controls.MetroButton btnEdit;
        private MetroFramework.Controls.MetroButton btnWebsitesNotAllowed;
        private MetroFramework.Controls.MetroButton btnWebsitesVisited;
        private MetroFramework.Controls.MetroButton btnWebsitesBlocked;
        private MetroFramework.Controls.MetroButton btnWebsitesAllowed;
        private MetroFramework.Controls.MetroLabel metroLabel1;
        private MetroFramework.Controls.MetroRadioButton rbSelectiveBlock;
        private MetroFramework.Controls.MetroRadioButton rbSelectiveAllow;
        private MetroFramework.Controls.MetroButton btnClearSelection;
        private MetroFramework.Controls.MetroButton btnViewRemotely;
        private MetroFramework.Controls.MetroButton btnApplyPatchSelectedUser;
        private MetroFramework.Controls.MetroCheckBox chkBlockDownloads;
        private System.Windows.Forms.DataGridViewTextBoxColumn colStatus;
        private System.Windows.Forms.DataGridViewTextBoxColumn colName;
        private System.Windows.Forms.DataGridViewTextBoxColumn colIP;
        private System.Windows.Forms.DataGridViewTextBoxColumn colSerial;
        private System.Windows.Forms.DataGridViewTextBoxColumn colCompatibility;
        private System.Windows.Forms.DataGridViewTextBoxColumn colLastStatusUpdate;
        private System.Windows.Forms.DataGridViewTextBoxColumn colLastPatch;
        private MetroFramework.Controls.MetroButton btnLicense;
    }
}