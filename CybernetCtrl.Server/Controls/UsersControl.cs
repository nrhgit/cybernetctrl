﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using CybernetCtrl.Modal;
using CybernetCtrl.Common;
using MetroFramework.Controls;
using System.Threading;
using CybernetCtrl.Message;
using CybernetCtrl.TcpComm;
using System.IO;

namespace CybernetCtrl.Server {
    public partial class UsersControl : MetroUserControl {
        public event EventHandler AccessModeTypeChanged;
        public UsersControl() {
            InitializeComponent();
        }

        public User SelectedUser {
            get {
                if (dgrUsers.SelectedRows.Count > 0) {
                    return (User)dgrUsers.SelectedRows[0].Tag;
                }

                return null;
            }
        }

        public void LoadControl() {
            List<User> users = User.GetUsers();
            foreach (User user in users) {
                AddRow(user);
            }

            Thread ipUpdateThread = new Thread(new ThreadStart(IpUpdateThread));
            ipUpdateThread.IsBackground = true;
            ipUpdateThread.Start();

            Thread statusUpdateThread = new Thread(new ThreadStart(StatusUpdateThread));
            statusUpdateThread.IsBackground = true;
            statusUpdateThread.Start();

            dgrUsers.ClearSelection();
        }

        private void StatusUpdateThread() {
            while (true) {
                foreach (DataGridViewRow row in dgrUsers.Rows) {
                    string lastUpdate = row.Cells[colLastStatusUpdate.Index].Value == null ? string.Empty : (string)row.Cells[colLastStatusUpdate.Index].Value;

                    if (String.IsNullOrEmpty(lastUpdate) == false) {
                        DateTime lastUpdateDateTime = Convert.ToDateTime(lastUpdate);

                        if ((DateTime.Now - lastUpdateDateTime).TotalSeconds > 180) {
                            Logger.Log("No alive status set in last 30 seconds. Hence setting color to gray", LogType.Information, ApplicationType.Server);
                            SetStatusColor(row.Index, Color.Gray);
                        }
                    } else {
                        Logger.Log("Setting intial alive status to gray", LogType.Information, ApplicationType.Server);
                        SetStatusColor(row.Index, Color.Gray);
                    }
                }

                Thread.Sleep(5000);
            }
        }

        private void IpUpdateThread() {
            while (true) {
                foreach (DataGridViewRow row in dgrUsers.Rows) {
                    User user = (User)row.Tag;

                    foreach (NetworkNode node in NetworkSerialAdapter.Nodes) {
                        if (node.Serial == user.Serial) {
                            dgrUsers[colIP.Name, row.Index].Value = node.IP;
                            break;
                        }
                    }
                }

                Thread.Sleep(10000);
            }
        }

        public void SetIsAlive(string ip) {
            Logger.Log("Update alive status on UI for IP: " + ip, LogType.Information, ApplicationType.Server);
            foreach (DataGridViewRow row in dgrUsers.Rows) {
                if ((string)row.Cells[colIP.Index].Value == ip) {
                    Logger.Log("Setting status to green. " + ip, LogType.Information, ApplicationType.Server);
                    SetStatusColor(row.Index, Color.Green);
                    row.Cells[colLastStatusUpdate.Index].Value = DateTime.Now.ToString();
                    break;
                }
            }
        }

        public void SetCompatibility(int userID, bool isCompatible) {
            foreach (DataGridViewRow row in dgrUsers.Rows) {
                User user = (User)row.Tag;

                if (user.ID == userID) {
                    row.Cells[colCompatibility.Name].Value = (isCompatible ? "Yes" : "No");

                    if (isCompatible) {
                        row.Cells[colCompatibility.Name].Style.BackColor = Color.LightBlue;
                    } else {
                        row.Cells[colCompatibility.Name].Style.BackColor = Color.Yellow;
                    }

                    break;
                }
            }
        }

        public void SetPatchApplied(int userID, string name) {
            foreach (DataGridViewRow row in dgrUsers.Rows) {
                User user = (User)row.Tag;

                if (user.ID == userID) {
                    row.Cells[colLastPatch.Name].Value = name;

                    break;
                }
            }
        }

        private void SetStatusColor(int rowIndex, Color color) {
            if (InvokeRequired) {
                this.Invoke((MethodInvoker)delegate {
                    SetStatusColor(rowIndex, color);
                });
            } else {
                dgrUsers.Rows[rowIndex].Cells[0].Style.BackColor = color;
            }
        }

        private void AddRow(User user) {
            int index = dgrUsers.Rows.Add();
            dgrUsers[colName.Name, index].Value = user.Name;
            dgrUsers[colIP.Name, index].Value = user.IPVerified ? user.IP : "Reading....";
            dgrUsers[colSerial.Name, index].Value = user.Serial;
            dgrUsers[colCompatibility.Name, index].Value = "Unknown";
            dgrUsers[colLastPatch.Name, index].Value = "Unknown";
            dgrUsers.Rows[index].Tag = user;
        }

        private void btnAdd_Click(object sender, EventArgs e) {
            NetworkUsersForm networkUsersForm = new NetworkUsersForm();
            networkUsersForm.LoadForm();
            if (networkUsersForm.ShowDialog() == System.Windows.Forms.DialogResult.OK) {
                AddRow(networkUsersForm.SelectedUser);
            }
        }

        private void btnEdit_Click(object sender, EventArgs e) {
            Edit();
        }

        private void Edit() {
            if (dgrUsers.SelectedRows.Count > 0) {
                User user = (User)dgrUsers.SelectedRows[0].Tag;

                EditUserForm editUserForm = new EditUserForm();
                editUserForm.LoadForm(user);
                if (editUserForm.ShowDialog() == System.Windows.Forms.DialogResult.OK) {
                    dgrUsers.SelectedRows[0].Cells[colName.Name].Value = user.Name;
                    dgrUsers.SelectedRows[0].Cells[colIP.Name].Value = user.IP;
                }
            }
        }

        private void btnRemove_Click(object sender, EventArgs e) {
            if (dgrUsers.SelectedRows.Count > 0) {
                if (MessageBox.Show("Do you really want to remove this user ?", "Remove", MessageBoxButtons.OKCancel, MessageBoxIcon.Question) != DialogResult.OK) {
                    return;
                }

                User user = (User)dgrUsers.SelectedRows[0].Tag;

                user.Remove();

                dgrUsers.Rows.Remove(dgrUsers.SelectedRows[0]);
            }
        }

        private void btnWebsitesVisited_Click(object sender, EventArgs e) {
            if (dgrUsers.SelectedRows.Count > 0) {
                User user = (User)dgrUsers.SelectedRows[0].Tag;

                UrlsVisitedForm urlVisitedForm = new UrlsVisitedForm();
                urlVisitedForm.LoadForm(user);
                urlVisitedForm.ShowDialog();
            }
        }

        private void btnWebsitesBlocked_Click(object sender, EventArgs e) {
            if (dgrUsers.SelectedRows.Count > 0) {
                User user = (User)dgrUsers.SelectedRows[0].Tag;

                UrlsBlockedDetailsForm urlBlockedForm = new UrlsBlockedDetailsForm();
                urlBlockedForm.LoadForm(user);
                urlBlockedForm.ShowDialog();
            }
        }

        private void btnWebsitesAllowed_Click(object sender, EventArgs e) {
            if (dgrUsers.SelectedRows.Count > 0) {
                User user = (User)dgrUsers.SelectedRows[0].Tag;

                UrlsAllowedForm urlsAllowedForm = new UrlsAllowedForm();
                urlsAllowedForm.LoadForm(user);
                urlsAllowedForm.ShowDialog();
            }
        }

        private void btnWebsitesNotAllowed_Click(object sender, EventArgs e) {
            if (dgrUsers.SelectedRows.Count > 0) {
                User user = (User)dgrUsers.SelectedRows[0].Tag;

                UrlsBlockedForm urlsBlockedForm = new UrlsBlockedForm();
                urlsBlockedForm.LoadForm(user);
                urlsBlockedForm.ShowDialog();
            }
        }

        private void rbSelectiveAllow_CheckedChanged(object sender, EventArgs e) {
            if (dgrUsers.SelectedRows.Count > 0 && rbSelectiveAllow.Checked) {
                User user = (User)dgrUsers.SelectedRows[0].Tag;

                AccessModeServer.SaveAccessMode(user.ID, AccessModeType.SelectiveAllow);

                NotifyAccessModeChanged();
            }
        }

        private void rbSelectiveBlock_CheckedChanged(object sender, EventArgs e) {
            if (dgrUsers.SelectedRows.Count > 0 && rbSelectiveBlock.Checked) {
                User user = (User)dgrUsers.SelectedRows[0].Tag;

                AccessModeServer.SaveAccessMode(user.ID, AccessModeType.SelectiveBlock);

                NotifyAccessModeChanged();
            }
        }

        private void dgrUsers_RowStateChanged(object sender, DataGridViewRowStateChangedEventArgs e) {
            if (e.StateChanged != DataGridViewElementStates.Selected) return;

            if (dgrUsers.SelectedRows.Count > 0) {
                User user = (User)dgrUsers.SelectedRows[0].Tag;

                if (user == null) return;

                AccessModeType accessMode = AccessModeServer.GetAccessMode(user.ID);
                switch (accessMode) {
                    case AccessModeType.SelectiveAllow: rbSelectiveAllow.Checked = true; break;
                    case AccessModeType.SelectiveBlock: rbSelectiveBlock.Checked = true; break;
                }

                chkBlockDownloads.Checked = (UserFileLimitServer.GetUserFileLimit(user.ID) == 0);
            }

            btnWebsitesAllowed.Enabled = rbSelectiveAllow.Checked;
            btnWebsitesNotAllowed.Enabled = rbSelectiveBlock.Checked;

            NotifyAccessModeChanged();
        }

        private void dgrUsers_CellDoubleClick(object sender, DataGridViewCellEventArgs e) {
            Edit();
        }

        private void NotifyAccessModeChanged() {
            if (AccessModeTypeChanged != null) {
                AccessModeTypeChanged(this, null);
            }
        }

        private void btnClearSelection_Click(object sender, EventArgs e) {
            dgrUsers.ClearSelection();

            rbSelectiveAllow.Checked = rbSelectiveBlock.Checked = false;
            chkBlockDownloads.Checked = false;
        }

        private void btnViewRemotely_Click(object sender, EventArgs e) {
            if (dgrUsers.SelectedRows.Count > 0) {
                User user = (User)dgrUsers.SelectedRows[0].Tag;

                if (user == null) return;

                RemoteViewerForm remoteViewerForm = new RemoteViewerForm();
                remoteViewerForm.LoadForm(user.IP);
                remoteViewerForm.Show();
            }
        }

        private void btnApplyPatchSelectedUser_Click(object sender, EventArgs e) {
            if (dgrUsers.SelectedRows.Count > 0) {
                User user = (User)dgrUsers.SelectedRows[0].Tag;

                if (user == null) return;

                OpenFileDialog dlg = new OpenFileDialog();
                dlg.Multiselect = false;
                dlg.Filter = "Path files|*.ctrlpatch";

                if (dlg.ShowDialog() == DialogResult.OK) {
                    string name = Path.GetFileName(dlg.FileName);
                    string content = StringBinaryConverter.BinaryFileToString(dlg.FileName);
                    PatchFileMessage message = new PatchFileMessage() { IP = user.IP, Name = name, Content = content };

                    TcpCommunicator.Instance.EnqueMessage(message);
                }
            }
        }

        private void chkBlockDownloads_CheckedChanged(object sender, EventArgs e) {
            if (dgrUsers.SelectedRows.Count > 0) {
                User user = (User)dgrUsers.SelectedRows[0].Tag;

                if (user == null) return;

                if (chkBlockDownloads.Checked) {
                    UserFileLimitServer.SaveUserFileLimit(user.ID, 0);
                } else {
                    UserFileLimitServer.SaveUserFileLimit(user.ID, 100);
                }
            }
        }

        private void btnLicense_Click(object sender, EventArgs e) {
            if (dgrUsers.SelectedRows.Count > 0) {
                User user = (User)dgrUsers.SelectedRows[0].Tag;

                ClientLicenseForm licenseForm = new ClientLicenseForm();
                licenseForm.LoadForm(user.Serial);
                licenseForm.ShowDialog();
            }
        }
    }
}
