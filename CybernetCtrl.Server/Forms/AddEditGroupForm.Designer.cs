﻿namespace CybernetCtrl.Server
{
    partial class AddEditGroupForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.metroLabel1 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel2 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel3 = new MetroFramework.Controls.MetroLabel();
            this.txtGroupName = new MetroFramework.Controls.MetroTextBox();
            this.txtTotalDuration = new MetroFramework.Controls.MetroTextBox();
            this.txtSessionDuration = new MetroFramework.Controls.MetroTextBox();
            this.btnSave = new MetroFramework.Controls.MetroButton();
            this.btnClose = new MetroFramework.Controls.MetroButton();
            this.SuspendLayout();
            // 
            // metroLabel1
            // 
            this.metroLabel1.AutoSize = true;
            this.metroLabel1.Location = new System.Drawing.Point(11, 82);
            this.metroLabel1.Name = "metroLabel1";
            this.metroLabel1.Size = new System.Drawing.Size(45, 19);
            this.metroLabel1.TabIndex = 19;
            this.metroLabel1.Text = "Name";
            // 
            // metroLabel2
            // 
            this.metroLabel2.AutoSize = true;
            this.metroLabel2.Location = new System.Drawing.Point(11, 140);
            this.metroLabel2.Name = "metroLabel2";
            this.metroLabel2.Size = new System.Drawing.Size(144, 19);
            this.metroLabel2.TabIndex = 20;
            this.metroLabel2.Text = "Session Duration (mins)";
            // 
            // metroLabel3
            // 
            this.metroLabel3.AutoSize = true;
            this.metroLabel3.Location = new System.Drawing.Point(11, 111);
            this.metroLabel3.Name = "metroLabel3";
            this.metroLabel3.Size = new System.Drawing.Size(131, 19);
            this.metroLabel3.TabIndex = 21;
            this.metroLabel3.Text = "Total Duration (mins)";
            // 
            // txtGroupName
            // 
            this.txtGroupName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtGroupName.Lines = new string[0];
            this.txtGroupName.Location = new System.Drawing.Point(161, 82);
            this.txtGroupName.MaxLength = 32767;
            this.txtGroupName.Name = "txtGroupName";
            this.txtGroupName.PasswordChar = '\0';
            this.txtGroupName.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtGroupName.SelectedText = "";
            this.txtGroupName.Size = new System.Drawing.Size(231, 23);
            this.txtGroupName.TabIndex = 22;
            this.txtGroupName.UseSelectable = true;
            // 
            // txtTotalDuration
            // 
            this.txtTotalDuration.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtTotalDuration.Lines = new string[0];
            this.txtTotalDuration.Location = new System.Drawing.Point(161, 111);
            this.txtTotalDuration.MaxLength = 32767;
            this.txtTotalDuration.Name = "txtTotalDuration";
            this.txtTotalDuration.PasswordChar = '\0';
            this.txtTotalDuration.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtTotalDuration.SelectedText = "";
            this.txtTotalDuration.Size = new System.Drawing.Size(231, 23);
            this.txtTotalDuration.TabIndex = 23;
            this.txtTotalDuration.UseSelectable = true;
            // 
            // txtSessionDuration
            // 
            this.txtSessionDuration.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtSessionDuration.Lines = new string[0];
            this.txtSessionDuration.Location = new System.Drawing.Point(161, 140);
            this.txtSessionDuration.MaxLength = 32767;
            this.txtSessionDuration.Name = "txtSessionDuration";
            this.txtSessionDuration.PasswordChar = '\0';
            this.txtSessionDuration.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtSessionDuration.SelectedText = "";
            this.txtSessionDuration.Size = new System.Drawing.Size(231, 23);
            this.txtSessionDuration.TabIndex = 24;
            this.txtSessionDuration.UseSelectable = true;
            // 
            // btnSave
            // 
            this.btnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSave.Location = new System.Drawing.Point(236, 169);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 28);
            this.btnSave.TabIndex = 0;
            this.btnSave.Text = "Save";
            this.btnSave.UseSelectable = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.Location = new System.Drawing.Point(317, 169);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 28);
            this.btnClose.TabIndex = 25;
            this.btnClose.Text = "Close";
            this.btnClose.UseSelectable = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // AddEditGroupForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(415, 218);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.txtSessionDuration);
            this.Controls.Add(this.txtTotalDuration);
            this.Controls.Add(this.txtGroupName);
            this.Controls.Add(this.metroLabel3);
            this.Controls.Add(this.metroLabel2);
            this.Controls.Add(this.metroLabel1);
            this.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name = "AddEditGroupForm";
            this.Text = "Add/Edit Group";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private MetroFramework.Controls.MetroLabel metroLabel1;
        private MetroFramework.Controls.MetroLabel metroLabel2;
        private MetroFramework.Controls.MetroLabel metroLabel3;
        private MetroFramework.Controls.MetroTextBox txtGroupName;
        private MetroFramework.Controls.MetroTextBox txtTotalDuration;
        private MetroFramework.Controls.MetroTextBox txtSessionDuration;
        private MetroFramework.Controls.MetroButton btnSave;
        private MetroFramework.Controls.MetroButton btnClose;
    }
}