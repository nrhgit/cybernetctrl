﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using CybernetCtrl.Modal;
using MetroFramework.Forms;

namespace CybernetCtrl.Server
{
    public partial class AddEditGroupForm : MetroForm
    {
        private UrlGroupServer group;

        public AddEditGroupForm()
        {
            InitializeComponent();
        }

        public void LoadForm(UrlGroupServer group)
        {
            this.group = group;

            txtGroupName.Text = group.Name;
            txtTotalDuration.Text = group.AllowedDuration.ToString();
            txtSessionDuration.Text = group.SessionDuration.ToString();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (IsValidInput())
            {
                group.Name = txtGroupName.Text;
                group.AllowedDuration = Int32.Parse(txtTotalDuration.Text);
                group.SessionDuration = Int32.Parse(txtSessionDuration.Text);

                group.Save();

                this.DialogResult = System.Windows.Forms.DialogResult.OK;
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
        }

        private bool IsValidInput()
        {
            if (String.IsNullOrEmpty(txtGroupName.Text))
            {
                MessageBox.Show("User name cannot be empty.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }

            int duration = 0;
            if (Int32.TryParse(txtTotalDuration.Text, out duration) == false)
            {
                MessageBox.Show("Total duration is not in valid format.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }

            int sessionDuration = 0;
            if (Int32.TryParse(txtSessionDuration.Text, out sessionDuration) == false)
            {
                MessageBox.Show("Session duration is not in valid format.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }

            if (sessionDuration > duration)
            {
                MessageBox.Show("Session duration cannot be greater than total duration.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }

            if (duration % sessionDuration != 0)
            {
                MessageBox.Show("Total duration is required to be multiples of session duration and natural number." + Environment.NewLine + Environment.NewLine + "For example total duration can be 15, 30, 45, 60 etc when session duration is 15, but 10, 20, 30, 40, 50 etc", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }

            return true;
        }
    }
}
