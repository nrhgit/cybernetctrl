﻿using MetroFramework.Forms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Text;
using System.Windows.Forms;

namespace CybernetCtrl.Server
{
    public partial class ArchivesForm : MetroForm
    {
        private string localDirName = CybernetCtrl.Common.Settings.LocalDir;

        public ArchivesForm()
        {
            InitializeComponent();
        }

        public string FileName
        {
            get
            {
                if (dgrArchives.Rows.Count > 0 && dgrArchives.SelectedRows.Count > 0)
                {
                    return (string)dgrArchives.SelectedRows[0].Cells[0].Value;
                }

                return string.Empty;
            }
        }

        public void LoadForm()
        {
            string localDir = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) + @"\" + localDirName;

            string[] files = Directory.GetFiles(localDir);
            foreach (string file in files)
            {
                if (File.Exists(file) && Path.GetFileName(file).StartsWith("archive"))
                {
                    int index = dgrArchives.Rows.Add();
                    dgrArchives.Rows[index].Cells[0].Value = Path.GetFileNameWithoutExtension(file);
                }
            }
        }

        private void btnLoad_Click(object sender, EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.OK;
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
        }
    }
}
