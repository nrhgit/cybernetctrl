﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using CybernetCtrl.Modal;
using MetroFramework.Forms;

namespace CybernetCtrl.Server
{
    public partial class ChooseGroupForm : MetroForm
    {
        public ChooseGroupForm()
        {
            InitializeComponent();
        }

        public UrlGroupServer SelectedGroup
        {
            get
            {
                if (dgrGroups.SelectedRows.Count > 0)
                {
                    return (UrlGroupServer)dgrGroups.SelectedRows[0].Tag;
                }
                return null;
            }
        }

        public void LoadForm()
        {
            List<UrlGroupServer> groups = UrlGroupServer.GetGroups();
            foreach (UrlGroupServer group in groups)
            {
                AddRow(group);
            }
        }

        private void AddRow(UrlGroupServer group)
        {
            int index = dgrGroups.Rows.Add();
            dgrGroups[colName.Name, index].Value = group.Name;
            dgrGroups[colAllowedDuration.Name, index].Value = group.AllowedDuration;
            dgrGroups.Rows[index].Tag = group;
        }

        private void btnSelect_Click(object sender, EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.OK;
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
        }
    }
}
