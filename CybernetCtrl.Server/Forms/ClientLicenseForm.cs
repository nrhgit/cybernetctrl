﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using MetroFramework.Forms;
using CybernetCtrl.Modal;

namespace CybernetCtrl.Server {
    public partial class ClientLicenseForm : MetroForm {

        private ClientLicense license;

        public ClientLicenseForm() {
            InitializeComponent();
        }

        public void LoadForm(string serial) {
            lblSerial.Text = serial;

            this.license = ClientLicense.GetLicense(serial);
            if (this.license != null) {
                txtLicense.Text = this.license.License;
            } else {
                this.license = new ClientLicense();
            }
        }

        private void btnSave_Click(object sender, EventArgs e) {
            this.license.Serial = lblSerial.Text;
            this.license.License = txtLicense.Text;
            this.license.Save();

            this.DialogResult = DialogResult.OK;
        }

        private void btnClose_Click(object sender, EventArgs e) {
            this.DialogResult = DialogResult.Cancel;
        }
    }
}
