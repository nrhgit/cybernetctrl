﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Net;
using System.Text;
using System.Windows.Forms;
using CybernetCtrl.Modal;
using MetroFramework.Forms;
using System.Net.NetworkInformation;

namespace CybernetCtrl.Server
{
    public partial class EditUserForm : MetroForm
    {
        private User user;

        public EditUserForm()
        {
            InitializeComponent();
        }

        public void LoadForm(User user)
        {
            this.user = user;

            txtName.Text = user.Name;
            txtIP.Text = user.IP;
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (IsValidInput())
            {
                user.Name = txtName.Text;
                user.IP = txtIP.Text;

                user.Save();

                this.DialogResult = System.Windows.Forms.DialogResult.OK;
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
        }

        private bool IsValidInput()
        {
            if (String.IsNullOrEmpty(txtName.Text))
            {
                MessageBox.Show("User name cannot be empty.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }

            IPAddress ip;
            bool b = IPAddress.TryParse(txtIP.Text, out ip);
            if (b == false)
            {
                MessageBox.Show("IP Address entered is not in valid format.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }

            return true;
        }

        private void btnPing_Click(object sender, EventArgs e)
        {
            Ping pinger = new Ping();

            PingReply reply = pinger.Send(txtIP.Text);

            if (reply.Status == IPStatus.Success)
            {
                MessageBox.Show("Ping is successful!", "Ping", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                MessageBox.Show("Ping failed. Please check IP address and network connectivity.", "Ping", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
