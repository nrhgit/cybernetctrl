﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using MetroFramework.Forms;
using CybernetCtrl.Common;
using CybernetCtrl.Modal;

namespace CybernetCtrl.Server {
    public partial class NetworkUsersForm : MetroForm {

        private User user = new User();

        public NetworkUsersForm() {
            InitializeComponent();
        }

        public User SelectedUser { get { return user; } }

        public void LoadForm() {
            dgrNetworkUsers.Rows.Clear();
            foreach (NetworkNode node in NetworkSerialAdapter.Nodes) {
                AddNetworkNode(node);
            }
        }

        private void AddNetworkNode(NetworkNode node) {
            int index = dgrNetworkUsers.Rows.Add();
            dgrNetworkUsers[colIP.Name, index].Value = node.IP;
            dgrNetworkUsers[colSerial.Name, index].Value = node.Serial;
            dgrNetworkUsers[colName.Name, index].Value = node.ComputerName;

            dgrNetworkUsers.Rows[index].Tag = node;
        }

        private void btnAddUser_Click(object sender, EventArgs e) {
            if (dgrNetworkUsers.SelectedRows.Count > 0) {
                NetworkNode node = (NetworkNode)dgrNetworkUsers.SelectedRows[0].Tag;

                if (String.IsNullOrEmpty(node.Serial)) {
                    MessageBox.Show("This node cannot be used as Serial is empty. Please make sure client is running in the respective remote machine or wait for sometime untill this information is read from client", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }

                user.Name = node.ComputerName;
                user.IP = node.IP;
                user.Serial = node.Serial;

                user.Save();

                user.IPVerified = true;

                this.DialogResult = DialogResult.OK;
            }
        }

        private void btnClose_Click(object sender, EventArgs e) {
            this.DialogResult = DialogResult.Cancel;
        }

        private void btnRefresh_Click(object sender, EventArgs e) {
            LoadForm();
        }
    }
}
