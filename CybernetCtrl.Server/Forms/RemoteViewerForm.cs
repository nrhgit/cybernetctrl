﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Diagnostics;
using System.IO;
using System.Threading;
using System.Net;
using System.Net.Sockets;
using MetroFramework.Forms;
using CybernetCtrl.TcpComm;
using CybernetCtrl.Message;
using CybernetCtrl.Common;

namespace CybernetCtrl.Server {
    public partial class RemoteViewerForm : MetroForm {

        private bool stop = false;
        private bool recording = false;
        private int imageIndex = 0;
        private string localDir = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) + @"\CybernetCtrl\";
        private string captureDir = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) + @"\CybernetCtrl\Capture";
        private string captureFile = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) + @"\CybernetCtrl\Capture\Capture.wmv";

        private static int port = 7856;

        private object o = new object();

        public RemoteViewerForm() {
            InitializeComponent();

            CheckForIllegalCrossThreadCalls = false; // TODO: remove this
        }

        public string IP { get; set; }

        public void LoadForm(string ip) {
            this.IP = ip;

            StartCaptureCommand cmd = new StartCaptureCommand() { IP = this.IP, Port = port };
            TcpCommunicator.Instance.EnqueMessage(cmd);

            Thread thread = new Thread(new ParameterizedThreadStart(ListernerThread));
            thread.IsBackground = true;
            thread.Start(port);

            port++;
        }

        private void RemoteViewerForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            stop = true;
        }

        private void ListernerThread(object o) {
            int port = (int)o;

            IPEndPoint ipep = new IPEndPoint(IPAddress.Parse(IPReader.LocalIP), port);

            Socket socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);

            socket.Bind(ipep);
            socket.Listen(10);

            Socket client = socket.Accept();

            while (true) {
                if (stop)
                {
                    socket.Close();
                    client.Close();

                    break;
                }

                byte[] data = new byte[500 * 2014];
                int recv = client.Receive(data);

                if (recv == 0) continue;

                using (MemoryStream fileStream = new MemoryStream(data)) {
                    try {
                        lock (o) {
                            Image img = Image.FromStream(fileStream);
                            if (recording) {
                                SaveImage(img);
                            }

                            pictureBox1.Image = img;
                        }
                    } catch { }
                }
            }
        }

        private void SaveImage(Image img) {
            string filename = captureDir + @"\img" + imageIndex + ".jpg";

            img.Save(filename);

            imageIndex++;
        }

        private void btnRecord_Click(object sender, EventArgs e) {
            if (recording) {
                btnRecord.Text = "Start Recording";
                recording = !recording;

                CreateVideoFile();

                SaveFileDialog dlg = new SaveFileDialog();
                dlg.DefaultExt = "wmv";
                dlg.Filter = "Windows media audio/video file|*.wmv";
                dlg.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);

                Thread.Sleep(1000);

                if (File.Exists(captureFile) && dlg.ShowDialog() == System.Windows.Forms.DialogResult.OK) {
                    File.Copy(captureFile, dlg.FileName, true);
                }

                return;
            }

            btnRecord.Text = "End Recording";
            bool recordingTemp = !recording;

            if (recordingTemp) {
                if (Directory.Exists(captureDir)) DeleteDirectory(captureDir);

                Thread.Sleep(3000);

                Directory.CreateDirectory(captureDir);

                imageIndex = 1;
            }

            recording = !recording;
        }

        public static void DeleteDirectory(string targetDir) {
            string[] files = Directory.GetFiles(targetDir);
            string[] dirs = Directory.GetDirectories(targetDir);

            foreach (string file in files) {
                File.SetAttributes(file, FileAttributes.Normal);
                File.Delete(file);
            }

            foreach (string dir in dirs) {
                DeleteDirectory(dir);
            }

            Directory.Delete(targetDir, true);
        }

        private void CreateVideoFile() {
            string ffmpegFile = localDir + "ffmpeg.exe";
            if (File.Exists(ffmpegFile) == false) {
                MessageBox.Show("Utility to create video not found!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            ProcessStartInfo ProcessInfo = new ProcessStartInfo(Application.StartupPath + @"\\videomaker.bat");
            ProcessInfo.CreateNoWindow = true;
            ProcessInfo.UseShellExecute = false;
            // *** Redirect the output ***
            ProcessInfo.RedirectStandardError = true;
            ProcessInfo.RedirectStandardOutput = true;

            Process.Start(ProcessInfo);
        }
    }
}
