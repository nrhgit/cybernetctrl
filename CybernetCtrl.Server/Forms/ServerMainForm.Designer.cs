﻿namespace CybernetCtrl.Server
{
    partial class ServerMainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ServerMainForm));
            this.tabControl1 = new MetroFramework.Controls.MetroTabControl();
            this.tabUsers = new System.Windows.Forms.TabPage();
            this.usersControl1 = new CybernetCtrl.Server.UsersControl();
            this.tabRequests = new System.Windows.Forms.TabPage();
            this.urlRequestControl1 = new CybernetCtrl.Server.UrlRequestControl();
            this.tabGroups = new System.Windows.Forms.TabPage();
            this.groupsControl1 = new CybernetCtrl.Server.GroupsControl();
            this.tabAllowed = new System.Windows.Forms.TabPage();
            this.urlsAllowedControl1 = new CybernetCtrl.Server.UrlsAllowedControl();
            this.tabBlocked = new System.Windows.Forms.TabPage();
            this.urlsBlockedControl1 = new CybernetCtrl.Server.UrlsBlockedControl();
            this.lnkRegister = new MetroFramework.Controls.MetroLink();
            this.lnkLogging = new MetroFramework.Controls.MetroLink();
            this.lnkOpenLogDirectory = new MetroFramework.Controls.MetroLink();
            this.metroLabel1 = new MetroFramework.Controls.MetroLabel();
            this.lnkChangePassword = new MetroFramework.Controls.MetroLink();
            this.tabControl1.SuspendLayout();
            this.tabUsers.SuspendLayout();
            this.tabRequests.SuspendLayout();
            this.tabGroups.SuspendLayout();
            this.tabAllowed.SuspendLayout();
            this.tabBlocked.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl1.Controls.Add(this.tabUsers);
            this.tabControl1.Controls.Add(this.tabRequests);
            this.tabControl1.Controls.Add(this.tabGroups);
            this.tabControl1.Controls.Add(this.tabAllowed);
            this.tabControl1.Controls.Add(this.tabBlocked);
            this.tabControl1.Location = new System.Drawing.Point(23, 63);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(859, 479);
            this.tabControl1.TabIndex = 1;
            this.tabControl1.UseSelectable = true;
            // 
            // tabUsers
            // 
            this.tabUsers.Controls.Add(this.usersControl1);
            this.tabUsers.Location = new System.Drawing.Point(4, 38);
            this.tabUsers.Name = "tabUsers";
            this.tabUsers.Size = new System.Drawing.Size(851, 437);
            this.tabUsers.TabIndex = 0;
            this.tabUsers.Text = "Users";
            // 
            // usersControl1
            // 
            this.usersControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.usersControl1.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.usersControl1.Location = new System.Drawing.Point(0, 0);
            this.usersControl1.Name = "usersControl1";
            this.usersControl1.Size = new System.Drawing.Size(851, 437);
            this.usersControl1.TabIndex = 0;
            this.usersControl1.UseSelectable = true;
            // 
            // tabRequests
            // 
            this.tabRequests.Controls.Add(this.urlRequestControl1);
            this.tabRequests.Location = new System.Drawing.Point(4, 38);
            this.tabRequests.Name = "tabRequests";
            this.tabRequests.Size = new System.Drawing.Size(851, 437);
            this.tabRequests.TabIndex = 1;
            this.tabRequests.Text = "Requests";
            // 
            // urlRequestControl1
            // 
            this.urlRequestControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.urlRequestControl1.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.urlRequestControl1.Location = new System.Drawing.Point(0, 0);
            this.urlRequestControl1.Name = "urlRequestControl1";
            this.urlRequestControl1.Size = new System.Drawing.Size(851, 437);
            this.urlRequestControl1.TabIndex = 0;
            this.urlRequestControl1.UseSelectable = true;
            // 
            // tabGroups
            // 
            this.tabGroups.Controls.Add(this.groupsControl1);
            this.tabGroups.Location = new System.Drawing.Point(4, 38);
            this.tabGroups.Name = "tabGroups";
            this.tabGroups.Size = new System.Drawing.Size(851, 437);
            this.tabGroups.TabIndex = 2;
            this.tabGroups.Text = "Groups";
            // 
            // groupsControl1
            // 
            this.groupsControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupsControl1.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupsControl1.Location = new System.Drawing.Point(0, 0);
            this.groupsControl1.Name = "groupsControl1";
            this.groupsControl1.Size = new System.Drawing.Size(851, 437);
            this.groupsControl1.TabIndex = 0;
            this.groupsControl1.UseSelectable = true;
            // 
            // tabAllowed
            // 
            this.tabAllowed.Controls.Add(this.urlsAllowedControl1);
            this.tabAllowed.Location = new System.Drawing.Point(4, 38);
            this.tabAllowed.Name = "tabAllowed";
            this.tabAllowed.Size = new System.Drawing.Size(851, 437);
            this.tabAllowed.TabIndex = 3;
            this.tabAllowed.Text = "Allowed";
            // 
            // urlsAllowedControl1
            // 
            this.urlsAllowedControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.urlsAllowedControl1.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.urlsAllowedControl1.Location = new System.Drawing.Point(0, 0);
            this.urlsAllowedControl1.Name = "urlsAllowedControl1";
            this.urlsAllowedControl1.Size = new System.Drawing.Size(851, 437);
            this.urlsAllowedControl1.TabIndex = 0;
            this.urlsAllowedControl1.UseSelectable = true;
            // 
            // tabBlocked
            // 
            this.tabBlocked.Controls.Add(this.urlsBlockedControl1);
            this.tabBlocked.Location = new System.Drawing.Point(4, 38);
            this.tabBlocked.Name = "tabBlocked";
            this.tabBlocked.Size = new System.Drawing.Size(851, 437);
            this.tabBlocked.TabIndex = 4;
            this.tabBlocked.Text = "Blocked";
            // 
            // urlsBlockedControl1
            // 
            this.urlsBlockedControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.urlsBlockedControl1.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.urlsBlockedControl1.Location = new System.Drawing.Point(0, 0);
            this.urlsBlockedControl1.Name = "urlsBlockedControl1";
            this.urlsBlockedControl1.Size = new System.Drawing.Size(851, 437);
            this.urlsBlockedControl1.TabIndex = 0;
            this.urlsBlockedControl1.UseSelectable = true;
            // 
            // lnkRegister
            // 
            this.lnkRegister.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lnkRegister.Location = new System.Drawing.Point(807, 34);
            this.lnkRegister.Name = "lnkRegister";
            this.lnkRegister.Size = new System.Drawing.Size(75, 23);
            this.lnkRegister.TabIndex = 2;
            this.lnkRegister.Text = "Register";
            this.lnkRegister.UseSelectable = true;
            this.lnkRegister.Click += new System.EventHandler(this.lnkRegister_Click);
            // 
            // lnkLogging
            // 
            this.lnkLogging.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lnkLogging.Location = new System.Drawing.Point(642, 544);
            this.lnkLogging.Name = "lnkLogging";
            this.lnkLogging.Size = new System.Drawing.Size(104, 23);
            this.lnkLogging.TabIndex = 3;
            this.lnkLogging.Text = "Enable Logging";
            this.lnkLogging.UseSelectable = true;
            this.lnkLogging.Click += new System.EventHandler(this.lnkLogging_Click);
            // 
            // lnkOpenLogDirectory
            // 
            this.lnkOpenLogDirectory.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lnkOpenLogDirectory.Location = new System.Drawing.Point(752, 544);
            this.lnkOpenLogDirectory.Name = "lnkOpenLogDirectory";
            this.lnkOpenLogDirectory.Size = new System.Drawing.Size(130, 23);
            this.lnkOpenLogDirectory.TabIndex = 4;
            this.lnkOpenLogDirectory.Text = "Open Log Directory";
            this.lnkOpenLogDirectory.UseSelectable = true;
            this.lnkOpenLogDirectory.Click += new System.EventHandler(this.lnkOpenLogDirectory_Click);
            // 
            // metroLabel1
            // 
            this.metroLabel1.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.metroLabel1.AutoSize = true;
            this.metroLabel1.Location = new System.Drawing.Point(310, 544);
            this.metroLabel1.Name = "metroLabel1";
            this.metroLabel1.Size = new System.Drawing.Size(285, 19);
            this.metroLabel1.TabIndex = 1;
            this.metroLabel1.Text = "Copyright © Cybernet-Ctrl. All Rights Reserved";
            // 
            // lnkChangePassword
            // 
            this.lnkChangePassword.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lnkChangePassword.Location = new System.Drawing.Point(687, 34);
            this.lnkChangePassword.Name = "lnkChangePassword";
            this.lnkChangePassword.Size = new System.Drawing.Size(114, 23);
            this.lnkChangePassword.TabIndex = 5;
            this.lnkChangePassword.Text = "Change Password";
            this.lnkChangePassword.UseSelectable = true;
            this.lnkChangePassword.Click += new System.EventHandler(this.lnkChangePassword_Click);
            // 
            // ServerMainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(905, 565);
            this.Controls.Add(this.lnkChangePassword);
            this.Controls.Add(this.metroLabel1);
            this.Controls.Add(this.lnkOpenLogDirectory);
            this.Controls.Add(this.lnkLogging);
            this.Controls.Add(this.lnkRegister);
            this.Controls.Add(this.tabControl1);
            this.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "ServerMainForm";
            this.Text = "Cybernet-Ctrl Administrator";
            this.Load += new System.EventHandler(this.ServerForm_Load);
            this.tabControl1.ResumeLayout(false);
            this.tabUsers.ResumeLayout(false);
            this.tabRequests.ResumeLayout(false);
            this.tabGroups.ResumeLayout(false);
            this.tabAllowed.ResumeLayout(false);
            this.tabBlocked.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private MetroFramework.Controls.MetroTabControl tabControl1;
        private System.Windows.Forms.TabPage tabUsers;
        private System.Windows.Forms.TabPage tabRequests;
        private System.Windows.Forms.TabPage tabGroups;
        private System.Windows.Forms.TabPage tabAllowed;
        private System.Windows.Forms.TabPage tabBlocked;
        private UsersControl usersControl1;
        private UrlRequestControl urlRequestControl1;
        private GroupsControl groupsControl1;
        private UrlsAllowedControl urlsAllowedControl1;
        private UrlsBlockedControl urlsBlockedControl1;
        private MetroFramework.Controls.MetroLink lnkRegister;
        private MetroFramework.Controls.MetroLink lnkLogging;
        private MetroFramework.Controls.MetroLink lnkOpenLogDirectory;
        private MetroFramework.Controls.MetroLabel metroLabel1;
        private MetroFramework.Controls.MetroLink lnkChangePassword;


    }
}

