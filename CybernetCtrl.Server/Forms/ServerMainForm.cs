﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using CybernetCtrl.TcpComm;
using CybernetCtrl.Message;
using CybernetCtrl.Common;
using CybernetCtrl.Modal;
using MetroFramework.Forms;
using System.IO;
using System.Diagnostics;
using CybernetCtrl.Common.Forms;

namespace CybernetCtrl.Server {
    public partial class ServerMainForm : MetroForm {
        public ServerMainForm() {
            InitializeComponent();

            Logger.Init();

            CopyFiles();

            SetFirewallSettings();

            lnkLogging.Text = Logger.IsLogEnabled ? "Disable Logging" : "Enable Logging";

            usersControl1.AccessModeTypeChanged += usersControl1_AccessModeTypeChanged;

            groupsControl1.GroupSettingsChanged += groupsControl1_GroupSettingsChanged;
        }

        private void ServerForm_Load(object sender, EventArgs e) {
            AdminAuthorizationForm authorizationForm = new AdminAuthorizationForm();
            if (authorizationForm.ShowDialog() != System.Windows.Forms.DialogResult.OK) {
                Environment.Exit(0);
                return;
            }

            TcpCommunicator.Instance.StartServer();

            usersControl1.LoadControl();

            urlRequestControl1.LoadControl();

            groupsControl1.LoadControl();

            urlsAllowedControl1.LoadControl();

            urlsBlockedControl1.LoadControl();

            NetworkSerialAdapter.Load();

            Thread readerThread = new Thread(new ThreadStart(ReaderThread));
            readerThread.IsBackground = true;
            readerThread.Start();

            Thread commandThread = new Thread(new ThreadStart(CommandThread));
            commandThread.IsBackground = true;
            commandThread.Start();
        }

        private void SetFirewallSettings() {
            WinFirewall.EnablePorts();

            WinFirewall.ActivateApplication("CybernetCtrl Server");
        }

        private void ReaderThread() {
            ClientMessageProcessor messageProcessor = new ClientMessageProcessor();
            messageProcessor.ClientVersionReceived += new EventHandler<ClientVersionEventArgs>(MessageProcessor_ClientVersionReceived);
            messageProcessor.PatchAppliedReceived += new EventHandler<PatchAppliedEventArgs>(MessageProcessor_PatchAppliedReceived);

            while (true) {
                List<MessageBase> messages = TcpCommunicator.Instance.DequeMessage();

                // process messages received
                foreach (MessageBase message in messages) {
                    if (message is IsAliveMessage) {
                        Logger.Log("Is alive message received", LogType.Information, ApplicationType.Server);
                        IsAliveMessage isAliveMessage = (IsAliveMessage)message;
                        usersControl1.SetIsAlive(isAliveMessage.SourceIP);
                    } else {
                        messageProcessor.ProcessMessage(message);
                    }
                }

                Thread.Sleep(10000);
            }
        }

        private void MessageProcessor_ClientVersionReceived(object sender, ClientVersionEventArgs e) {
            if (InvokeRequired) {
                this.Invoke((MethodInvoker)delegate {
                    MessageProcessor_ClientVersionReceived(sender, e);
                });
            } else {
                usersControl1.SetCompatibility(e.UserID, e.IsCompatible);
            }
        }

        private void MessageProcessor_PatchAppliedReceived(object sender, PatchAppliedEventArgs e) {
            if (InvokeRequired) {
                this.Invoke((MethodInvoker)delegate {
                    MessageProcessor_PatchAppliedReceived(sender, e);
                });
            } else {
                usersControl1.SetPatchApplied(e.UserID, e.Name);
            }
        }

        private void CommandThread() {
            Synchronizer.Synchronize();
        }

        private void groupsControl1_GroupSettingsChanged(object sender, EventArgs e) {
            urlsAllowedControl1.LoadControl();
        }

        private void usersControl1_AccessModeTypeChanged(object sender, EventArgs e) {
            if (usersControl1.SelectedUser != null) {
                AccessModeType modeType = AccessModeServer.GetAccessMode(usersControl1.SelectedUser.ID);
                switch (modeType) {
                    case AccessModeType.SelectiveAllow:
                        if (tabControl1.TabPages.Contains(tabBlocked)) tabControl1.TabPages.Remove(tabBlocked);

                        if (tabControl1.TabPages.Contains(tabRequests) == false) tabControl1.TabPages.Add(tabRequests);
                        if (tabControl1.TabPages.Contains(tabGroups) == false) tabControl1.TabPages.Add(tabGroups);
                        if (tabControl1.TabPages.Contains(tabAllowed) == false) tabControl1.TabPages.Add(tabAllowed);
                        break;
                    case AccessModeType.SelectiveBlock:
                        if (tabControl1.TabPages.Contains(tabRequests)) tabControl1.TabPages.Remove(tabRequests);
                        if (tabControl1.TabPages.Contains(tabGroups)) tabControl1.TabPages.Remove(tabGroups);
                        if (tabControl1.TabPages.Contains(tabAllowed)) tabControl1.TabPages.Remove(tabAllowed);

                        if (tabControl1.TabPages.Contains(tabBlocked) == false) tabControl1.TabPages.Add(tabBlocked);
                        break;
                }
            }
        }

        private void lnkRegister_Click(object sender, EventArgs e) {
            
        }

        private void lnkLogging_Click(object sender, EventArgs e) {
            if (lnkLogging.Text == "Enable Logging") {
                lnkLogging.Text = "Disable Logging";
                Logger.IsLogEnabled = true;
            } else {
                lnkLogging.Text = "Enable Logging";
                Logger.IsLogEnabled = false;
            }
        }

        private void lnkOpenLogDirectory_Click(object sender, EventArgs e) {
            if (Directory.Exists(Logger.LogDir)) {
                Process.Start(Logger.LogDir);
            } else {
                MessageBox.Show("Log folder not found. Please enable logging and try to open log folder again.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void CopyFiles() {
            string localDir = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) + @"\" + CybernetCtrl.Common.Settings.LocalDir;
            string destFile = localDir + @"\ffmpeg.exe";
            string sourceFile = Application.StartupPath + @"\ffmpeg.exe";

            if (Directory.Exists(localDir) == false) {
                Directory.CreateDirectory(localDir);
            }

            File.Copy(sourceFile, destFile, true);
        }

        private void lnkChangePassword_Click(object sender, EventArgs e) {
            (new ChangeAdminPasswordForm()).ShowDialog();
        }
    }
}