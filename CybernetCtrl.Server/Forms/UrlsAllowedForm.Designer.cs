﻿namespace CybernetCtrl.Server
{
    partial class UrlsAllowedForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dgrWebsitesAllowed = new System.Windows.Forms.DataGridView();
            this.colUrl = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btnClose = new MetroFramework.Controls.MetroButton();
            this.btnBlock = new MetroFramework.Controls.MetroButton();
            ((System.ComponentModel.ISupportInitialize)(this.dgrWebsitesAllowed)).BeginInit();
            this.SuspendLayout();
            // 
            // dgrWebsitesAllowed
            // 
            this.dgrWebsitesAllowed.AllowUserToAddRows = false;
            this.dgrWebsitesAllowed.AllowUserToDeleteRows = false;
            this.dgrWebsitesAllowed.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.dgrWebsitesAllowed.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgrWebsitesAllowed.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgrWebsitesAllowed.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colUrl});
            this.dgrWebsitesAllowed.Location = new System.Drawing.Point(23, 63);
            this.dgrWebsitesAllowed.MultiSelect = false;
            this.dgrWebsitesAllowed.Name = "dgrWebsitesAllowed";
            this.dgrWebsitesAllowed.ReadOnly = true;
            this.dgrWebsitesAllowed.RowHeadersVisible = false;
            this.dgrWebsitesAllowed.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgrWebsitesAllowed.Size = new System.Drawing.Size(590, 280);
            this.dgrWebsitesAllowed.TabIndex = 4;
            // 
            // colUrl
            // 
            this.colUrl.HeaderText = "Website";
            this.colUrl.Name = "colUrl";
            this.colUrl.ReadOnly = true;
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.Location = new System.Drawing.Point(538, 349);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 28);
            this.btnClose.TabIndex = 32;
            this.btnClose.Text = "Close";
            this.btnClose.UseSelectable = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnBlock
            // 
            this.btnBlock.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnBlock.Location = new System.Drawing.Point(457, 349);
            this.btnBlock.Name = "btnBlock";
            this.btnBlock.Size = new System.Drawing.Size(75, 28);
            this.btnBlock.TabIndex = 33;
            this.btnBlock.Text = "Block";
            this.btnBlock.UseSelectable = true;
            this.btnBlock.Click += new System.EventHandler(this.btnBlock_Click);
            // 
            // UrlsAllowedForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(636, 400);
            this.Controls.Add(this.btnBlock);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.dgrWebsitesAllowed);
            this.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name = "UrlsAllowedForm";
            this.Text = "Websites Allowed";
            ((System.ComponentModel.ISupportInitialize)(this.dgrWebsitesAllowed)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dgrWebsitesAllowed;
        private System.Windows.Forms.DataGridViewTextBoxColumn colUrl;
        private MetroFramework.Controls.MetroButton btnClose;
        private MetroFramework.Controls.MetroButton btnBlock;
    }
}