﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using CybernetCtrl.Modal;
using MetroFramework.Forms;

namespace CybernetCtrl.Server
{
    public partial class UrlsAllowedForm : MetroForm
    {
        private User user;

        public UrlsAllowedForm()
        {
            InitializeComponent();
        }

        public void LoadForm(User user)
        {
            this.user = user;

            List<UrlAllowedServer> urlsAllowed = UrlAllowedServer.GetAllowedUrls(user);
            foreach (UrlAllowedServer urlAllowed in urlsAllowed)
            {
                int index = dgrWebsitesAllowed.Rows.Add();
                dgrWebsitesAllowed[colUrl.Index, index].Value = urlAllowed.Url;

                dgrWebsitesAllowed.Rows[index].Tag = urlAllowed;
            }
        }

        private void btnBlock_Click(object sender, EventArgs e)
        {
            if (dgrWebsitesAllowed.SelectedRows.Count > 0)
            {
                UrlAllowedServer urlAllowed = (UrlAllowedServer)dgrWebsitesAllowed.SelectedRows[0].Tag;

                urlAllowed.Remove(this.user.ID);

                dgrWebsitesAllowed.Rows.Remove(dgrWebsitesAllowed.SelectedRows[0]);
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
        }
    }
}
