﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using CybernetCtrl.Modal;
using MetroFramework.Forms;
using System.IO;

namespace CybernetCtrl.Server
{
    public partial class UrlsBlockedDetailsForm : MetroForm
    {
        private int index = 0, pageSize = CybernetCtrl.Common.Settings.PageSize;
        private User user;
        private string fileName = string.Empty;

        public UrlsBlockedDetailsForm()
        {
            InitializeComponent();
        }

        public void LoadForm(User user)
        {
            this.user = user;

            dtpFrom.Value = new DateTime(2010, 1, 1);
            dtpTo.Value = DateTime.Now;

            LoadUrls();
        }

        private void LoadUrls()
        {
            List<UrlBlockedDetailsServer> urls = UrlBlockedDetailsServer.GetUrlsBlocked(user.ID, index, pageSize, txtSearch.Text, dtpFrom.Value, dtpTo.Value);

            dgrWebsitesVisited.Rows.Clear();
            foreach (UrlBlockedDetailsServer url in urls)
            {
                int rowIndex = dgrWebsitesVisited.Rows.Add();
                dgrWebsitesVisited.Rows[rowIndex].Cells[colUrl.Name].Value = url.Url;
                dgrWebsitesVisited.Rows[rowIndex].Cells[colDateTime.Name].Value = url.VisitedDateTime.ToString("dd-MMM-yyyy HH:mm:ss");
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
        }

        private void btnFilter_Click(object sender, EventArgs e)
        {
            index = 0;

            if (dtpFrom.Value > dtpTo.Value)
            {
                MessageBox.Show("Start date cannot be greater than end date", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            LoadUrls();
        }

        private void btnFirst_Click(object sender, EventArgs e)
        {
            index = 0;

            LoadUrls();
        }

        private void btnPrev_Click(object sender, EventArgs e)
        {
            index--;

            if (index < 0) index = 0;

            LoadUrls();
        }

        private void btnNext_Click(object sender, EventArgs e)
        {
            index++;

            int rowsCount = UrlBlockedDetailsServer.GetTotalUrls(user.ID, dtpFrom.Value, dtpTo.Value);

            int pagesCount = (int)Math.Ceiling((decimal)rowsCount / pageSize);

            if (index > pagesCount - 1)
            {
                index = pagesCount - 1;
            }

            LoadUrls();
        }

        private void btnLast_Click(object sender, EventArgs e)
        {
            int rowsCount = UrlBlockedDetailsServer.GetTotalUrls(user.ID, dtpFrom.Value, dtpTo.Value);

            index = (int)Math.Ceiling((decimal)rowsCount / pageSize) - 1;

            if (index < 0)
            {
                index = 0;
            }

            LoadUrls();
        }

        private void btnDownload_Click(object sender, EventArgs e) {
            List<UrlBlockedDetailsServer> urls = UrlBlockedDetailsServer.GetUrlsBlocked(user.ID, txtSearch.Text, dtpFrom.Value, dtpTo.Value);

            if (urls.Count > 0) {
                SaveFileDialog dlg = new SaveFileDialog();
                dlg.Filter = "Text files|*.txt";
                dlg.DefaultExt = ".txt";
                if (dlg.ShowDialog() == DialogResult.OK) {
                    string text = string.Empty;
                    foreach (UrlBlockedDetailsServer url in urls) {
                        text += url.Url + Environment.NewLine;
                    }
                    File.WriteAllText(dlg.FileName, text);
                }
            }
        }
    }
}