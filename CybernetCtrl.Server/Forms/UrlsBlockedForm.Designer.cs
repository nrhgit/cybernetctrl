﻿namespace CybernetCtrl.Server
{
    partial class UrlsBlockedForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dgrWebsitesBlocked = new System.Windows.Forms.DataGridView();
            this.colUrl = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btnClose = new MetroFramework.Controls.MetroButton();
            this.btnAllow = new MetroFramework.Controls.MetroButton();
            ((System.ComponentModel.ISupportInitialize)(this.dgrWebsitesBlocked)).BeginInit();
            this.SuspendLayout();
            // 
            // dgrWebsitesBlocked
            // 
            this.dgrWebsitesBlocked.AllowUserToAddRows = false;
            this.dgrWebsitesBlocked.AllowUserToDeleteRows = false;
            this.dgrWebsitesBlocked.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.dgrWebsitesBlocked.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgrWebsitesBlocked.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgrWebsitesBlocked.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colUrl});
            this.dgrWebsitesBlocked.Location = new System.Drawing.Point(23, 63);
            this.dgrWebsitesBlocked.MultiSelect = false;
            this.dgrWebsitesBlocked.Name = "dgrWebsitesBlocked";
            this.dgrWebsitesBlocked.ReadOnly = true;
            this.dgrWebsitesBlocked.RowHeadersVisible = false;
            this.dgrWebsitesBlocked.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgrWebsitesBlocked.Size = new System.Drawing.Size(603, 285);
            this.dgrWebsitesBlocked.TabIndex = 7;
            // 
            // colUrl
            // 
            this.colUrl.HeaderText = "Website";
            this.colUrl.Name = "colUrl";
            this.colUrl.ReadOnly = true;
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.Location = new System.Drawing.Point(551, 354);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 28);
            this.btnClose.TabIndex = 33;
            this.btnClose.Text = "Close";
            this.btnClose.UseSelectable = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnAllow
            // 
            this.btnAllow.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAllow.Location = new System.Drawing.Point(470, 354);
            this.btnAllow.Name = "btnAllow";
            this.btnAllow.Size = new System.Drawing.Size(75, 28);
            this.btnAllow.TabIndex = 34;
            this.btnAllow.Text = "Allow";
            this.btnAllow.UseSelectable = true;
            this.btnAllow.Click += new System.EventHandler(this.btnAllow_Click);
            // 
            // UrlsBlockedForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(649, 403);
            this.Controls.Add(this.btnAllow);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.dgrWebsitesBlocked);
            this.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "UrlsBlockedForm";
            this.Text = "Websites Blocked";
            ((System.ComponentModel.ISupportInitialize)(this.dgrWebsitesBlocked)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dgrWebsitesBlocked;
        private System.Windows.Forms.DataGridViewTextBoxColumn colUrl;
        private MetroFramework.Controls.MetroButton btnClose;
        private MetroFramework.Controls.MetroButton btnAllow;
    }
}