﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using CybernetCtrl.Modal;
using MetroFramework.Forms;

namespace CybernetCtrl.Server
{
    public partial class UrlsBlockedForm : MetroForm
    {
        private User user;

        public UrlsBlockedForm()
        {
            InitializeComponent();
        }

        public void LoadForm(User user)
        {
            this.user = user;

            List<UrlBlockedServer> urlsBlocked = UrlBlockedServer.GetBlockedUrls(user);
            foreach (UrlBlockedServer urlBlocked in urlsBlocked)
            {
                int index = dgrWebsitesBlocked.Rows.Add();
                dgrWebsitesBlocked[colUrl.Index, index].Value = urlBlocked.Url;

                dgrWebsitesBlocked.Rows[index].Tag = urlBlocked;
            }
        }

        private void btnAllow_Click(object sender, EventArgs e)
        {
            if (dgrWebsitesBlocked.SelectedRows.Count > 0)
            {
                UrlBlockedServer urlBlocked = (UrlBlockedServer)dgrWebsitesBlocked.SelectedRows[0].Tag;

                urlBlocked.Remove(this.user.ID);

                dgrWebsitesBlocked.Rows.Remove(dgrWebsitesBlocked.SelectedRows[0]);
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
        }
    }
}
