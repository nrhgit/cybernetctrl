﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using CybernetCtrl.Modal;
using MetroFramework.Forms;

namespace CybernetCtrl.Server
{
    public partial class UsersForm : MetroForm
    {
        public UsersForm()
        {
            InitializeComponent();
        }

        public void LoadForm(List<User> users)
        {
            dgrUsers.Rows.Clear();
            foreach (User user in users)
            {
                AddRow(user);
            }
        }

        private void AddRow(User user)
        {
            int index = dgrUsers.Rows.Add();
            dgrUsers[colName.Name, index].Value = user.Name;
            dgrUsers[colIP.Name, index].Value = user.IP;
            dgrUsers.Rows[index].Tag = user;
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
        }
    }
}
