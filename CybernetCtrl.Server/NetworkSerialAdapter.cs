﻿using System;
using System.Collections.Generic;
using System.Text;
using CybernetCtrl.Common;
using System.Threading;
using CybernetCtrl.TcpComm;
using CybernetCtrl.Message;

namespace CybernetCtrl.Server {
    internal class NetworkSerialAdapter {
        
        public static List<NetworkNode> Nodes { get; set; }

        static NetworkSerialAdapter() {
            Nodes = new List<NetworkNode>();
        }

        public static void Load() {
            Thread serialReadThread = new Thread(new ThreadStart(SerialReadThread));
            serialReadThread.IsBackground = true;
            serialReadThread.Start();
        }

        public static void SetNodeSerial(string ip, string serial) {
            foreach (NetworkNode node in Nodes) {
                if (node.IP == ip) {
                    node.Serial = serial;
                    break;
                }
            }
        }

        private static void SerialReadThread() {
            while (true) {
                List<NetworkNode> nodes = IPReader.GetIps();
                foreach (NetworkNode node in nodes) {
                    bool exists = false;

                    foreach (NetworkNode existingNode in Nodes) {
                        if (existingNode.IP == node.IP) {
                            exists = true;
                            break;
                        }
                    }

                    if (String.IsNullOrEmpty(node.Serial)) {
                        SendNodeSerialCommand cmd = new SendNodeSerialCommand() { IP = node.IP };
                        TcpCommunicator.Instance.EnqueMessage(cmd);
                    }

                    if (exists == false) {
                        Nodes.Add(node);
                    }
                }

                Thread.Sleep(30 * 1000); // every 30 seconds
            }
        }
    }
}
