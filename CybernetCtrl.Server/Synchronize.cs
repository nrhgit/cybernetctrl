﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using CybernetCtrl.Message;
using CybernetCtrl.Modal;
using CybernetCtrl.TcpComm;
using System.Windows.Forms;
using CybernetCtrl.Common;

namespace CybernetCtrl.Server {
    internal abstract class Synchronizer {
        public static void Synchronize() {
            int loopDelay = 10000;
            int userDelay = 1000;

            while (true) {
                List<User> users = User.GetUsers();

                // set dynamic IP of all users
                foreach (User user in users) {
                    foreach (NetworkNode node in NetworkSerialAdapter.Nodes) {
                        if (node.Serial == user.Serial) {
                            user.IP = node.IP;
                            user.IPVerified = true;
                            break;
                        }
                    }
                }

                User trialUser = null;
#if LICENSE
                if (CybernetCtrl.License.LicenseManager.HasClientLicense == false && users.Count > 1)
                { 
                    trialUser = users[0];

                    MessageBox.Show(String.Format("Cybernet-Ctrl is either not registered or license is expired. Hence data is synchronized only for the user: {0}. Please click OK to continue.", trialUser.Name), "License", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
#elif FIXED_LICENSING
                if (CybernetCtrl.License.FixedLicenseManager.HasServerLicense == false && users.Count > 1)
                { 
                    trialUser = users[0];

                    MessageBox.Show(String.Format("Cybernet-Ctrl is either not registered or license is expired. Hence data is synchronized only for the user: {0}. Please click OK to continue.", trialUser.Name), "License", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
#endif

                // CHECK CLIENT VERSIONS
                foreach (User user in users) {
                    if (user.IPVerified == false) continue;

                    if (trialUser == null || user == trialUser) {
                        Logger.Log("Sending expectec client version: " + user.Name + " IP: " + user.IP, LogType.Information, ApplicationType.Server);

                        SyncVersionCommand message = new SyncVersionCommand() { IP = user.IP, UserID = user.ID, ExpectedClientVersion = Settings.CompatibleClientVersion };

                        TcpCommunicator.Instance.EnqueMessage(message);

                        Thread.Sleep(userDelay);
                    }
                }

                // SERVER TIME STAMP
                foreach (User user in users) {
                    if (user.IPVerified == false) continue;

                    if (trialUser == null || user == trialUser) {
                        Logger.Log("Sending server timestamp to user: " + user.Name + " IP: " + user.IP, LogType.Information, ApplicationType.Server);

                        ServerTimeStampMessage message = new ServerTimeStampMessage() { IP = user.IP, ServerDateTime = DateTime.Now };

                        TcpCommunicator.Instance.EnqueMessage(message);

                        Thread.Sleep(userDelay);
                    }
                }

                // ADMIN PASSWORD
                foreach (User user in users) {
                    if (user.IPVerified == false) continue;

                    if (trialUser == null || user == trialUser) {
                        Logger.Log("Sending admin password to user: " + user.Name + " IP: " + user.IP, LogType.Information, ApplicationType.Server);

                        AdminPasswordMessage message = new AdminPasswordMessage() { IP = user.IP, Password = AdminPassword.Password };

                        TcpCommunicator.Instance.EnqueMessage(message);

                        Thread.Sleep(userDelay);
                    }
                }

                // CLIENT LICENSE
                foreach (User user in users) {
                    if (user.IPVerified == false) continue;

                    if (trialUser == null || user == trialUser) {
                        Logger.Log("Sending license key to user: " + user.Name + " IP: " + user.IP, LogType.Information, ApplicationType.Server);

                        ClientLicense license = ClientLicense.GetLicense(user.Serial);
                        ClientLicenseMessage message = new ClientLicenseMessage() { IP = user.IP, License = (license == null ? "" : license.License) };
                        
                        TcpCommunicator.Instance.EnqueMessage(message);

                        Thread.Sleep(userDelay);
                    }
                }

                // ACCESS MODE
                foreach (User user in users) {
                    if (user.IPVerified == false) continue;

                    if (trialUser == null || user == trialUser) {
                        Logger.Log("Sending access mode to user: " + user.Name + " IP: " + user.IP, LogType.Information, ApplicationType.Server);

                        UserAccessModeMessage message = new UserAccessModeMessage() { IP = user.IP, ModType = AccessModeServer.GetAccessMode(user.ID) };

                        TcpCommunicator.Instance.EnqueMessage(message);

                        Thread.Sleep(userDelay);
                    }
                }

                // SYNC FILE LIMIT
                foreach (User user in users) {
                    if (user.IPVerified == false) continue;

                    if (trialUser == null || user == trialUser) {
                        UserFileLimitMessage message = new UserFileLimitMessage() { IP = user.IP, FileLimit = UserFileLimitServer.GetUserFileLimit(user.ID) };

                        TcpCommunicator.Instance.EnqueMessage(message);

                        Thread.Sleep(userDelay);
                    }
                }

                // ASK USERS TO SEND VISITED URLS
                foreach (User user in users) {
                    if (user.IPVerified == false) continue;

                    if (trialUser == null || user == trialUser) {
                        Logger.Log("Sending visited urls command message to user: " + user.Name + " IP: " + user.IP, LogType.Information, ApplicationType.Server);

                        long lastMilliSeconds = UrlVisitedServer.GetLastVisitedMilliSeconds(user.ID);

                        SendUrlsVistedCommand cmd = new SendUrlsVistedCommand() { IP = user.IP, UserID = user.ID, LastMilliSeconds = lastMilliSeconds };

                        TcpCommunicator.Instance.EnqueMessage(cmd);

                        Thread.Sleep(userDelay);
                    }
                }

                // ASK USERS TO SEND BLOCKED URLS
                foreach (User user in users) {
                    if (user.IPVerified == false) continue;

                    if (trialUser == null || user == trialUser) {
                        Logger.Log("Sending blocked urls command message to user: " + user.Name + " IP: " + user.IP, LogType.Information, ApplicationType.Server);

                        long lastMilliSeconds = UrlBlockedDetailsServer.GetLastVisitedMilliSeconds(user.ID);

                        SendUrlsBlockedDetailCommand cmd = new SendUrlsBlockedDetailCommand() { IP = user.IP, UserID = user.ID, LastMilliSeconds = lastMilliSeconds };

                        TcpCommunicator.Instance.EnqueMessage(cmd);

                        Thread.Sleep(userDelay);
                    }
                }

                // ASK USERS TO SEND ALL URL ACCESS REQUESTS
                foreach (User user in users) {
                    if (user.IPVerified == false) continue;

                    if (trialUser == null || user == trialUser) {
                        Logger.Log("Sending requested urls command message to user: " + user.Name + " IP: " + user.IP, LogType.Information, ApplicationType.Server);

                        DateTime lastVisitedDateTime = UrlRequestServer.GetLastRequestDateTime(user.ID);

                        SendUrlRequestCommand cmd = new SendUrlRequestCommand() { IP = user.IP, UserID = user.ID, LastDateTime = lastVisitedDateTime };

                        TcpCommunicator.Instance.EnqueMessage(cmd);

                        Thread.Sleep(userDelay);
                    }
                }

                // SYNC GROUPS
                foreach (User user in users) {
                    if (user.IPVerified == false) continue;

                    if (trialUser == null || user == trialUser) {
                        Logger.Log("Synchronizing groups with user: " + user.Name + " IP: " + user.IP, LogType.Information, ApplicationType.Server);

                        Synchronizer.SynchronizeGroups(user.IP);

                        Thread.Sleep(userDelay);
                    }
                }

                // SYNC ALLOWED URLS
                foreach (User user in users) {
                    if (user.IPVerified == false) continue;

                    if (trialUser == null || user == trialUser) {
                        Logger.Log("Synchronizing allowed urls with user: " + user.Name + " IP: " + user.IP, LogType.Information, ApplicationType.Server);

                        Synchronizer.SynchronizeAllowedUrls(user.IP, user);

                        Thread.Sleep(userDelay);
                    }
                }

                // SYNC BLOCKED URLS
                foreach (User user in users) {
                    if (user.IPVerified == false) continue;

                    if (trialUser == null || user == trialUser) {
                        Logger.Log("Synchronizing blocked urls with user: " + user.Name + " IP: " + user.IP, LogType.Information, ApplicationType.Server);

                        Synchronizer.SynchronizeBlockedUrls(user.IP, user);

                        Thread.Sleep(userDelay);
                    }
                }

                // SYNC PATCH APPLIED
                foreach (User user in users) {
                    if (user.IPVerified == false) continue;

                    if (trialUser == null || user == trialUser) {
                        SyncPatchCommand message = new SyncPatchCommand() { IP = user.IP, UserID = user.ID };

                        TcpCommunicator.Instance.EnqueMessage(message);

                        Thread.Sleep(userDelay);
                    }
                }

                Thread.Sleep(loopDelay);
            }
        }

        public static void Validate(MessageBase message) {
            if (message is ValidateGroupMessage) {
                (new GroupSynchronizer() { IP = message.SourceIP }).ValidateData(message);
            } else if (message is ValidateUrlAllowedMessage) {
                (new UrlAllowedSynchronizer() { IP = message.SourceIP }).ValidateData(message);
            } else if (message is ValidateUrlBlockedMessage) {
                (new UrlBlockedSynchronizer() { IP = message.SourceIP }).ValidateData(message);
            }
        }

        private static void SynchronizeGroups(string ip) {
            Synchronize(new GroupSynchronizer() { IP = ip });
        }

        public static void SynchronizeAllowedUrls(string ip, User user) {
            Synchronize(new UrlAllowedSynchronizer() { IP = ip, User = user });
        }

        public static void SynchronizeBlockedUrls(string ip, User user) {
            Synchronize(new UrlBlockedSynchronizer() { IP = ip, User = user });
        }

        private static void Synchronize(SynchronizerBase synchronizer) {
            // Updates all existing data
            synchronizer.Update();

            // Ask client to send all its group for validation
            synchronizer.InitValidation();
        }
    }

    internal abstract class SynchronizerBase {
        internal string IP { get; set; }

        public abstract void Update();

        public abstract void InitValidation();

        public abstract void ValidateData(MessageBase message);
    }

    internal class GroupSynchronizer : SynchronizerBase {
        public override void Update() {
            // get all groups and send it to client
            List<UrlGroupServer> groups = UrlGroupServer.GetGroups();
            foreach (UrlGroupServer group in groups) {
                GroupMessage message = new GroupMessage() { IP = IP, ID = group.ID, Name = group.Name, AllowedDuration = group.AllowedDuration, SessionDuration = group.SessionDuration };

                TcpCommunicator.Instance.EnqueMessage(message);
            }
        }

        public override void InitValidation() {
            TcpCommunicator.Instance.EnqueMessage(new InitValidateGroupMessage() { IP = this.IP });
        }

        public override void ValidateData(MessageBase message) {
            ValidateGroupMessage validateGroupMessage = (ValidateGroupMessage)message;

            UrlGroupServer group = UrlGroupServer.GetGroup(validateGroupMessage.GroupID);
            if (group == null) {
                TcpCommunicator.Instance.EnqueMessage(new InvalidGroupCommand() { IP = this.IP, GroupID = validateGroupMessage.GroupID });
            }
        }
    }

    internal class UrlAllowedSynchronizer : SynchronizerBase {
        public User User { get; set; }

        public override void Update() {
            // get all urls and send it to client
            List<UrlAllowedServer> urls = UrlAllowedServer.GetAllowedUrls(User);
            foreach (UrlAllowedServer url in urls) {
                UrlAllowedMessage message = new UrlAllowedMessage() { IP = this.IP, Url = url.Url, GroupID = url.GroupID };

                TcpCommunicator.Instance.EnqueMessage(message);
            }
        }

        public override void InitValidation() {
            TcpCommunicator.Instance.EnqueMessage(new InitValidateUrlAllowedMessage() { IP = this.IP });
        }

        public override void ValidateData(MessageBase message) {
            ValidateUrlAllowedMessage validateAllowedUrlMessage = (ValidateUrlAllowedMessage)message;

            bool exists = UrlAllowedServer.IsExists(validateAllowedUrlMessage.Url);
            if (exists == false) {
                TcpCommunicator.Instance.EnqueMessage(new InvalidUrlAllowedCommand() { IP = this.IP, Url = validateAllowedUrlMessage.Url });
            }
        }
    }

    internal class UrlBlockedSynchronizer : SynchronizerBase {
        public User User { get; set; }

        public override void Update() {
            // get all urls and send it to client
            List<UrlBlockedServer> urls = UrlBlockedServer.GetBlockedUrls(User);
            foreach (UrlBlockedServer url in urls) {
                UrlBlockedMessage message = new UrlBlockedMessage() { IP = this.IP, Url = url.Url };

                TcpCommunicator.Instance.EnqueMessage(message);
            }
        }

        public override void InitValidation() {
            TcpCommunicator.Instance.EnqueMessage(new InitValidateUrlBlockedMessage() { IP = this.IP });
        }

        public override void ValidateData(MessageBase message) {
            ValidateUrlBlockedMessage validateBlockedUrlMessage = (ValidateUrlBlockedMessage)message;

            bool exists = UrlBlockedServer.IsExists(validateBlockedUrlMessage.Url);
            if (exists == false) {
                TcpCommunicator.Instance.EnqueMessage(new InvalidBlockedUrlCommand() { IP = this.IP, Url = validateBlockedUrlMessage.Url });
            }
        }
    }
}