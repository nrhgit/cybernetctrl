Set objFirewall = CreateObject("HNetCfg.FwMgr")
Set objPolicy = objFirewall.LocalPolicy.CurrentProfile

Set colPorts = objPolicy.GloballyOpenPorts

Dim found1
Dim found2
found1 = False
found2 = False

For Each objPort in colPorts
	If objPort.Port = 7855 Then
		found1 = True
	End If
	If objPort.Port = 7856 Then
		found2 = True
	End If
Next

If Not found1 Then
	Set objPort = CreateObject("HNetCfg.FwOpenPort")
	objPort.Port = 7855
	objPort.Name = "Cybernet Port"
	objPort.Enabled = TRUE
	Set colPorts = objPolicy.GloballyOpenPorts

	errReturn = colPorts.Add(objPort)
End If

If Not found2 Then
	Set objPort = CreateObject("HNetCfg.FwOpenPort")
	objPort.Port = 7856
	objPort.Name = "Cybernet Remote port"
	objPort.Enabled = TRUE
	Set colPorts = objPolicy.GloballyOpenPorts

	errReturn = colPorts.Add(objPort)
End If