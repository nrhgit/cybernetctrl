Set objFirewall = CreateObject("HNetCfg.FwMgr")
Set objPolicy = objFirewall.LocalPolicy.CurrentProfile

Set colApplications = objPolicy.AuthorizedApplications

Set colArgs = WScript.Arguments.Named
strName = colArgs.Item("name")
strPath = colArgs.Item("path")

Dim found
found = False

For Each objApplication in colApplications
	'C:\Program Files (x86)\Cybernet-Ctrl.Server\CybernetCtrl.Server.exe
	If objApplication.ProcessImageFileName = strPath Then
		found = True
	End If    
Next

If Not found Then

	Wscript.Echo "not found"
	Set objApplication = CreateObject("HNetCfg.FwAuthorizedApplication")
	objApplication.Name = strName
	objApplication.IPVersion = 2
	objApplication.ProcessImageFileName = strPath
	objApplication.RemoteAddresses = "*"
	objApplication.Scope = 0
	objApplication.Enabled = True

	Set colApplications = objPolicy.AuthorizedApplications
	colApplications.Add(objApplication)

End If


