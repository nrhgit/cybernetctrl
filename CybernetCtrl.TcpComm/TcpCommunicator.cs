﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading;
using CybernetCtrl.Message;
using CybernetCtrl.Common;

namespace CybernetCtrl.TcpComm {
    public class TcpCommunicator {
        private const int port = 7855;
        private const int bufferSize = 500 * 1024; // 500 kb
        private const int queueSize = 5000;

        private Socket server;

        private static TcpCommunicator instance;
        private static object _receiverLock = new object();
        private static object _senderLock = new object();

        private Dictionary<string, BlockingQueue> receiveMessageQueues = new Dictionary<string, BlockingQueue>();
        private Dictionary<string, BlockingQueue> sendMessageQueues = new Dictionary<string, BlockingQueue>();

        private TcpCommunicator() { }

        public static TcpCommunicator Instance {
            get {
                if (instance == null) instance = new TcpCommunicator();

                return instance;
            }
        }

        public bool IsServerConnected {
            get {
                if (server != null && server.Connected) {
                    return true;
                }

                return false;
            }
        }

        public void StartServer() {
            Thread startServerThread = new Thread(new ThreadStart(StartServerThread));
            startServerThread.IsBackground = true;
            startServerThread.Start();
        }

        public void RegisterServer(string serverIP) {
            Thread registerClientThread = new Thread(new ParameterizedThreadStart(RegisterServerThread));
            registerClientThread.IsBackground = true;
            registerClientThread.Start(serverIP);
        }

        public void EnqueMessage(MessageBase message) {
            if (sendMessageQueues.ContainsKey(message.IP)) {
                sendMessageQueues[message.IP].Enqueue(message);
            }
        }

        public List<MessageBase> DequeMessage() {
            List<MessageBase> messages = new List<MessageBase>();
            string[] keys = new string[1000];

            receiveMessageQueues.Keys.CopyTo(keys, 0);

            foreach (string key in keys) {
                if (key == null) continue;

                try {
                    int count = receiveMessageQueues[key].Count;

                    Logger.Log("Dequeued messages. Count: " + count, LogType.Information, ApplicationType.TCP);

                    for (int i = 0; i < count; i++) {
                        object o = receiveMessageQueues[key].Dequeue(100);

                        if (o != null && o is MessageBase) {
                            messages.Add((MessageBase)o);
                        }
                    }
                } catch (QueueTimeoutException) {

                }
            }

            return messages;
        }

        private void StartServerThread() {
            Logger.Log("Starting server", LogType.Information, ApplicationType.TCP);

            IPEndPoint ip = new IPEndPoint(IPAddress.Any, port);
            Socket serverSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);

            serverSocket.Bind(ip);
            serverSocket.Listen(10);

            Logger.Log("Listening to new connections", LogType.Information, ApplicationType.TCP);

            while (true) {
                Logger.Log("Waiting for new socket connection", LogType.Information, ApplicationType.TCP);

                Socket clientSocket = serverSocket.Accept();

                Logger.Log("Accepted new socket connection", LogType.Information, ApplicationType.TCP);

                // start sender thread
                Thread senderThread = new Thread(new ParameterizedThreadStart(SenderThread));
                senderThread.IsBackground = true;
                senderThread.Start(new object[] { clientSocket, ((IPEndPoint)clientSocket.RemoteEndPoint).Address.ToString() });

                // start receiver thread
                Thread receiverThread = new Thread(new ParameterizedThreadStart(ReceiverThread));
                receiverThread.IsBackground = true;
                receiverThread.Start(new object[] { clientSocket, ((IPEndPoint)clientSocket.RemoteEndPoint).Address.ToString() });
            }
        }

        private void RegisterServerThread(object o) {
            string serverIP = (string)o;

            Logger.Log("Registering server: " + serverIP, LogType.Information, ApplicationType.TCP);

            IPEndPoint ip = new IPEndPoint(IPAddress.Parse(serverIP), port);
            server = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);

            try {
                server.Connect(ip);

                Logger.Log("Connected to server with IP: " + serverIP, LogType.Information, ApplicationType.TCP);

                // start sender thread
                Thread senderThread = new Thread(new ParameterizedThreadStart(SenderThread));
                senderThread.IsBackground = true;
                senderThread.Start(new object[] { server, serverIP });

                // start receiver thread
                Thread receiverThread = new Thread(new ParameterizedThreadStart(ReceiverThread));
                receiverThread.IsBackground = true;
                receiverThread.Start(new object[] { server, serverIP });
            } catch (Exception exc) {
                Logger.Log(exc.Message + Environment.NewLine + exc.StackTrace, LogType.Error, ApplicationType.TCP);
            }
        }

        private void SenderThread(object o) {
            Socket socket = (Socket)((object[])o)[0];
            string ip = (string)((object[])o)[1];

            if (sendMessageQueues.ContainsKey(ip) == false) {
                try {
                    Logger.Log("Adding new blocking sender queue for IP: " + ip, LogType.Information, ApplicationType.TCP);

                    sendMessageQueues.Add(ip, new BlockingQueue(queueSize));
                } catch (Exception exc) {
                    Logger.Log(exc.Message + Environment.NewLine + exc.StackTrace, LogType.Error, ApplicationType.TCP);
                }
            }

            while (true) {
                if (socket.Connected == false) break;

                lock (_senderLock) {
                    object queueMessage = sendMessageQueues[ip].Dequeue();

                    Logger.Log("Dequeued message from queue. IP: " + ip, LogType.Information, ApplicationType.TCP);

                    try {
                        if (queueMessage is MessageBase) {
                            MessageBase message = (MessageBase)queueMessage;

                            byte[] buffer = ObjectToByteArray(message);
                            socket.Send(buffer);

                            Logger.Log("Sending new packet dequeued. IP: " + ip, LogType.Information, ApplicationType.TCP);
                        }
                    } catch (Exception exc) {
                        Logger.Log(exc.Message + Environment.NewLine + exc.StackTrace, LogType.Error, ApplicationType.TCP);
                    }
                }
            }
        }

        private void ReceiverThread(object o) {
            Socket socket = (Socket)((object[])o)[0];
            string ip = (string)((object[])o)[1];

            if (receiveMessageQueues.ContainsKey(ip) == false) {
                try {
                    Logger.Log("Adding new blocking receiver queue for IP: " + ip, LogType.Information, ApplicationType.TCP);

                    receiveMessageQueues.Add(ip, new BlockingQueue(queueSize));
                } catch (Exception exc) {
                    Logger.Log(exc.Message + Environment.NewLine + exc.StackTrace, LogType.Error, ApplicationType.TCP);
                }
            }

            byte[] buffer = new byte[bufferSize];

            while (true) {
                if (socket.Connected == false) break;

                lock (_receiverLock) {
                    try {

                        Array.Clear(buffer, 0, buffer.Length);

                        socket.Receive(buffer);

                        Logger.Log("Received new packet. IP: " + ip + ". Added to receiver queue.", LogType.Information, ApplicationType.TCP);

                        object receivedObj = ByteArrayToObject(buffer);

                        receiveMessageQueues[ip].Enqueue(receivedObj);

                        Logger.Log("Added to packet received to blocking queue. IP: " + ip, LogType.Information, ApplicationType.TCP);
                    } catch (Exception exc) {
                        Logger.Log(exc.Message + Environment.NewLine + exc.StackTrace, LogType.Error, ApplicationType.TCP);
                    }
                }
            }
        }

        private byte[] ObjectToByteArray(Object o) {
            if (o == null) return null;

            BinaryFormatter bf = new BinaryFormatter();
            using (MemoryStream stream = new MemoryStream()) {
                bf.Serialize(stream, o);
                return stream.ToArray();
            }
        }

        private Object ByteArrayToObject(byte[] bytes) {
            BinaryFormatter formattor = new BinaryFormatter();

            using (MemoryStream ms = new MemoryStream(bytes)) {
                return formattor.Deserialize(ms);
            }
        }
    }
}