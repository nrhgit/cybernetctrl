﻿using System;
using System.Collections.Generic;
using CybernetCtrl.Common;
using System.IO;
using System.Diagnostics;
using System.Threading;
using System.Runtime.InteropServices;
using Microsoft.Win32;

namespace CybernetCtrl.Uninstall {
    static class Program {

        [DllImport("wininet.dll")]
        public static extern bool InternetSetOption(IntPtr hInternet, int dwOption, IntPtr lpBuffer, int dwBufferLength);
        public const int INTERNET_OPTION_SETTINGS_CHANGED = 39;
        public const int INTERNET_OPTION_REFRESH = 37;

        static void Main() {
            // 1. Delete license file
            try {
                string licenseFile = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) + @"\" + Settings.LocalDir + @"\license.lic";
                if (File.Exists(licenseFile)) {
                    File.Delete(licenseFile);
                }
            } catch (Exception exc) {
                Logger.Log(exc.Message + Environment.NewLine + exc.StackTrace, LogType.Error, ApplicationType.Misc);
            }

            // 2. Close client and watcher instance
            try {
                Process[] processes = Process.GetProcessesByName("Wt");
                if (processes.Length > 0) {
                    processes[0].Kill();
                }

                processes = Process.GetProcessesByName("CybernetCtrl.Client.App");
                if (processes.Length > 0) {
                    processes[0].Kill();
                }
            } catch (Exception exc) {
                Logger.Log(exc.Message + Environment.NewLine + exc.StackTrace, LogType.Error, ApplicationType.Misc);
            }

            // 3. Disable proxy at system level
            try {
                RegistryKey key = Registry.CurrentUser.OpenSubKey(@"Software\Microsoft\Windows\CurrentVersion\Internet Settings", true);
                key.SetValue("ProxyEnable", "0", RegistryValueKind.DWord);

                bool settingsReturn = InternetSetOption(IntPtr.Zero, INTERNET_OPTION_SETTINGS_CHANGED, IntPtr.Zero, 0);
                bool refreshReturn = InternetSetOption(IntPtr.Zero, INTERNET_OPTION_REFRESH, IntPtr.Zero, 0);

            } catch (Exception exc) {
                Logger.Log(exc.Message + Environment.NewLine + exc.StackTrace, LogType.Error, ApplicationType.Misc);
            }

            // 4. Remove Internet Options registry entries
            try {
                RegistryKey root = Registry.CurrentUser;

                string key = @"Software\Policies\Microsoft\Internet Explorer\Restrictions";
                RegistryAdapter.RemoveKey(root, key);

                key = @"Software\Microsoft\Windows\CurrentVersion\Policies\Explorer\DisallowCpl";
                RegistryAdapter.RemoveKey(root, key);

                key = @"Software\Microsoft\Windows\CurrentVersion\Policies\Explorer";
                RegistryAdapter.RemoveValue(root, key, "DisallowCpl");

            } catch (Exception exc) {
                Logger.Log(exc.Message + Environment.NewLine + exc.StackTrace, LogType.Error, ApplicationType.Misc);
            }

            // 5. Remove client application copied to local directory
            string libpath = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) + @"\CybernetCtrl\lib";
            if (Directory.Exists(libpath)) {
                Directory.Delete(libpath, true);
            }
        }
    }
}
