﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace CybernetCtrl.Watcher {
    public partial class AllowMessageForm : Form {
        public AllowMessageForm() {
            InitializeComponent();
        }

        private void btnOK_Click(object sender, EventArgs e) {
            this.DialogResult = DialogResult.OK;
        }
    }
}
