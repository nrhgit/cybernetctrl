﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Diagnostics;
using CybernetCtrl.Common;
using System.Threading;
using System.IO;

namespace CybernetCtrl.Watcher {
    public partial class WatcherForm : Form {
        private const string processName = "CybernetCtrl.Client.App";

        public WatcherForm() {
            InitializeComponent();
        }

        private void WatcherForm_Load(object sender, EventArgs e) {
            //Logger.Init();

            Thread watcherThread = new Thread(new ThreadStart(WatcherThread));
            watcherThread.IsBackground = true;
            watcherThread.Start();

            Thread systemProcessWatcherThread = new Thread(new ThreadStart(SystemProcessWatcherThread));
            systemProcessWatcherThread.IsBackground = true;
            systemProcessWatcherThread.Start();
        }

        private void WatcherForm_Activated(object sender, EventArgs e) {
            notifyIcon1.Visible = true;
            this.Hide();
        }

        public static void WatcherThread() {
            while (true) {
                try {
                    Process[] processes = Process.GetProcessesByName(processName);
                    if (processes.Length == 0) {
                        Logger.Log("Starting " + processName, LogType.Information, ApplicationType.Misc);
                        ProcessStartInfo startInfo = new ProcessStartInfo(Application.StartupPath + @"\" + processName + ".exe");
                        if (Environment.OSVersion.Version.Major > 5) {
                            startInfo.Verb = "runas";
                        }

                        Process process = new Process();
                        process.StartInfo = startInfo;
                        process.Start();
                    }
                } catch (Exception exc) {
                    Logger.Log(exc.Message + Environment.NewLine + exc.StackTrace, LogType.Error, ApplicationType.Misc);
                }

                Thread.Sleep(Settings.WatcherTime);
            }
        }

        public static void SystemProcessWatcherThread() {
            string regedit = "regedit";
            string msconfig = "msconfig";
            string rundll32 = "rundll32";
            string settingsPath = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) + @"\" + Settings.LocalDir + (@"\winprocess.utr");

            while (true) {
                ProcessConfig processConfig = new ProcessConfig();
                if (File.Exists(settingsPath)) {
                    processConfig = (ProcessConfig)LoadSave.Load(typeof(ProcessConfig), settingsPath);
                }

                if (processConfig.Regedit == "0") {
                    KillSystemProcess(regedit);
                }

                if (processConfig.MSConfig == "0") {
                    KillSystemProcess(msconfig);
                }

                if (processConfig.Rundll32 == "0") {
                    KillSystemProcess(rundll32);
                }

                Thread.Sleep(Settings.WatcherTime);
            }
        }

        private static void KillSystemProcess(string processName) {
            try {
                Process[] processes = Process.GetProcessesByName(processName);

                if (processes != null && processes.Length > 0) {
                    foreach (Process process in processes) {
                        process.Kill();

                        AllowMessageForm messageForm = new AllowMessageForm();
                        messageForm.ShowDialog();
                    }
                }
            } catch (Exception exc) {
                Logger.Log(exc.Message + Environment.NewLine + exc.StackTrace, LogType.Error, ApplicationType.Misc);
                MessageBox.Show(exc.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}