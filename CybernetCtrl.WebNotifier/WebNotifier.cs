﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Runtime.InteropServices;
using CybernetCtrl.Common;

namespace CybernetCtrl.WebCtrl {
    public static class WebNotifier {
        public static event EventHandler<WebVisitedEventArgs> WebVisited;

        [DllImport("kernel32.dll", CallingConvention = CallingConvention.Cdecl)]
        internal static extern bool SetDllDirectory(string pathName);

        static WebNotifier() {
            SetLocalReferenceDirectory();

            InitFiddler();
        }

        internal static bool SetLocalReferenceDirectory() {
            string path = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) + @"\" + Settings.LocalDir;

            return SetDllDirectory(path);
        }

        private static void InitFiddler() {
            Fiddler.CONFIG.bMITM_HTTPS = false;
            Fiddler.FiddlerApplication.BeforeRequest += delegate(Fiddler.Session oS) {
                WebVisited(null, new WebVisitedEventArgs() { Session = oS, Title = "", Url = oS.fullUrl, FullUrl = oS.fullUrl });

                oS.bBufferResponse = true;
            };

            Fiddler.FiddlerApplication.Startup(8877, true, true);
        }

        public static void Block(Fiddler.Session session) {
            session.oRequest.FailSession(404, "Blocked", "Website is currently blocked.");
        }

        public static void ShutDown() {
            Fiddler.FiddlerApplication.Shutdown();
        }
    }

    public class WebVisitedEventArgs : EventArgs {
        public string Title { get; set; }
        public string Url { get; set; }
        public string FullUrl { get; set; }
        public Fiddler.Session Session { get; set; }
    }
}